PROJECT_NAME           = cantankerous-impurity
COMPUTATION_SUBPROJECT = Computations
PRECALC_SUBPROJECT     = Precalculation

PROJECT_DIR      ?= ~/${PROJECT_NAME}
PROJECT_DATA_DIR ?= ${PROJECT_DIR}/Data


### DEPENDENCY DIRECTORIES
# sudo apt install sqlite3 # sqlite3 -- https://www.sqlite.org/index.html
# boost >= 1.72  -- https://www.boost.org/

# http://www.feynarts.de/cuba/
CUBA_DIR ?= ${HOME}/cuba

# https://petsc.org/release/
PETSC_DIR  ?= ${HOME}/petsc
# PETSC_DIR ?= ${HOME}/petsc-dev
# PETSC_DIR ?= ${HOME}/petsc-release
PETSC_ARCH ?= arch-linux-c-debug
# PETSC_ARCH ?= arch-linux-cxx-debug

# https://slepc.upv.es/
SLEPC_DIR ?= ${HOME}/slepc
# SLEPC_DIR ?= ${HOME}/slepc-dev
# SLEPC_DIR ?= ${HOME}/slepc-release