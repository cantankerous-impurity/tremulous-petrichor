namespace Matrix{
    constexpr int NumOpTerms { 1 + 2 + 4 };
    constexpr auto SimpBloch{ NaiveBloch||SingleValleyModel };
    constexpr auto NumTermTypes{ SingleValleyModel ? 1 : 3 };
    constexpr auto NumBlochTypes{ SimpBloch ? 1 : 2 };
    using ElemValues = std::array<std::array<cmplx*,NumOpTerms>,NumOperators>;

    class Elem
    {
        PackedIndex Ix;
    public:
        Basis::StatePair& sP;
        Coeff::Element * cE{ nullptr };
    private:
        Integrand::Puncture::Manager pM;
        Integrand::Quadrature::Manager qM;
        Integrand::Cubature::Manager cbM;
        ElemValues _z{ nullptr };

    public:

        using Spec = Operator::Spec;

        Elem() = delete;
        Elem(size_t N, ElemValues _ar) :
            Ix{ N },
            sP{ Basis::CurrBases[Ix()] },
            cE{ &Coeff::cM[sP.LaguerreIndex()] },
            pM{ &sP, cE },
            qM{ &sP, cE },
            cbM{ &sP, cE },
            _z{ _ar }
        {
            cE->Initialize<true>(sP.LaguerreIndex());
            cE->Initialize<false>(sP.LaguerreIndex());
        }

        void Compute()
        {
            const auto f{
                [&](auto oi)
                {
                    constexpr Spec oS{ oi() };
                    _Compute<oS>();
                }
            };
            constexpr_for<0, NumOperators, 1>(f);
        }
        template< Spec oS >
        void _Compute()
        {
            constexpr auto r{ oS.Repr() };
            const auto v0{ sP.Dec0() };
            const auto s0{ sP.Azimuth0() };
            const auto v1{ sP.Dec1() };
            const auto s1{ sP.Azimuth1() };
                    
            const auto f{
                [&](auto bi)
                {
                    constexpr Bloch::Type bT{ bi() };
                    if constexpr( Operator::NoPot<oS> && Bloch::IsTrunc<bT> )
                    { return 0.; }
                    else
                    { _Compute<oS,bT,r>(v0,s0,v1,s1); }
                }
            };
            constexpr_for<0, NumBlochTypes, 1>(f);
        }
        template< Spec oS, Bloch::Type bT, Td::Representation r >
        void _Compute(const auto v0,const auto s0,const auto v1,const auto s1)
        {
            using Operator::Symmetry::Prefactor;
            const auto f{
                [&](auto ti)
                {
                    constexpr Term::Type tT{ ti() };
                    const auto pf{ Prefactor<tT>(r,v0,s0,v1,s1) };
                    if (pf != czero) { Compute1<oS,tT,bT>(pf); }
                }
            };
            constexpr_for<0, NumTermTypes, 1>(f);
        }

        template< Spec oS, Term::Type tT, Bloch::Type bT >
        void Compute1(const cmplx syp)
        {
            constexpr bool Flat{ Operator::IsFlat<oS>(bT,tT) };
            constexpr bool Iso{ Operator::IsIsotropic<oS>(bT,tT) };

            const bool Rescalable{ Flat && sP.Same_Aniso() };
            const bool UnityScale{ Iso  && sP.Unity_Aniso()} ;
            const bool Ana{ UnityScale || Rescalable };
            
            const bool AziTest{ sP.Same_AzimuthFunc() };

            const auto zp{ _z[oS][TermIndex(tT,bT)] };

            *zp = Compute0<oS,tT,bT>(Ana,AziTest);

            *zp *= syp;
            *(_z[oS][6]) += *zp;
        }


        template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
            requires Operator::Pot<oS> || Bloch::IsNaive<bT>
        cmplx Compute0(const bool Analytic, const bool AzimuthTest)
        {
            cmplx res;
            constexpr bool SameDir{ tT.SameDir() };
            constexpr bool AI{ Operator::IsAzimuthallyInvariant<oS>(bT,tT) };
            if ( SameDir && AI && AzimuthTest )
            {
                if( Analytic )
                {
                    if constexpr( Integrand::Puncture::Candidate<oS,tT,bT> )
                    {
                        pM.Compute<oS,tT,bT>();
                        res = pM.value();
                    }
                }
                else
                {
                    if constexpr( Integrand::Quadrature::Candidate<oS,tT,bT> )
                    {
                        qM.Compute<oS,tT,bT>();
                        res = qM.value();
                    }
                }
            }
            else
            {
                if constexpr( Integrand::Cubature::Candidate<oS,tT,bT> )
                {
                    cbM.Compute<oS,tT,bT>();
                    res = cbM.value();
                }
            }
            return res;
        }

        cmplx& Get(Operator::Spec oS) { return *_z[oS][6]; }
        cmplx Value(Operator::Spec oS, size_t i=6) const { return *_z[oS][i]; }

    };
}