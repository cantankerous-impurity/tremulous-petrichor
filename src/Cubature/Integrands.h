namespace Integrand
{
    namespace Cubature
    {
        template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
        concept Candidate = (
            !Puncture::Candidate<oS,tT,bT> && !Quadrature::Candidate<oS,tT,bT>
        );

        template<
            Operator::Spec oS, Term::Type tT, Bloch::Type bT,
            typename KMesh
        > requires Candidate<oS,tT,bT>
        struct Impl
        {
            cmplx operator()(
                Calcs<oS,tT,bT> &iC, const Coeff::Element &cE,
                const double chi, const double phi
            ) {
                iC.Evaluate(chi,phi);
                cmplx C{ iC.LeadingConstant() * iC.EllipticalScaling() };

                cmplx sum { 0 };
                auto summation { 
                    [&sum,&iC,&cE,&chi,&phi](auto ui){
                        using is_t = Summation<oS,tT,bT,true,KMesh,ui>;
                        using is_f = Summation<oS,tT,bT,false,KMesh,ui>;
                        cmplx data{ oS.U0(ui) };
                        if(iC.EffectiveRadialMismatch() == 0)
                        {
                            if(iC.sP.ReverseFlag())
                            { data *= is_t::template f<true>(iC,cE); }
                            else
                            { data *= is_t::template f<false>(iC,cE); }
                        } else {
                            if(iC.sP.ReverseFlag())
                            { data *= is_f::template f<true>(iC,cE); }
                            else
                            { data *= is_f::template f<false>(iC,cE); }
                        }
                        bool AI{ oS.SolidAngle(ui).IsAzimuthallyInvariant() };
                        bool SI{ oS.SolidAngle(ui).IsSphericallyInvariant() };
                        if( !AI ) { data *= oS.SolidAngle(ui)(chi,phi); }
                        else 
                        if( !SI ) { data *= oS.SolidAngle(ui)(chi); }
                        
                        sum += data;
                    }
                };
                
                constexpr_for<0, oS.nterms(), 1>(summation);
                {
                    const unsigned int m0{ iC.sP.m0() };
                    const Flip && Az0{ iC.sP.Azimuth0() };
                    C *= Integrand::SolidAngle::Azimuthal(iC.phi(0),m0,Az0);
                    // if(!bool(Az0)) { C *= iunit; }
                }
                {
                    const unsigned int m1{ iC.sP.m1() };
                    const Flip && Az1{ iC.sP.Azimuth1() };
                    C *= Integrand::SolidAngle::Azimuthal(iC.phi(1),m1,Az1);
                    // if(!bool(Az1)) { C *= iunit; }
                }
                return C*sum;
            }
        };


        // selects specific function to use based on template specialization
        template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
            requires Candidate<oS,tT,bT>
        cmplx Integrand(
            Calcs<oS,tT,bT> &iC, const Coeff::Element &cE,
            const double chi, const double phi
        ) {
            if constexpr( Operator::II<oS> )
            { return Impl<oS,tT,bT,double>()(iC,cE,chi,phi); }
            if constexpr( Operator::Kin<oS> )
            { return Impl<oS,tT,bT,Kin<double>>()(iC,cE,chi,phi); }
            if constexpr( Operator::KK<oS>)
            { return Impl<oS,tT,bT,KinKin<double>>()(iC,cE,chi,phi); }
        }
    }
}