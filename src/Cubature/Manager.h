namespace Integrand
{   
    namespace Cubature
    {
        class Manager
        {

            Basis::StatePair* sPp{ nullptr };
            Coeff::Element* cEp{ nullptr };

        public:

            Manager() = default;
            constexpr Manager(Basis::StatePair *sPi, Coeff::Element *cEi) :
                sPp{ sPi }, cEp{ cEi }
            {}

            void Reinitialize(Basis::StatePair *sPi, Coeff::Element *cEi)
            {
                sPp = sPi; cEp = cEi;
            }
            template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
                requires Candidate<oS,tT,bT>
            void Compute()
            {
                // void Vegas(
                //     const int ndim, const int ncomp,
                //     integrand_t integrand, void *userdata, const int nvec,
                //     const double epsrel, const double epsabs,
                //     const int flags, const int seed,
                //     const int mineval, const int maxeval,
                //     const int nstart, const int nincrease, const int nbatch,
                //     const int gridno, const char *statefile, void *spin,
                //     int *neval, int *fail,
                //     double integral[], double error[], double prob[]
                // );

                // void Suave(
                //     const int ndim, const int ncomp,
                //     integrand_t integrand, void *userdata, const int nvec,
                //     const double epsrel, const double epsabs,
                //     const int flags, const int seed,
                //     const int mineval, const int maxeval,
                //     const int nnew, const int nmin,
                //     const double flatness, const char *statefile, void *spin,
                //     int *nregions, int *neval, int *fail,
                //     double integral[], double error[], double prob[]
                // );

                // typedef void (*peakfinder_t)(
                //     const int *ndim, const double b[],
                //     int *n, double x[], void *userdata
                // );

                // void Divonne(const int ndim, const int ncomp,
                //     integrand_t integrand, void *userdata, const int nvec,
                //     const double epsrel, const double epsabs,
                //     const int flags, const int seed,
                //     const int mineval, const int maxeval,
                //     const int key1, const int key2, const int key3,
                //     const int maxpass, const double border,
                //     const double maxchisq, const double mindeviation,
                //     const int ngiven, const int ldxgiven, double xgiven[],
                //     const int nextra, peakfinder_t peakfinder,
                //     const char *statefile, void *spin,
                //     int *nregions, int *neval, int *fail,
                //     double integral[], double error[], double prob[]
                // );

                userdata_t _X{ sPp, cEp };

                // cmplx res;
                // {
                //     const Basis::StatePair& sP { *std::get<0>( _X ) };
                //     const Coeff::Element& cE { *std::get<1>( _X ) };
                //     Calcs<oS,tT,bT> iC(sP);

                //     constexpr bool bPara{ tT.IsNotPerp() };
                //     const bool bEven{ (sP.m0()%2 + sP.m1()%2) == 0 };

                //     constexpr double x0{ 0.25316998545181877663 };
                //     constexpr double x1{ 0.27172268444770753353 };

                //     res = Cubature::Integrand<oS,tT,bT>(
                //         iC,cE,
                //         x0,
                //         (bPara && bEven ? 0.5 : 1)*PI*(x1-0.5)
                //     );
                // }

                Cuhre(
                    ndim, ncomp,
                    (integrand_t)integrand_wrapper<oS,tT,bT>,
                    &_X, nvec,
                    epsrel, epsabs,
                    flags,
                    mineval, maxeval,
                    key, statefile, spin,
                    &nregions, &neval, &fail,
                    values, error, prob
                );
            }
            std::complex<double> value() const
            {
                return std::complex<double>(values[0],values[1]); 
            }


        private:

            typedef std::tuple<
                const Basis::StatePair * const,const Coeff::Element * const
            > userdata_t;

            int nregions,neval,fail;

            static constexpr int ndim{ 2 };
            static constexpr int ncomp{ 2 };
            static constexpr int nvec{ 1 };

            double values[ncomp], error[ncomp], prob[ncomp];

            static constexpr double && epsrel{ 1e-8 };
            static constexpr double && epsabs{ 1e-10 };
            
            // static constexpr int flags { 4 | 2 };
            static constexpr int flags { 0 };

            static constexpr int mineval { 20 };
            static constexpr int maxeval { 40000 };
            static constexpr int key{ 0 };

            static constexpr char * statefile = nullptr;
            static constexpr void * spin = nullptr;

            typedef int (*integrand_t)(
                const int *ndim, const double x[], const int *ncomp,
                double f[],
                void *userdata
            );

            template<
                Operator::Spec oS, Term::Type tT, Bloch::Type bT
            > requires Candidate<oS,tT,bT>
            static int integrand_wrapper(
                const int *ndim, const double tau[],
                const int *ncomp, double f[], void * userdata_ptr
            ) {
                (void)ndim;
                (void)ncomp;

                const userdata_t& _X{ *(userdata_t*)(userdata_ptr) };
                const Basis::StatePair& sP { *std::get<0>( _X ) };
                const Coeff::Element& cE { *std::get<1>( _X ) };

                Calcs<oS,tT,bT> iC(sP);

                constexpr bool bPara{ tT.IsNotPerp() };
                const bool bEven{ (sP.m0()%2 + sP.m1()%2) == 0 };

                std::complex<double> res {
                    Cubature::Integrand<oS,tT,bT>(
                        iC,cE,
                        tau[0],
                        (bPara && bEven ? 0.5 : 1)*PI*(tau[1]-0.5)
                    )
                };

                f[0] = res.real();
                f[1] = res.imag();

                return 0;
            }
        };
    }
}