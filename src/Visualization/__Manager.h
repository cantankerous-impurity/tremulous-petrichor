namespace Visualization
{
    class Manager
    {
        std::array<Element,NumLKPairs> _data;

    public:
        Manager() = default;
        Element& Get(size_t lkN) { return _data[lkN]; }
        Element& operator[](size_t lkN) { return _data[lkN]; }
        Element& Get(size_t lki1,size_t lki2)
        {
            return _data[PackIdxs(lki1,lki2)];
        }
        // Element& operator[](size_t lki1,size_t lki2)
        // {
        //     return _data[PackIdxs(lki1,lki2)];
        // }
        template< bool dZ >
        void Initialize(const size_t lkN)
        {
            _data[lkN].Initialize<dZ>(lkN);
        }
        template< bool dZ >
        void Initialize(size_t lki1,size_t lki2)
        {
            Initialize<dZ>(PackIdxs(lki1,lki2));
        }
    };
}
