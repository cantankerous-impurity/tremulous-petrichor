namespace Visualization
{

    template < unsigned int NI >
    struct Container : SQL::ContainerWithIndex<NI,std::array<int,NI>,double>
    {
        using index_t = std::array<int,NI>;
        using value_t = double;
        using Base = SQL::ContainerWithIndex<NI,index_t,value_t>;
        using data_t = std::tuple<index_t,value_t>;
        using Base::push_back;
        
        void push_back (data_t x) { Base::push_back(x); }
        void push_back (index_t i,value_t c) { push_back(data_t(i,c)); }
        void push_back (value_t) {
            throw std::invalid_argument( "Missing set of indices to push." );
        }
    };

    template <>
    struct Container<0> : SQL::ContainerBase<double>
    {
        using Base = SQL::ContainerBase<double>;
        using Base::data;
        using Base::push_back;

        void push_back (double c) { Base::push_back(c); }
        void push_back (auto,double) {
            throw std::invalid_argument(
                "Inappropriate inclusion of set of indices."
            );
        }

        double value(size_t n) const { return Base::data(n); }
    };
}