namespace Integrand
{
    namespace Puncture
    {

        template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
        concept Candidate = ( 
            Operator::Symmetry::FlatOrIso<oS> && NaivePara<tT,bT>
        );

        template<
            Operator::Spec oS, Term::Type tT, Bloch::Type bT, class KMesh
        > requires Candidate<oS,tT,bT>
        struct Impl
        {
            cmplx operator()(
                Calcs<oS,tT,bT> &iC, const Coeff::Element &cE
            ) {
                cmplx C{ iC.LeadingConstant() };

                cmplx sum { 0 };
                auto summation { 
                    [&sum,&iC,&cE](auto ui){
                        using is_t = Summation<oS,tT,bT,true,KMesh,ui>;
                        using is_f = Summation<oS,tT,bT,false,KMesh,ui>;
                        cmplx data{ oS.U0(ui) };
                        if(iC.EffectiveRadialMismatch() == 0)
                        {
                            if(iC.sP.ReverseFlag())
                            { data *= is_t::template f<true>(iC,cE); }
                            else
                            { data *= is_t::template f<false>(iC,cE); }
                        } else {
                            if(iC.sP.ReverseFlag())
                            { data *= is_f::template f<true>(iC,cE); }
                            else
                            { data *= is_f::template f<false>(iC,cE); }
                            
                        }
                        sum += data;
                    }
                };
                constexpr_for<0, oS.nterms(), 1>(summation);

                return C*sum;
            }
        };



        template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
            requires Candidate<oS,tT,bT>
        cmplx Integrand(
            Calcs<oS,tT,bT> &iC, const Coeff::Element &cE
        ) {
            const auto l0{ iC.sP.l0() }; const auto l1{ iC.sP.l1() };
            const auto dl{ std::max(l0,l1) - std::min(l0,l1) };
            if( dl != 0 && abs(dl) != 2 && abs(dl) != 4 ) { return 0.; }
            if constexpr( Operator::II<oS> )
            {
                if( dl != 0 ) { return 0.; }
                return Impl<oS,tT,bT,double>()(iC,cE);
            }
            if constexpr( Operator::Kin<oS> )
            {
                if( dl != 0 && abs(dl) != 2 ) { return 0.; }
                return Impl<oS,tT,bT,Kin<double>>()(iC,cE);
            }
            if constexpr( Operator::KK<oS> )
            { return Impl<oS,tT,bT,KinKin<double>>()(iC,cE); }
        }

    }
}