namespace Integrand
{    
    namespace Puncture
    {
        class Manager
        {

            Basis::StatePair* sP{ nullptr };
            Coeff::Element* cE{ nullptr };

        public:

            Manager() = default;
            constexpr Manager(Basis::StatePair *sPi, Coeff::Element *cEi) :
                sP{ sPi }, cE{ cEi }
            {}

            void Reinitialize(Basis::StatePair *sPi, Coeff::Element *cEi)
            {
                sP = sPi; cE = cEi;
            }
            template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
                requires Candidate<oS,tT,bT>
            void Compute()
            {
                Calcs<oS,tT,bT> iC(*sP);
                _value = Puncture::Integrand<oS,tT,bT>(iC,*cE);
            }
            std::complex<double> value() const { return _value; }
        
        private:

            std::complex<double> _value;

        };
    }
}