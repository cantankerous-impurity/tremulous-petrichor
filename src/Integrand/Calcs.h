namespace Integrand
{

    template< Operator::Spec oS, Term::Type tT, Bloch::Type bT >
    class Calcs
    {

    public:

        using N_t = std::array<ui_t,2>;
        using X_t = std::array<double,2>;
        using Z_t = Bloch::Z_t;
        using Z_arr = Bloch::Z_arr;

        static constexpr ui_t NumPotTerms{ oS.nterms() };
        static constexpr Z_t v0{ 0., 0. };    

        const Basis::StatePair &sP;
        const bool RF;

    protected:

        bool State{ false };

        Flip _Omega;
        Flip _Omega0;

        N_t _L;
        double _L01;

        N_t _K;
        ui_t _K01;
        
        double _LeadingConstant;

        X_t _Phi;
        X_t _ScaledChi;
        X_t _ScaledNorm;

        double _SAAvgScaling;
        double _SAScalingMismatch;

        double _MismatchFactor;

        double _EffectiveAvgRadius
        {
            std::numeric_limits<double>::signaling_NaN()
        };
        X_t _EffectiveRadScale;

        double _EffectiveRadialMismatch
        {
            std::numeric_limits<double>::signaling_NaN()
        };

        double _EllipticalScaling;

        Z_arr _Zs;
        static constexpr auto& bI { Bloch::GetInterface<tT,bT>() };

    public:

        Calcs() = delete;
        constexpr Calcs( const Basis::StatePair& _sP ) :
            sP{ _sP }, RF{ sP.ReverseFlag() }
        {
            _Omega = sP.Omega0() * sP.Omega1();
            _Omega0 = sP.Omega0();

            bool Kin0{ RF ? oS.KR() : oS.KL() };
            bool Kin1{ RF ? oS.KL() : oS.KR() };
            {
                const ui_t l0{ sP.l0() };
                _L[0] = 2*l0+3-2*(Basis::WfIsSlater?(Kin0?1:0):0);
                const ui_t k0{ sP.k0() };
                _K[0] = k0; if(Kin0) { _K[0] += 1; if(l0 >= 2) { _K[0] += 1; } }
            }
            {
                const ui_t l1{ sP.l1() };
                _L[1] = 2*l1+3-2*(Basis::WfIsSlater?(Kin1?1:0):0);
                const ui_t k1{ sP.k1() };
                _K[1] = k1; if(Kin1) { _K[1] += 1; if(l1 >= 2) { _K[1] += 1; } }
            }

            const double & d{ (double)oS.exp() };
            _L01 = (_L[0]+_L[1])/2 + d;
            _K01 = _K[0] + _K[1];


            const double & rLR{ sP.RadiusLR() };
            double y{ std::pow(rLR,d) };
            if(Kin0) { y /= rLR*rLR; }
            if(Kin1) { y /= rLR*rLR; }
            _LeadingConstant = y;

            if( !sP.Same_Radius() )
            {
                const double s0{ sP.RadScale0() };
                double x0{ std::pow(sqrt(s0),_L[0]) };
                if(Kin0) { x0 *= s0*s0; }
                
                const double s1{ sP.RadScale1() };
                double x1{ std::pow(sqrt(s1),_L[1]) };
                if(Kin1) { x1 *= s1*s1; }

                _LeadingConstant *= x0*x1;
            }

            bool SA{ sP.Same_Aniso() && tT.SameDir() && bT.IsNaive() };
            if( SA || sP.Unity_Aniso() )
            {
                _SAScalingMismatch = 0;
                _MismatchFactor = 1;

                _EffectiveRadialMismatch = sP.Mismatch01();
                _EffectiveRadScale = { sP.RadScale0(), sP.RadScale1() };
            }

            if( sP.Unity_Aniso() )
            {
                _SAAvgScaling = 1;
                _EllipticalScaling = 1;
                _ScaledNorm = { 1, 1 };

                _EffectiveAvgRadius = sP.RadiusLR();

                if constexpr( NaivePara<tT,bT> ) { CalcZ(); }
            }

        }
        void CalcZ(const double chi=0, const double phi=0)
        {
            constexpr_for<0,NumPotTerms,1>(
                [&](auto iu){
                    constexpr auto& dS { oS.Decay(iu) };
                    constexpr auto dT { Decay::EffectiveType<dS,tT,bT>() };
                    if constexpr( !dT.UsesNoZ() )
                    {
                        auto& bI { Bloch::GetInterface<tT,bT>() };
                        constexpr bool SI{ dS.IsSphericallyInvariant() };
                        constexpr bool AI{ dS.IsAzimuthallyInvariant() };
                        constexpr bool Var{ !SI && !AI };
                        const double r{ EffectiveAvgRadius() };

                        if constexpr( NaiveNotPerp<tT,bT> && !Var )
                        {
                            if constexpr( SI && NaivePara<tT,bT> )
                            {
                                bI.template CalcZ<oS,dS>(_Zs,r);
                            }
                            else
                            {
                                bI.template CalcZ<oS,dS>(_Zs,r,chi);
                            }
                        }
                        else
                        {
                            if constexpr( bT == Bloch::Type::Naive )
                            {
                                bI.template CalcZ<oS,dS>(_Zs,r,chi,phi);
                            }
                            if constexpr( bT == Bloch::Type::Trunc )
                            {
                                bI.template CalcZ<oS,dS>(r,chi,phi);
                            }
                        }
                    }
                }
            );
        }

        std::array<double,3>& GetSAKin() { return _GetSAKin(); }
        std::array<double,9>& GetSAKinKin() { return _GetSAKinKin(); }

        auto& GetOneArr() { return _GetOneArr(); }
        auto& GetTwoArr() { return _GetTwoArr(); }
        auto& GetOneOneArr() { return _GetOneOneArr(); }
        auto& GetTwoOneArr() { return _GetTwoOneArr(); }

        auto& GetOne(ui_t sk = 0) { return GetOneArr()[sk]; }
        auto& GetTwo(ui_t sk = 0) { return GetTwoArr()[sk]; }
        auto& GetOneOne(ui_t sk = 0) { return GetOneOneArr()[sk]; }
        auto& GetTwoOne(ui_t sk = 0) { return GetTwoOneArr()[sk]; }


        const Z_t& GetZ(size_t g, ui_t k) const
        {
            if constexpr( bT == Bloch::Type::Naive )
            {
                if( g==0 )
                {
                    if constexpr(tT == Term::Type::Perpendicular)
                    {
                        if ( k<=1 ) { return _Zs[k]; }
                    }
                    else
                    {
                        if( k==0 ) { return _Zs[k]; }
                    }
                    throw std::invalid_argument("Key index out of range.");
                }
                throw std::invalid_argument("Value index out of range.");
            }
            if constexpr( bT == Bloch::Type::Trunc ) { return bI.GetZ(g,k); }
        }

        template< bool SR, bool miss, bool match >
        auto& GetArr()
        {
            if constexpr( !SR )
            {
                if constexpr( match )
                {
                    // Case D4a, D != 0, z != 1
                    if constexpr( miss ) { return GetTwoOneArr(); }
                    else { return GetTwoArr() ; }
                }
                else
                {
                    // Case D4b, D != 0, z = 1
                    if constexpr( miss ) { return GetOneOneArr(); }
                    else { return GetOneArr() ; }
                }
            }
            else
            {
                if constexpr( match )
                {
                    // Case D4c, D = 0, z != 1
                    if constexpr( miss ) { return GetOneOneArr(); }
                    else { return GetOneArr(); }
                }
                else
                {
                    // Case D4d, D = 0, z = 1
                    return GetOneArr() ;
                }
            }
        }


        void ExecuteFOnJArr(const auto &f)
        {
            f(0);
            if constexpr( !Operator::II<oS> )
            {
                const bool bL{ sP.l0() >= 2 }; const bool bR{ sP.l1() >= 2 };
                if constexpr( Operator::Kin<oS> )
                {
                    const bool bR1{ bR && Operator::IK<oS> };
                    const bool bL1{ bL && Operator::KI<oS> };
                    if( !(bL1 || bR1) ) { return; }
                    f(1);
                }
                else if constexpr( Operator::KK<oS> )
                {
                    if( !(bL || bR) ) { return; }
                    f(1);
                    if( bL && bR ) { f(2); }
                }
            }
        }

        template< bool SR, bool miss, bool match >
        void ZeroJArr()
        {
            const auto& f{ 
                [&](auto sk) { return ZeroJTerm<SR,miss,match>(sk); }
            };
            ExecuteFOnJArr(f);
        }

        template< bool SR, bool miss, bool match >
        void AddToJArr(size_t g = 0, ui_t k = 0)
        {
            const bool iA{ bI.template IsAnti<oS>(g,k) };

            const Z_t& Zs{ GetZ(g,k) };
            const auto Zabs{ bI.template Value<oS>(g) };
            const auto ph{ bI.template Phase<oS>(g) };
            const cmplx Z1{ ( iA ? (int)Omega0() : 1 )*std::polar(Zabs,ph) };
            const auto& f{
                [&](auto sk) { return AddToJTerm<SR,miss,match>(Zs,Z1,sk); }
            };
            ExecuteFOnJArr(f);
        }


        template< bool SR, bool miss, bool match >
        void ZeroJTerm(const ui_t sk = 0)
        {
            auto& res{ GetArr<SR,miss,match>()[sk] };

            if constexpr( !SR && match )
            {
                const auto _K0{ K0() };
                const auto _K1{ K1() };
                res.zero(_K0,_K1);
            }
            else if constexpr( SR && !match && !miss )
            {
                if( res.size() < 1 )
                {
                    throw std::length_error( "Not enough space for One." );
                }
                res(0) = 0;
            }
            else
            {
                const auto _K{ K() };
                res.zero(_K);
            }
        }

        template< bool SR, bool miss, bool match >
        void AddToJTerm(const Z_t& Zs, const cmplx& Z1, const ui_t sk = 0)
        {
            const auto _L{ L() - WfType*sk };
            if (_L < 0) { return; };
            const auto _O{ Omega() };

            auto& res{ GetArr<SR,miss,match>()[sk] };

            if constexpr( !SR )
            {
                const auto _D{ EffectiveRadialMismatch() };

                if constexpr( match )
                {
                    // Case D4a, D != 0, z != 1
                    const auto _K0{ K0() };
                    const auto _K1{ K1() };
                    auto &Fs{ _GetTwo() };
                    if constexpr( miss )
                    {
                        auto &Gs{ _GetOne1() };
                        Radial::AddJaTo(res,Fs,Gs, Zs,_L,_K0,_K1,_D,_O,Z1);
                    }
                    else
                    {
                        Radial::AddJaTo(res,Fs, Zs[0],_L,_K0,_K1,_D,_O,Z1);
                    }
                }
                else
                {
                    // Case D4b, D != 0, z = 1
                    const auto _K{ K() };
                    auto &Fs{ _GetOneD() };
                    if constexpr( miss )
                    {
                        auto& Gs{ _GetOne1() };
                        Radial::AddJbTo(res,Fs,Gs, Zs[1],_L,_K,_D,_O,Z1 );
                    }
                    else
                    {
                        Radial::AddJbTo(res,Fs, _K,_D);
                    }
                }
            }
            else
            {
                const auto _K{ K() };
                if constexpr( match )
                {
                    // Case D4c, D = 0, z != 1
                    auto &Fs{ _GetOne0() };
                    if constexpr( miss )
                    {
                        auto& Gs{ _GetOne1() };
                        Radial::AddJcTo(res,Fs,Gs, Zs,_L,_K,_O,Z1);
                    }
                    else
                    {
                        Radial::AddJcTo(res,Fs, Zs[0],_L,_K,_O,Z1);
                    }
                }
                else
                {
                    // Case D4d, D = 0, z = 1
                    if constexpr( miss )
                    {
                        auto& Gs{ _GetOne1() };
                        Radial::AddJdTo(res,Gs, Zs[1],_L,_K,_O,Z1);
                    }
                    else
                    {
                        Radial::AddJdTo(res);
                    }
                }
            }
        }

        static constexpr size_t NumValues()
        { return bI.template NumValues<oS>(); }
        static constexpr auto NumKeys(size_t g = 0)
        { return bI.template NumKeys<oS>(g); }

        std::array<double,2> ScaleChi(const X_t & Chi)
        {
            std::array<double,2> _AngScale
            {
                std::numeric_limits<double>::signaling_NaN()
            };
            const X_t Aniso { sP.Aniso0(), sP.Aniso1() };
            constexpr_for<(ui_t)0,(ui_t)2,(ui_t)1>([&](auto i)
            {
                const double ans{ Aniso[i] };
                if( ans == 1 )
                {
                    _ScaledChi[i] = Chi[i];
                    _ScaledNorm[i] = 1.;
                    _AngScale[i] = 1.;
                }
                else if(Chi[i] == 1)
                {
                    _ScaledChi[i] = 1.;
                    _ScaledNorm[i] = 1./ans;
                    _AngScale[i] = ans;
                }
                else if(Chi[i] == 0)
                {
                    _ScaledChi[i] = 0.;
                    _ScaledNorm[i] = sqrt(ans);
                    _AngScale[i] = 1.;
                }
                else
                {
                    if ( ans > 1 )
                    {
                        double x{ 1./(ans*ans) };
                        _AngScale[i] = x + (1 - x)*Chi[i]*Chi[i];
                        _AngScale[i] = ans*sqrt(_AngScale[i]);
                    }
                    if ( ans < 1 )
                    {
                        _AngScale[i] = 1 + (ans*ans-1)*Chi[i]*Chi[i];
                        _AngScale[i] = sqrt(_AngScale[i]);
                    }
                    const double Ax{ ans/_AngScale[i] };
                    const double Bx{ _AngScale[i]*_AngScale[i] };
                    _ScaledChi[i] = Ax*Chi[i];
                    _ScaledNorm[i] = sqrt(Ax/Bx);
                }
            });
            return _AngScale;
            
        }

        void Evaluate(const double chi)
        {
            State = true;

            if( sP.Unity_Aniso() )
            {
                _ScaledChi = { chi, chi };
            }
            if( sP.Same_Aniso() )
            {
                const double & ans{ sP.AnisoL() };
                if(chi == 1)
                {
                    _ScaledNorm = { 1/ans, 1/ans };
                    _SAAvgScaling = ans;
                    _ScaledChi = { 1, 1 };
                }
                else if(chi == 0)
                {
                    _ScaledNorm = { sqrt(ans), sqrt(ans) };
                    _SAAvgScaling = 1;
                    _ScaledChi = { 0, 0 };
                }
                else 
                {
                    if ( ans > 1 )
                    {
                        double x{ 1./(ans*ans) };
                        _SAAvgScaling = ans*sqrt(x + (1 - x)*chi*chi);
                    }
                    if ( ans < 1 )
                    {
                        _SAAvgScaling = sqrt(1 + (ans*ans-1)*chi*chi);
                    }
                    const double Ax{ ans/_SAAvgScaling };
                    const double Bx{ _SAAvgScaling*_SAAvgScaling };
                    _ScaledChi = { Ax*chi, Ax*chi };
                    _ScaledNorm = { sqrt(Ax/Bx), sqrt(Ax/Bx) };
                }

                _EffectiveAvgRadius = sP.RadiusLR()/_SAAvgScaling;

                const double && d{ _L01 - (_L[0]+_L[1])/2 };
                _EllipticalScaling = 1./std::pow(_SAAvgScaling,d);

            } else {
                std::array<double,2> &&_AngScale{ ScaleChi({chi,chi}) };
                {
                    double x0, x1;
                    if constexpr(Basis::WfIsSlater)
                    {
                        x0 = _AngScale[0];
                        x1 = _AngScale[1];
                        _SAAvgScaling = (x0+x1)/2;
                    }
                    if constexpr(Basis::WfIsGauss)
                    {
                        x0 = _AngScale[0]*_AngScale[0];
                        x1 = _AngScale[1]*_AngScale[1];
                        _SAAvgScaling = sqrt((x0+x1)/2);
                    }
                    _SAScalingMismatch = (x0-x1)/(x0+x1);
                }
                {
                    double s{ sP.Mismatch01() };
                    const double & e{ _SAScalingMismatch };

                    const double x{ 1. + e*s };

                    _EffectiveRadialMismatch = (e + s) / x;
                    if constexpr(Basis::WfIsSlater) { _MismatchFactor = x; }
                    if constexpr(Basis::WfIsGauss) { _MismatchFactor = sqrt(x); }
                }
                {
                    const double x{ _MismatchFactor*_SAAvgScaling };
                    {
                        const X_t Radius{ sP.Radius0(), sP.Radius1() };
                        _EffectiveAvgRadius = sP.RadiusLR() / x;
                        const double & rE{ _EffectiveAvgRadius };
                        const X_t ri
                        {
                            Radius[0]/_AngScale[0], Radius[1]/_AngScale[1]
                        };
                        _EffectiveRadScale = { rE/ri[0], rE/ri[1] };
                    }
                    const double x0{ std::pow(sqrt(_AngScale[0]),_L[0]) };
                    const double x1{ std::pow(sqrt(_AngScale[1]),_L[1]) };
                    const double X01{ std::pow(x,_L01) };

                    _EllipticalScaling = (x0*x1)/X01;
                }
            }
            CalcZ(chi);
        }
        void Evaluate0(const double chiL, const double chiR)
        {
            const X_t Chi { RF ? chiR : chiL, RF ? chiL : chiR };
            State = true;

            if ( sP.Unity_Aniso() )
            {
                _ScaledChi = Chi;
                _ScaledNorm = { 1, 1 };
                _SAAvgScaling = 1 ;
                _SAScalingMismatch = 0;
                _EffectiveRadialMismatch = sP.Mismatch01();
                _EffectiveAvgRadius = sP.RadiusLR();
                _EllipticalScaling = 1;
                _EffectiveRadScale = { sP.RadScale0(), sP.RadScale1() };
                return;
            }

            X_t &&_AngScale{ ScaleChi(Chi) };
            {
                double x0, x1;
                if constexpr(Basis::WfIsSlater)
                {
                    x0 = _AngScale[0];
                    x1 = _AngScale[1];
                    _SAAvgScaling = (x0+x1)/2;
                }
                if constexpr(Basis::WfIsGauss)
                {
                    x0 = _AngScale[0]*_AngScale[0];
                    x1 = _AngScale[1]*_AngScale[1];
                    _SAAvgScaling = sqrt((x0+x1)/2);            
                }
                _SAScalingMismatch = (x0-x1)/(x0+x1);
            }
            {
                const double s{ sP.Mismatch01() };
                const double e{ _SAScalingMismatch };
                const double x{ 1. + e*s };

                _EffectiveRadialMismatch = (e + s) / x;
                if constexpr(Basis::WfIsSlater) { _MismatchFactor = x; }
                if constexpr(Basis::WfIsGauss) { _MismatchFactor = sqrt(x); }
            }
            {
                const double x{ _MismatchFactor*_SAAvgScaling };
                {
                    const X_t Radius { sP.Radius0(), sP.Radius1() };
                    _EffectiveAvgRadius = sP.RadiusLR() / x;
                    const double rE{ _EffectiveAvgRadius };
                    const X_t ri {
                        Radius[0]/_AngScale[0], Radius[1]/_AngScale[1]
                    };
                    _EffectiveRadScale = { rE/ri[0], rE/ri[1] };

                }
                const double x0{ std::pow(sqrt(_AngScale[0]),_L[0]) };
                const double x1{ std::pow(sqrt(_AngScale[1]),_L[1]) };
                const double X01{ std::pow(x,_L01) };

                _EllipticalScaling = (x0*x1)/X01;
            }
        }
        void Evaluate(const double chi, const double phi)
        {
            double chiL, chiR;
            double phiL, phiR;
            const double c{ cos(phi) };
            const double s{ sin(phi) };
            const double x{ sqrt(1-chi*chi)*c };
            const double y{ sqrt(1-chi*chi)*s };

            AnisoDirection dirL{ tT.Dirs()[0] };
            AnisoDirection dirR{ tT.Dirs()[1] };

            using enum AnisoDirection::Enum;
            switch(dirL)
            {
                case X: chiL = x; phiL = atan2(chi,y); break;
                case Y: chiL = y; phiL = atan2(x,chi); break;
                case Z: chiL = chi; phiL = phi; break;
            }
            switch(dirR)
            {
                case X: chiR = x; phiR = atan2(chi,y); break;
                case Y: chiR = y; phiR = atan2(x,chi); break;
                case Z: chiR = chi; phiR = phi; break;
            }

            Evaluate0(chiL,chiR);
            CalcZ(chi,phi);
            _Phi = { RF ? phiR : phiL, RF ? phiL : phiR };
        }

        double L() const { return _L01; }
        ui_t K() const { return _K01; }
        ui_t K0() const { return _K[0]; }
        ui_t K1() const { return _K[1]; }
        Flip Omega() const { return _Omega; }
        Flip Omega0() const { return _Omega0; }
        double SolidAngleAvgScaling() const { return _SAAvgScaling; }
        double SolidAngleScalingMismatch() const { return _SAScalingMismatch; }
        double MismatchFactor() const { return _MismatchFactor; }
        double EffectiveAvgRadius() const { return _EffectiveAvgRadius; }
        double EffectiveRadialMismatch() const
        {
            return _EffectiveRadialMismatch;
        }
        double LeadingConstant() const { return _LeadingConstant; }
        double EllipticalScaling() const { return _EllipticalScaling; }

        double EffectiveRadScale(ui_t n) const { return _EffectiveRadScale[n]; }
        double EffectiveRadScaleL() const {
            return (RF ? _EffectiveRadScale[1] : _EffectiveRadScale[0]);
        }
        double EffectiveRadScaleR() const {
            return (RF ? _EffectiveRadScale[0] : _EffectiveRadScale[1]);
        }
        double ScaledNorm(ui_t n) const { return _ScaledNorm[n]; }
        double chi(ui_t n) const { return _ScaledChi[n]; }
        double phi(ui_t n) const { return _Phi[n]; }
        

        bool HasEvaluated() const { return State; }
    };
}