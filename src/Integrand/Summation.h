namespace Integrand
{
    template<
        Operator::Spec oS,
        Term::Type tT, Bloch::Type bT,
        bool SR, typename KMesh,
        size_t ui
    >
    struct Summation
    {

        template< bool RF >
        static auto f(Calcs<oS,tT,bT> &iC, const Coeff::Element &cE)
        {
            constexpr auto _K { RF ? oS.Kx : oS.K };
            
            constexpr size_t _ex { oS.e_idx() };
            constexpr auto _D { Decay::EffectiveType<oS.Decay(ui),tT,bT>() };
            const auto& C{ cE.template Get<_K,_ex,_D,SR>() };
            const KMesh &&P{ (SolidAngle::LegAB<_K,oS,tT,bT>{})(iC) };

            if constexpr( Decay::Zero<_D> && SR )
            {
                return Radial::Summation(C,P);
            }
            else
            {
                using rC = Radial::Core<oS,tT,bT,SR,ui>;
                const auto& J{ rC()(iC) }; return Radial::Summation(C,P,J);
            }
            
        }
    };

}