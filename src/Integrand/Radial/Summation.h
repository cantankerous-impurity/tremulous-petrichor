namespace Integrand
{
    namespace Radial
    {
        // D4d, Same Radius, No Decay ==========================================
        template < Kinetic::Spec K, size_t ex, class T1 >
        cmplx Summation(
            const Coeff::Interface<K,ex,Decay::Type::Zero,true>& C,
            const T1& P
        ) {
            if constexpr(Kinetic::II<K>)
            {
                return C.value(0)*P;
            }
            else
            {
                double sum{ 0 };
                for (unsigned int n = 0; n<C.size(); n++)
                {
                    if constexpr(Kinetic::IK_or_KI<K>)
                    {
                        const int s{ C.index(n,0) };
                        sum += C.value(n)*P(s);
                    }
                    if constexpr(Kinetic::KK<K>)
                    {
                        const int s0{ C.index(n,0) };
                        const int s1{ C.index(n,1) };
                        sum += C.value(n)*P(s0,s1);
                    }
                }
                return sum;
            }
        }
        
        // D4d,      Same Radius, Different Decay ==============================
        // D4c,      Same Radius,      Same Decay ==============================
        // D4c,      Same Radius,      Full Decay ==============================
        // D4b, Different Radius,        No Decay ==============================
        // D4b, Different Radius, Different Decay ==============================
        // D4a, Different Radius,      Same Decay ==============================
        // D4a, Different Radius,      Full Decay ==============================
        template <
            Kinetic::Spec K, size_t ex, Decay::Type D, bool SR,
            class T1, class T2
        >
            requires requires {( Decay::NonZero<D> || !SR );}
        auto Summation(
            const Coeff::Interface<K,ex,D,SR>& C,
            const T1& P,
            const T2& J
        ) {
            using M1_t = One<double>;
            using M2_t = Two<double>;
            using M11_t = OneOne<double>;
            using M21_t = TwoOne<double>;

            using Kin1_t = Kin<One<double>>;
            using Kin2_t = Kin<Two<double>>;
            using Kin11_t = Kin<OneOne<double>>;
            using Kin21_t = Kin<TwoOne<double>>;

            using KK1_t = KinKin<One<double>>;
            using KK2_t = KinKin<Two<double>>;
            using KK11_t = KinKin<OneOne<double>>;
            using KK21_t = KinKin<TwoOne<double>>;

            constexpr bool Is1{ std::is_same_v<T2, M1_t> };

            cmplx sum{ 0. };
            for (size_t n = 0; n<C.size(); n++)
            {
                const auto &Cv { C.value(n) };
                if constexpr(Kinetic::II<K>)
                {
                    constexpr bool Is2{ std::is_same_v<T2, M2_t> };
                    constexpr bool Is11{ std::is_same_v<T2, M11_t> };
                    constexpr bool Is21{ std::is_same_v<T2, M21_t> };

                    const auto &Pv { P };
                    if constexpr( Is1 )
                    {
                        const unsigned int t{ (unsigned int) C.index(n,0) };
                        sum += Cv*Pv*J(t);
                    }

                    if constexpr( Is2 || Is11 )
                    {
                        const unsigned int t0{ (unsigned int) C.index(n,0) };
                        const unsigned int ux{ Basis::WfIsSlater ? 0 : t0 };
                        const unsigned int t1{ C.index(n,1)-ux };
                        sum += Cv*Pv*J(t0,t1);
                    }
                    if constexpr( Is21 )
                    {
                        const unsigned int u0{ (unsigned int) C.index(n,0) };
                        const unsigned int ux{ Basis::WfIsSlater ? 0 : u0 };
                        const unsigned int u1{ C.index(n,1)-ux };
                        const unsigned int  w{ (unsigned int) C.index(n,2) };
                        sum += Cv*Pv*J(u0,u1,w);
                    }
                }
                if constexpr(Kinetic::IK_or_KI<K>)
                {
                    constexpr bool IsKin1{ std::is_same_v<T2, Kin1_t> };
                    constexpr bool IsKin2{ std::is_same_v<T2, Kin2_t> };
                    constexpr bool IsKin11{ std::is_same_v<T2, Kin11_t> };
                    constexpr bool IsKin21{ std::is_same_v<T2, Kin21_t> };

                    const si_t s{ C.index(n,0) };
                    const auto &Pv { P(s) };
                    if constexpr( Is1 )
                    {
                        const unsigned int t{ (unsigned int) C.index(n,1) };
                        sum += Cv*Pv*J(t);
                    }
                    if constexpr( IsKin1 )
                    {
                        const unsigned int t{ (unsigned int) C.index(n,1) };
                        sum += Cv*Pv*J(s,t);
                    }
                    if constexpr( IsKin2 || IsKin11 )
                    {
                        const unsigned int t0{ (unsigned int) C.index(n,1) };
                        const unsigned int tx{ Basis::WfIsSlater ? 0 : t0 };
                        const unsigned int t1{ C.index(n,2)-tx };
                        sum += Cv*Pv*J(s,t0,t1);
                    }
                    if constexpr( IsKin21 )
                    {
                        const unsigned int u0{ (unsigned int) C.index(n,1) };
                        const unsigned int ux{ Basis::WfIsSlater ? 0 : u0 };
                        const unsigned int u1{ C.index(n,2)-ux };
                        const unsigned int  w{ (unsigned int) C.index(n,3) };
                        sum += Cv*Pv*J(s,u0,u1,w);
                    }
                }
                if constexpr(Kinetic::KK<K>)
                {
                    constexpr bool IsKinKin1{ std::is_same_v<T2, KK1_t> };
                    constexpr bool IsKinKin2{ std::is_same_v<T2, KK2_t> };
                    constexpr bool IsKinKin11{ std::is_same_v<T2, KK11_t> };
                    constexpr bool IsKinKin21{ std::is_same_v<T2, KK21_t> };

                    const si_t s0{ C.index(n,0) };
                    const si_t s1{ C.index(n,1) };
                    const auto &Pv { P(s0,s1) };
                    if constexpr( Is1 )
                    {
                        const unsigned int t{ (unsigned int)C.index(n,2) };
                        sum += Cv*Pv*J(t);
                    }
                    if constexpr( IsKinKin1 ) 
                    {
                        const unsigned int t{ (unsigned int)C.index(n,2) };
                        sum += Cv*Pv*J(s0,s1,t);
                    }
                    if constexpr( IsKinKin2 || IsKinKin11 )
                    {
                        const unsigned int t0{ (unsigned int)C.index(n,2) };
                        const unsigned int tx{ Basis::WfIsSlater ? 0 : t0 };
                        const unsigned int t1{ C.index(n,3)-tx };
                        sum += Cv*Pv*J(s0,s1,t0,t1);
                    }
                    // This is a dummy result -- should not happen.
                    if constexpr( IsKinKin21 ) {}
                }
            }
            return sum;
        }
    }
}