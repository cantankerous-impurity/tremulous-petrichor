namespace Integrand
{
    namespace Radial
    {

        using ui_t = uint_fast8_t;

        // used by cases D4b and D4c
        template< typename T >
        void _SetF(One<T> &res, const T& Z0, const T& Z1, const ui_t K)
        {
            if( res.size() < K+1U )
            {
                throw std::length_error( "Not enough space for One." );
            }
            res(0) = Z0;
            for( ui_t u = 1; u <= K; u++ ) { res(u) = res(u-1)*Z1; }
        }

        // Case D4a: D != 0, z != 1
        void SetF(
            Two<cmplx> &res,
            const cmplx &z,
            const double n,
            const ui_t K0, const ui_t K1,
            const double Delta
        ) {
            cmplx Z;
            if constexpr(Basis::WfIsSlater) { Z = std::pow( z , n ); }
            if constexpr(Basis::WfIsGauss)  { Z = std::pow( sqrt(z) , n ); }
            const cmplx Z0{ 1.-z*(1.+Delta) };
            const cmplx Z1{ 1.-z*(1.-Delta) };

            if( res.size() < (ui_t)( (K0+1)*(K1+1) ) )
            {
                throw std::length_error( "Not enough space for Two." );
            }
            
            const unsigned KIn{ K1 > K0 ? K1 : K0 };
            const unsigned KOut{ K1 > K0 ? K0 : K1 };
            const cmplx &ZIn{ K1 > K0 ? Z1 : Z0 };
            const cmplx &ZOut{ K1 > K0 ? Z0 : Z1 };
            res.resize(KOut+1,KIn+1);
            res(0,0) = Z;
            for (ui_t u0 = 1; u0 <= KOut; u0++)
            {
                res(u0,0) = res(u0-1,0)*ZOut;
            }
            for (ui_t u0 = 0; u0 <= KOut; u0++)
            {
                for (ui_t u1 = 1; u1 <= KIn; u1++)
                {
                    res(u0,u1) = res(u0,u1-1)*ZIn;
                }
            }
            if ( !(K1 > K0) ) { res.transpose(); }
        }
        // Case D4b: D != 0, z = 1
        void SetF(One<double> &res, const ui_t K, const double Delta)
        {
            _SetF(res,1.,std::forward<const double>(Delta),K);
        }
        // Case D4c: D = 0, z != 1
        void SetF(
            One<cmplx> &res,const cmplx &z, const double n, const ui_t K
        ) {
            cmplx Z;
            if constexpr(Basis::WfIsSlater) { Z = std::pow( z , n ); }
            if constexpr(Basis::WfIsGauss)  { Z = std::pow( sqrt(z) , n ); }
            const cmplx Z0{ 1.-z };

            _SetF(res,Z,Z0,K);
        }
        // Case D4d: D = 0, z = 1
        void SetF(One<double> &res, const ui_t K)
        {
            if( res.size() < ui_t(K+1) )
            {
                throw std::length_error( "Not enough space for One." );
            }
            // res.reset();
            res(0) = 1.;
        }

        // used by all cases
        void SetG(
            One<cmplx> &res, const cmplx &Z, const double n, const ui_t K12
        ) {
            if( res.size() < ui_t(K12+1) )
            {
                throw std::length_error( "Not enough space for One." );
            }
            if constexpr(Basis::WfIsSlater)
            {
                cmplx Z0{ std::pow(Z, n) };
                std::array<double,2> Xs{
                    sqrt(PI)/boost::math::tgamma(n/2.+0.5),
                    sqrt(PI)/boost::math::tgamma(n/2.+1.)
                };
                for (ui_t w = 0; w <= K12; w++)
                {
                    uint_fast8_t m{ uint_fast8_t(w % 2) };
                    if( n+w <= 0) { res(w) = 0.; } 
                    else
                    {
                        const cmplx & HF{ HyperFunc(n+w,Z) };
                        res(w) = Xs[m]*Z0*HF;
                    }
                    Xs[m] *= 2./(n+w+2-1);
                    Z0 *= Z;
                }               
            }
            if constexpr(Basis::WfIsGauss)
            {
                for (ui_t w = 0; w <= K12; w++)
                {
                    res(w) = HyperFunc(n+2*w,Z);
                }
            }
        }

        // Case D4a: D != 0, z != 1
        // with wavefunction / potential decay mismatch
        void AddJaTo(
            TwoOne<double> &res,
            Two<cmplx> &Fs,
            One<cmplx> &Gs,
            const std::array<cmplx,2> &Zs,
            const auto n, const auto K0, const auto K1,
            const double Delta,
            const Flip Om, const cmplx &Z0
        ) {
            const ui_t K01{ K0+K1 };
            if( res.size() < ((K0+1)*(K1+1)*(K01+2))/2 )
            {
                throw std::length_error( "Not enough space for TwoOne." );
            }

            SetF(Fs,Zs[0],n,K0,K1,Delta);
            SetG(Gs,Zs[1],n,K01);

            const ui_t KIn{ K1 > K0 ? K1 : K0 };            
            const ui_t KOut{ K1 > K0 ? K0 : K1 };
            res.resize(KOut+1,KIn+1);
            for( ui_t u0 = 0; u0 <= KOut; u0++ )
            {
                for ( ui_t u1 = 0; u1 <= KIn; u1++ )
                {
                    const ui_t ua{ K1 > K0 ? u0 : u1 };
                    const ui_t ub{ K1 > K0 ? u1 : u0 };
                    const auto &F{ Fs(ua,ub) };
                    for ( ui_t w = 0; w <= K01-u0-u1; w++ )
                    {
                        res(ua,ub,w) += ZSelect(Z0*F*Gs(u0+u1+w),Om); 
                    }
                }
            }
        }
        // No wavefunction / potential decay mismatch
        void AddJaTo(
            Two<double> &res,
            Two<cmplx> &temp,
            const cmplx &z,
            const auto n, const auto K0, const auto K1,
            const double D,
            const Flip Om, const cmplx &Z0
        ) {
            if( res.size() < (K0+1u)*(K1+1u) )
            {
                throw std::length_error( "Not enough space for Two." );
            }

            SetF(temp, z,n,K0,K1,D);

            const ui_t KIn{ K1 > K0 ? K1 : K0 };
            const ui_t KOut{ K1 > K0 ? K0 : K1 };
            res.resize(KOut+1,KIn+1);
            for (ui_t u0 = 0; u0 <= KOut; u0++)
            {
                for (ui_t u1 = 0; u1 <= KIn; u1++)
                {
                    const auto &F{ K1 > K0 ? temp(u0,u1) : temp(u1,u0) };
                    res(u0,u1) += ZSelect(Z0*F,Om);
                }
            }
            if ( !(K1 > K0) ) { res.transpose(); }
        }


        // Case D4b: D != 0, z = 1
        // with wavefunction / potential decay mismatch
        void AddJbTo(
            OneOne<double> &res,
            One<double> &Fs,
            One<cmplx> &Gs,
            const cmplx &Z,
            const auto n, const auto K,
            const double D,
            const Flip Om, const cmplx &Z0
        ) {
            if( res.size() < ((K+1)*(K+2))/2 )
            {
                throw std::length_error( "Not enough space for OneOne." );
            }

            SetF(Fs,K,D);
            SetG(Gs,Z,n,K);

            for ( ui_t u = 0; u <= K; u++ )
            {
                for ( ui_t w = 0; w <= K-u; w++ )
                {
                    res(u,w) += ZSelect(Z0*Fs(u)*Gs(u+w),Om);
                }
            }
        }
        // No decay (only used by Naive Parallel terms)
        void AddJbTo(
            One<double> &res, One<double> &Fs,
            const auto K, const double D
        ) {
            if( res.size() < K+1U )
            {
                throw std::length_error( "Not enough space for One." );
            }

            SetF(Fs,K,D);

            for ( ui_t u = 0; u <= K; u++ ) { res(u) += Fs(u); }
        }


        // Case D4c: D = 0, z != 1
        // with wavefunction / potential decay mismatch
        void AddJcTo(
            OneOne<double> &res,
            One<cmplx> &Fs,
            One<cmplx> &Gs,
            const std::array<cmplx,2> &Zs,
            const auto n, const auto K,
            const Flip Om, const cmplx &Z0
        ) {        
            if( res.size() < ((K+1)*(K+2))/2 )
            {
                throw std::length_error( "Not enough space for OneOne." );
            }

            SetF(Fs,Zs[0],n,K);
            SetG(Gs,Zs[1],n,K);

            for (ui_t u = 0; u <= K; u++)
            {
                for (ui_t w = 0; w <= K-u; w++)
                {
                    res(u,w) += ZSelect(Z0*Fs(u)*Gs(u+w),Om);
                }
            }
        }
        // No wavefunction / potential decay mismatch
        void AddJcTo(
            One<double> &res,
            One<cmplx> &temp,
            const cmplx &z,
            const auto n, const auto K,
            const Flip Om, const cmplx &Z0
        ) {
            if( res.size() < K+1u )
            {
                throw std::length_error( "Not enough space for One." );
            }
            
            SetF(temp,z,n,K);

            for( ui_t u = 0; u <= K; u++ )
            {
                res(u) += ZSelect(Z0*temp(u),Om);
            }
        }

        // Case D4d: D = 0, z = 1
        // with wavefunction / potential decay mismatch
        void AddJdTo(
            One<double> &res,
            One<cmplx> &temp,
            const cmplx &Z,
            const auto n, const auto K,
            const Flip Om, const cmplx &Z0
        ) {
            if( res.size() < K+1 )
            {
                throw std::length_error( "Not enough space for One." );
            }

            SetG(temp,std::forward<const cmplx>(Z),n,K);

            for (ui_t w = 0; w <= K; w++)
            {
                res(w) += ZSelect(Z0*temp(w),Om);
            }
        }
        // No decay (only used by Naive Parallel terms)
        // void AddJdTo(One<double> &res, const auto K)
        void AddJdTo(One<double> &res)
        {
            if( res.size() < 1 )
            {
                throw std::length_error( "Not enough space for One." );
            }

            res(0) += 1.;
        }

    }
}