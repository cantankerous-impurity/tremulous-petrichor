namespace Integrand
{

    using ui_t = uint_fast8_t;
    using si_t = int_fast8_t;
    using N_t = std::array<ui_t,2>;
    using X_t = std::array<double,2>;

    // Forward declaration of Integrand::Calcs
    template<Operator::Spec oS, Term::Type tT, Bloch::Type bT>
    class Calcs;

}