namespace SQL
{
    template< bool ReadOnly=true >
    class Interface
    {
    protected:
        sqlite3* db{ nullptr };
        int rc {-1};

    public:

        int OpenDatabase(const char* DBPath)
        {
            if constexpr(ReadOnly)
            {
                rc = sqlite3_open_v2(DBPath,&db,SQLITE_OPEN_READONLY,NULL);
            }
            else
            {
                rc = sqlite3_open(DBPath,&db);
            }
            
            try { if( rc != SQLITE_OK ) { throw sqlite3_errmsg(db); } }
            catch (const char* e)
            {
                std::cerr << "Problem opening SQLite3 database file.";
                std::cerr << std::endl;
                std::cerr << "Error code: " << e;
                std::cerr << std::endl;
                throw e;
            }
            return rc;
        }

        int CloseDatabase()
        {
            rc = sqlite3_close(db);
            try { if( rc != SQLITE_OK ) { throw sqlite3_errmsg(db); } }
            catch (const char* e)
            {
                std::cerr << "Problem closing SQLite3 database file.";
                std::cerr << std::endl;
                std::cerr << "Error code: " << e;
                std::cerr << std::endl;
                throw e;
            }
            return rc;
        }

        int PrepareStatement(sqlite3_stmt** StmtPtr, std::string_view Query)
        {
            const size_t QLen = Query.size() + 1;

            rc = sqlite3_prepare_v2(db,std::string(Query).c_str(),QLen,StmtPtr,0);
            try { if( rc != SQLITE_OK ) { throw sqlite3_errmsg(db); } }
            catch (const char* e)
            {
                std::cerr << "Unable to prepare SQLite3 statement: ";
                std::cerr << Query;
                std::cerr << std::endl;
                std::cerr << "Error code: " << e;
                std::cerr << std::endl;
                rc = FinalizeStatement(*StmtPtr);
            }
            return rc;
        }

        int FinalizeStatement(sqlite3_stmt* Stmt)
        {
            rc = sqlite3_finalize(Stmt);
            try { if( rc != SQLITE_OK ) { throw sqlite3_errmsg(db); } }
            catch (const char* e)
            {
                std::cerr << "SQLite3 statement had errors!";
                std::cerr << std::endl;
                std::cerr << "Error code: " << e;
                std::cerr << std::endl;
            }
            return rc;
        }
    
        void EndProgram()
        {
            try
            { if( rc != SQLITE_OK ) { CloseDatabase(); } }
            catch (const char* e)
            {
                std::cerr << "Ending program.";
                std::cerr << std::endl;
                throw e;
            }
        }

        auto ErrorStatus() const { return sqlite3_errmsg(db); }
    };

}