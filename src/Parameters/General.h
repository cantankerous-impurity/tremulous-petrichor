// Useful numerical constants
constexpr double PI    { 3.14159265358979323846 };
constexpr double E     { 2.71828182845904523536 };
constexpr double logPI { 1.14472988584940017414 };
constexpr double sqrt_3{ 1.73205080756887729353 };

// Sign function, returns n/abs(n) and 0 for n = 0
template <typename T> int_fast8_t sgn(T n) { return (T(0)<n) - (n<T(0)); }

// Parity function
uint_fast8_t parity(uint_fast8_t n) { return abs(n)%2; } // even:0, odd:1

// Useful complex constants
using cmplx = std::complex<double>;
constexpr cmplx iunit{ 0.,1. };
constexpr cmplx czero{ 0.,0. };

// Defining the concept of Complex numbers for project
template < typename T >
concept Complex = std::convertible_to<T,cmplx>;


// from https://artificial-mind.net/blog/2020/10/31/constexpr-for
template< auto Start, auto End, auto Inc, class F >
constexpr void constexpr_for(F&& f)
{
    if constexpr (Start < End)
    {
        f( std::integral_constant<decltype(Start), Start>() );
        constexpr_for<Start + Inc, End, Inc>(f);
    }
}

// Test that a sequence of values { fn(0), fn(1), ..., fn(N) } are all True.
template < size_t N >
auto AllTrue { 
    [](auto fn)->bool{
        bool b{ true };
        auto lam = [&b,&fn](auto i){ b = b && fn(i); };
        constexpr_for<0, N, 1>(lam);
        return b;
    } 
};

// Generates a parameter pack of range(N) 
// < size_t(0), size_t(1), ... , size_t(N-2), size_t(N-1) >
// https://loungecpp.net/cpp/indices-trick/
template<size_t... i> struct indices_pack{};
template<size_t N, size_t... i>
struct idx_range : idx_range<N-(size_t)1, N-(size_t)1, i...>{};
template<size_t... i>
struct idx_range<0, i...> : indices_pack<i...>{};





// Enumeration representation of 'binary' physical quantities
struct Flip
{
    enum Enum { Heads=+1, Tails=-1, undef=0 };

    constexpr Flip(Enum in = undef) : _value{ in } {}
    constexpr Flip(const Flip &in) : _value{ in._value } {}
    explicit constexpr Flip(int_fast8_t i) : _value{ static_cast<Enum>(i) } {}
    constexpr Flip(bool i) : Flip(int_fast8_t(2*i-1)) {}

    constexpr operator Enum() const { return _value; }
    explicit constexpr operator bool() const
    {
        if(_value == undef) { throw; }
        return bool((int(_value)+1)/2);
    }

    constexpr bool operator==(const Enum &rhs) { return _value == rhs; }
    constexpr bool operator!=(const Flip &rhs) { return !(*this == rhs); }


    constexpr Flip& operator=(Flip other) noexcept
    {
        if (this == &other) { return *this; }
        _value = other._value;
        return *this;
    }

    constexpr friend Flip operator*(const Flip lhs,const Flip &rhs)
    {
        return Flip( int_fast8_t(int_fast8_t(lhs) * int_fast8_t(rhs)) );
    }
    
    constexpr friend Flip operator+(const Flip lhs,const Flip &rhs)
    {
        if (lhs == undef || rhs == undef) { return Flip::undef; }
        else { return Flip(sgn(int_fast8_t(lhs + rhs))); }
    }

    Enum _value;
};

// Parity function
Flip par(uint_fast8_t n) {
    return Flip( int_fast8_t(1-2*parity(n)) );
} // even:H, odd:T

template< size_t N >
auto AllFlipProduct {
    [](auto fn,const Flip x0)->bool{
        Flip x{ x0 };
        auto lam = [&x,&fn](auto i){ x = x * fn(i); };
        constexpr_for<0, N, 1>(lam);
        return x;
    } 
};


double ZSelect(const cmplx &Z, const Flip &Omega)
{
    if ( bool(Omega) ) { return std::real(Z); }
    else { return std::imag(Z); }
}


using IndexPair = std::array<size_t,2>;

// Conversions btwn Packed and Standard representations of indices.
constexpr size_t _PackIdxs(size_t t0, size_t t1){ return (t0 + ((t1+1)*t1)/2); }
constexpr size_t PackIdxs(size_t s0, size_t s1){
    size_t t0 = std::min(s0,s1);
    size_t t1 = std::max(s0,s1);
    return _PackIdxs(t0,t1);
}
constexpr size_t PackIdxs(IndexPair t) { return PackIdxs(t[0],t[1]); }
constexpr IndexPair UnpackIdx(size_t T)
{
    size_t t1 = round(sqrt(2*T+5/4.))-1;
    size_t t0 = T-(t1*(t1+1))/2;
    return IndexPair{ t0 , t1 };
}


struct PackedIndex
{
    const size_t Idx{ 0 };
    const IndexPair idxs{ 0, 0 };

    PackedIndex() = delete;
    constexpr PackedIndex(size_t I) : Idx{ I }, idxs{ UnpackIdx(I) } {}
    constexpr PackedIndex(size_t i0, size_t i1)
        : Idx{ PackIdxs(i0,i1) }, idxs{ i0,i1 } {}
    IndexPair Indices() const { return idxs; }
    size_t Index() const { return Idx; }
    size_t i0() const { return idxs[0]; }
    size_t i1() const { return idxs[1]; }
    size_t operator()() const { return Index(); }
};




class UnitVecR3
{

    const std::array<double,3> _nvec;
    const double _theta;
    const double _phi;

public:

    double x() const { return _nvec[0]; }
    double y() const { return _nvec[1]; }
    double z() const { return _nvec[2]; }

    double chi() const { return z(); }
    double theta() const { return _theta; }
    double phi() const { return _phi; }

    std::array<double,3> Arr() const { return _nvec; }

    double operator()(std::array<double,3> R) const
    {
        return R[2]*z() + R[0]*x() + R[1]*y();
    }

    double operator()(UnitVecR3 other) const
    {
        return operator()(other.Arr());
    }

    std::array<double,3> UnitArr(double chi_n, double phi_n) const
    {
        if (chi_n < -1 || chi_n > +1) { throw; }
        std::array<double,3> res;
        res[2] = chi_n;
        double st{ sqrt(1-chi_n*chi_n) };
        double cp{ cos(phi_n) };
        res[0] = st*cp;
        res[1] = st*sqrt(1-cp*cp);
        return res;
    }

    std::array<double,3> UnitArr(double x, double y, double z) const
    {
        std::array<double,3> res;
        double r{ sqrt( x*x + y*y + z*z ) };
        if(r==0.) { throw; }
        res[0] = x/r;
        res[1] = y/r;
        res[2] = z/r;
        return res;
    }

    UnitVecR3(std::array<double,3> arr) :
        _nvec{ arr },
        _theta{ acos(chi()) }, _phi{ atan2(y(),x()) }
    {}

    UnitVecR3(double chi_n, double phi_n) : UnitVecR3{ UnitArr(chi_n,phi_n) } {}
    UnitVecR3(double x, double y, double z) : UnitVecR3{ UnitArr(x,y,z) } {}

    double operator()(double chi, double phi) const
    {
        return operator()(UnitArr(chi,phi));
    }

    double Angle(double chi, double phi) const
    {
        return acos(operator()(UnitArr(chi,phi)));
    }

    double operator()(double x, double y, double z) const
    {
        return operator()(UnitArr(x,y,z));
    }

    double Angle(double x, double y, double z) const
    {
        return acos(operator()(UnitArr(x,y,z)));
    }

};





struct AnisoDirection {
    enum Enum { X,Y,Z };

    AnisoDirection() = delete;
    constexpr AnisoDirection(Enum in) : _value(in) {}
    constexpr AnisoDirection(const AnisoDirection& in) : _value{ in._value } {}

    // Allow switch and comparisons.
    constexpr operator Enum() const { return _value; }
    
    // Prevent usage: if(AnisoDirection)
    explicit operator bool() = delete;

    constexpr bool IsZ() const { return _value == Z; }
    constexpr bool IsX() const { return _value == X; }
    constexpr bool IsY() const { return _value == Y; }

    constexpr bool IsAfter(AnisoDirection in) const
    {
        switch(_value)
        {
            case AnisoDirection::X:
                return (in == AnisoDirection::Z);
            case AnisoDirection::Y:
                return (in == AnisoDirection::X);
            case AnisoDirection::Z:
                return (in == AnisoDirection::Y);
            default:
                throw;
        }
    }
    constexpr bool IsBefore(AnisoDirection in) const
    {
        switch(_value)
        {
            case AnisoDirection::X:
                return (in == AnisoDirection::Y);
            case AnisoDirection::Y:
                return (in == AnisoDirection::Z);
            case AnisoDirection::Z:
                return (in == AnisoDirection::X);
            default:
                throw;
        }
    }

    constexpr AnisoDirection& operator=(AnisoDirection other) noexcept
    {
        if (this == &other)
            return *this;
        _value = other._value;
        return *this;
    }

    constexpr Enum value() const { return _value; }

    Enum _value;
};

template< AnisoDirection dir1, AnisoDirection dir2 >
concept SameDirection = requires { requires dir1 == dir2; };

namespace Radial{

    class Type
    {
    public:
        enum Enum{ Slater=1, Gauss=2 };

        static constexpr auto name(Enum in)
        {
            switch (in)
            {
                case Slater : return "Slater";
                case Gauss : return "Gauss";
            }
        }

        const Enum value;

        Type() = delete;
        constexpr Type(Enum in) : value(in) {}

        // Allow switch and comparisons.
        constexpr operator Enum() const { return value; }

        // Prevent usage: if(RadialType)
        explicit operator bool() = delete;

        
    };

}


namespace Kinetic{

    using si_t = int_fast8_t;
    using ui_t = uint_fast8_t;

    struct Spec {
        enum Enum { II,IK,KI,KK };

        static constexpr ui_t nidxs(Enum in)
        {
            switch (in)
            {
                case II : return 0;
                case IK : return 1;
                case KI : return 1;
                case KK : return 2;
            }
        }
        static constexpr bool KL(Enum in)
        {
            switch (in)
            {
                case II : return false;
                case IK : return false;
                case KI : return true;
                case KK : return true;
            }
        }
        static constexpr bool KR(Enum in)
        {
            switch (in)
            {
                case II : return false;
                case IK : return true;
                case KI : return false;
                case KK : return true;
            }
        }
        static constexpr auto name(Enum in)
        {
            switch (in)
            {
                case II : return "II";
                case IK : return "IK";
                case KI : return "KI";
                case KK : return "KK";
            }
        }
        static constexpr auto idxs(Enum in)
        {
            switch (in)
            {
                case II : return "";      // 0
                case IK : return "s2";    // 2
                case KI : return "s1";    // 2
                case KK : return "s1,s2"; // 5
            }
        }
        static constexpr auto length(Enum in) { return (ui_t)strlen(idxs(in)); }
        
        const Enum value;

        constexpr Spec() = delete;
        constexpr Spec(Enum in) : value{ in } {}

        // Allow switch and comparisons.
        constexpr operator Enum() const { return value; }

        // Prevent usage: if(Spec)
        explicit operator bool() = delete;


        constexpr auto nidxs() const { return nidxs(value); }
        constexpr auto KL() const { return KL(value); }
        constexpr auto KR() const { return KR(value); }
        constexpr auto name() const { return name(value); }
        constexpr auto idxs() const { return idxs(value); }
        constexpr auto length() const { return length(value); }
    };

    template< Spec kS >
    concept II = requires { requires kS == Spec::II; };
    template< Spec kS >
    concept IK = requires { requires kS == Spec::IK; };
    template< Spec kS >
    concept KI = requires { requires kS == Spec::KI; };
    template< Spec kS >
    concept IK_or_KI = IK<kS> || KI<kS>;
    template< Spec kS >
    concept KK = requires { requires kS == Spec::KK; };
}