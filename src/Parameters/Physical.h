
// =============================================================================
// referenced from CODATA 2018
// -----------------------------------------------------------------------------

// [exactly defined physical constants]
constexpr double PlanckSI = 6.62607015E-34; // J / Hz
constexpr double ChargeSI = 1.602176634E-19; // C
constexpr double Lightspeed = 299792458; // m / s



// [measured physical constants]
constexpr double ElectronMassSI = 9.1093837015E-31; // kg
constexpr double FineStructure = 7.2973525693E-3; // unitless


// [silicon properties]
constexpr double aSi = 543.1020511E-12; // m
/*
================================================================================
*/

/*
================================================================================
derived quantites
--------------------------------------------------------------------------------
*/

// [physical constants]

constexpr double ElectronVolt = ChargeSI; // J


constexpr double Planck = PlanckSI/ElectronVolt;
//                      = 4.135667696E-15 ! eV / Hz


constexpr double HBarSI = PlanckSI/(2*PI);
//                      = 1.054571817E-16 J * s
constexpr double HBar = Planck/(2*PI);
//                    = 6.582119569E-16 eV * s


constexpr double ElectronMassEnergySI = ElectronMassSI*Lightspeed*Lightspeed;
//                                    = 8.1871057769E-14 J
constexpr double ElectronMassEnergy = ElectronMassEnergySI/ElectronVolt;
//                                  = 0.51099895000E+6 eV


constexpr double ElectronMass = ElectronMassEnergy/(Lightspeed*Lightspeed);
//                            = 5.6856299437E-012 eV s^2 / m^2


constexpr double MuSI = 2*FineStructure*PlanckSI/(ChargeSI*ChargeSI*Lightspeed);
//                    = 1.25663706212E-6 N / A**2
constexpr double Mu = 2*FineStructure*Planck/(ChargeSI*ChargeSI*Lightspeed);
//                  = 7.843311626525692E+12 eV / m A**2

constexpr double EpSI = 1./(MuSI*Lightspeed*Lightspeed);
//                    = 8.8541878128E-12 C**2 / J m
constexpr double Ep = 1./(Mu*Lightspeed*Lightspeed);
//                  = 1.41859728298E-30 C**2 / eV m



// [System units in vacuum]

// length [in meters]
constexpr double SystemExtent_Hyd = 
    4*PI*Ep*HBar*HBar/(ElectronMass*ChargeSI*ChargeSI);
// energy [in electronvolts]
constexpr double EnergyAU_Hyd =
    ElectronMass*ChargeSI*ChargeSI*ChargeSI*ChargeSI
    /
    (2*(4*PI*Ep*HBar)*(4*PI*Ep*HBar));



// [Silicon properties]

// wave vectors 
constexpr double k_Si = 2*PI/aSi; // 1 / m
constexpr double k_val = (0.85)*k_Si; // 1 / m

/*
k_val repeated without reference (folklore?)
likely independently calculated
a matter of determining the silicon band structure ?
*/


// anisotropic masses

constexpr double mT = 0.1905 * ElectronMass; // eV s^2 / m^2
constexpr double mL = 0.9163 * ElectronMass; // eV s^2 / m^2

constexpr double aniso_SI = 0.1905/0.9163;


struct DielectricType
{

    enum Enum {
        Static, PantelidesSah, NaraMorita, WalterCohen, RichardsonVinsome
    };

    const Enum _value;
    constexpr Enum value() const { return _value; }

    constexpr DielectricType(const Enum in) : _value{ in } {}
    constexpr DielectricType(const DielectricType& in) : _value{ in.value() } {}

    constexpr operator Enum() const { return _value; }
    explicit constexpr operator int() const { return static_cast<int>(_value); }

    constexpr bool operator==(const Enum& rhs)
    {
        return _value == rhs; 
    }
    constexpr bool operator!=(const DielectricType& rhs)
    {
        return !(*this == rhs); 
    }

    // constexpr DielectricType& operator=(DielectricType other) noexcept
    // {
    //     if (this == &other)
    //         return *this;
    //     _value = other._value;
    //     return *this;
    // }

    constexpr double StaticDielectric() const
    {
        switch (_value)
        {
            case Static:
            case PantelidesSah: return 11.4;
            case WalterCohen: return 11.3;
            case NaraMorita:
            case RichardsonVinsome: return 10.8;
        };
    }

    constexpr double A() const
    {
        switch (_value)
        {
            case Static: return 0.;
            case PantelidesSah:
            case NaraMorita: return StaticDielectric()*(1.1750);
            case WalterCohen: return StaticDielectric();
            case RichardsonVinsome: return StaticDielectric()*(0.8918);
        };
    }

    constexpr double B() const
    {
        switch (_value)
        {
            case Static:
            case WalterCohen: return 0.;
            case PantelidesSah:
            case NaraMorita: return StaticDielectric()*(-.1750);
            case RichardsonVinsome: return StaticDielectric()*(0.1082);
        };
    }

    constexpr double lamA() const
    {
        switch (_value)
        {
            case Static: return 0.;
            case PantelidesSah: 
            case NaraMorita: return 0.7572;
            case WalterCohen: return 0.9500;
            case RichardsonVinsome: return 0.9743;
        };
    }

    constexpr double lamB() const
    {
        switch (_value)
        {
            case Static:
            case WalterCohen: return 0.;
            case PantelidesSah: 
            case NaraMorita: return 0.3223;
            case RichardsonVinsome: return 0.1586;
        };
    }

    constexpr double lamC() const
    {
        switch (_value)
        {
            case Static: return 0.;
            case PantelidesSah: 
            case NaraMorita:
            case WalterCohen: return 2.044;
            case RichardsonVinsome: return 0.1586;
        };
    }

    const std::string name() const
    {
        switch (_value)
        {
            case Static: return std::string{ "Static" };
            case PantelidesSah: return std::string{ "PantelidesSah" };
            case NaraMorita: return std::string{ "NaraMorita" };
            case WalterCohen: return std::string{ "WalterCohen" };
            case RichardsonVinsome: return std::string{ "RichardsonVinsome" };
            default: return std::string{ };
        };
    }

};