namespace Td
{

    struct Operations
    {
        enum Enum {
            xyz=1,x_y_z=2,_xy_z=3,_x_yz=4,
            yzx=5,y_z_x=6,_yz_x=7,_y_zx=8,
            zxy=9,_zx_y=10,_z_xy=11,z_x_y=12,
            xzy=13,x_z_y=14,  zyx=15,_zy_x=16,  yxz=17,_y_xz=18,
            _x_zy=19,_xz_y=20,z_y_x=21,_z_yx=22,_yx_z=23,y_x_z=24
        };

        Operations() = delete;
        constexpr Operations(Enum in) : _value(in) {}
        constexpr Operations(const Operations& in) : _value(in._value) {}

        // Allow switch and comparisons.
        constexpr operator Enum() const { return _value; }
        
        // Prevent usage: if(AnisoDirection)
        explicit operator bool() = delete;

        const Enum _value;
    };

    constexpr unsigned int NumRepresentations{ 5 };

    struct Representation {

        enum Enum { A1,A2,E,T1,T2 };

        Representation() = delete;
        constexpr Representation(Enum in) : _value(in) {}
        constexpr Representation(const Representation& in) :
            _value(in._value) {}

        // Allow switch and comparisons.
        constexpr operator Enum() const { return _value; }
        
        // Prevent usage: if(AnisoDirection)
        explicit operator bool() = delete;

        constexpr auto Name() const
        {
            switch(_value)
            {
                case Representation::A1 :
                    return "A1";
                case Representation::T2 :
                    return "T2";
                case Representation::A2 :
                    return "A2";
                case Representation::T1 :
                    return "T1";
                case Representation::E :
                    return "E";
                default :
                    return "";
            }
        }

        constexpr Flip Acyclic() const
        {
            switch(_value)
            {
                case Representation::A1 :
                case Representation::T2 :
                    return Flip::Heads;
                case Representation::A2 :
                case Representation::T1 :
                    return Flip::Tails;
                case Representation::E :
                default :
                    return Flip::undef;
            }
        }

        constexpr Flip Inversion() const
        {
            switch(_value)
            {
                case Representation::A1 :
                case Representation::A2 :
                case Representation::E :
                    return Flip::Heads;
                case Representation::T1 :
                case Representation::T2 :
                default :
                    return Flip::Tails;
            }
        }

        const Enum _value;
    };

    struct Decomposition {

        enum Enum {
            A1,A2,E1,E2,T1_x,T1_y,T1_z,T2_x,T2_y,T2_z
        };

        Decomposition() = delete;
        constexpr Decomposition(Enum in) : _value(in) {}
        constexpr Decomposition(const Decomposition& in) :
            _value(in._value) {}

        // Allow switch and comparisons.
        constexpr operator Enum() const { return _value; }
        
        // Prevent usage: if(AnisoDirection)
        explicit operator bool() = delete;

        constexpr Representation Symmetry() const
        {
            switch(_value)
            {
                case Decomposition::A1 :
                    return Representation::A1;
                case Decomposition::A2 :
                    return Representation::A2;
                case Decomposition::T1_x :
                case Decomposition::T1_y :
                case Decomposition::T1_z :
                    return Representation::T1;
                case Decomposition::T2_x :
                case Decomposition::T2_y :
                case Decomposition::T2_z :
                    return Representation::T2;
                case Decomposition::E1 :
                case Decomposition::E2 :
                    return Representation::E;
                default:
                    return Representation::A1;
            }
        }

        constexpr Flip Acyclic() const { return Symmetry().Acyclic(); }
        constexpr Flip Inversion() const { return Symmetry().Inversion(); }

        const Enum _value;
    };


    struct ValleyComb{

        using enum Flip::Enum;

        enum Enum {
            A,
            E1_Even,E1_Odd,
            E2_Even,E2_Odd,
            Tx,Ty,Tz
        };

        constexpr static const cmplx eta  { 1/sqrt_3 };
        constexpr static const cmplx tau  { -.5,+.5*sqrt_3 };
        constexpr static const cmplx tauS { -.5,-.5*sqrt_3 };

        const Enum _value;

        ValleyComb() = delete;
        constexpr ValleyComb(Enum in) : _value(in) {}
        constexpr ValleyComb(const ValleyComb& in) : _value(in._value) {}

        // Allow switch and comparisons.
        constexpr operator Enum() const { return _value; }
        
        // Prevent usage: if(AnisoDirection)
        explicit operator bool() = delete;


        using valley_t = std::array<std::complex<double>,3>;

        constexpr valley_t Coefficients() const
        {
            switch(_value)
            {
                case ValleyComb::A:
                    return {eta,eta,eta};
                case ValleyComb::Tx:
                    return {1,0,0};
                case ValleyComb::Ty:
                    return {0,1,0};
                case ValleyComb::Tz:
                    return {0,0,1};
                case ValleyComb::E1_Even:
                    return {eta*tau,eta*tauS,eta};
                case ValleyComb::E1_Odd:
                    return {iunit*eta*tau,iunit*eta*tauS,iunit*eta};
                case ValleyComb::E2_Even:
                    return {eta*tauS,eta*tau,eta};
                case ValleyComb::E2_Odd:
                    return {-iunit*eta*tauS,-iunit*eta*tau,-iunit*eta};
            }
        }

        constexpr Representation Repr(Flip zeta=Heads) const
        {
            switch(_value)
            {
                case ValleyComb::A:
                    if(zeta == Heads) { return Representation::A1; }
                    if(zeta == Tails) { return Representation::A2; }
                case ValleyComb::Tx:
                case ValleyComb::Ty:
                case ValleyComb::Tz:
                    if(zeta == Heads) { return Representation::T2; }
                    if(zeta == Tails) { return Representation::T1; }
                case ValleyComb::E1_Even:
                case ValleyComb::E2_Even:
                case ValleyComb::E1_Odd:
                case ValleyComb::E2_Odd:
                    return Representation::E;
            }
        }

        constexpr Decomposition Decomp(Flip zeta=Heads) const
        {
            switch(_value)
            {
                case ValleyComb::A:
                    if(zeta == Heads) { return Decomposition::A1; }
                    if(zeta == Tails) { return Decomposition::A2; }
                case ValleyComb::Tx:
                    if(zeta == Heads) { return Decomposition::T2_x; }
                    if(zeta == Tails) { return Decomposition::T1_x; }
                case ValleyComb::Ty:
                    if(zeta == Heads) { return Decomposition::T2_y; }
                    if(zeta == Tails) { return Decomposition::T1_y; }
                case ValleyComb::Tz:
                    if(zeta == Heads) { return Decomposition::T2_z; }
                    if(zeta == Tails) { return Decomposition::T1_z; }
                case ValleyComb::E1_Even:
                case ValleyComb::E1_Odd:
                    return Decomposition::E1;
                case ValleyComb::E2_Even:
                case ValleyComb::E2_Odd:
                    return Decomposition::E2;
            }
        }
    };

    constexpr cmplx ParaPref(
        const Representation r,
        const Decomposition v0, const Flip s0,
        const Decomposition v1, const Flip s1
    ) {
        using enum Flip::Enum;
        const bool SameSigma{ s0 == s1 };
        const bool IsHeads{ r.Acyclic() == Heads };

        switch(r)
        {
            case Representation::A1:
            case Representation::A2:
                if( v0 != v1 ) { return 0.; }
                if( SameSigma != IsHeads ) { return 0.; }
                if( IsHeads ) { return 1.; }
                else {
                    switch(v0)
                    {
                        case Decomposition::E1: return iunit*double(s0);
                        case Decomposition::E2: return -iunit*double(s0);
                        default: return 1.;
                    }
                }
            default: return 0.;
        }
    }

    constexpr cmplx PerpPref(
        const Representation r,
        const Decomposition v0, const Flip s0,
        const Decomposition v1, const Flip s1
    ) {
        using enum Flip::Enum;
        const bool SameSigma{ s0 == s1 };
        const bool IsHeads{ r.Acyclic() == Heads };

        switch(r)
        {
            case Representation::A1:
            case Representation::A2:
                if( v0 != v1 ) { return 0.; }
                if( SameSigma != IsHeads ) {
                    constexpr auto C0{ sqrt_3 };
                    switch(v0)
                    {
                        case Decomposition::E1:
                            return ( IsHeads ? double(s0)*C0 : -iunit*C0 );
                        case Decomposition::E2:
                            return ( IsHeads ? double(s0)*C0 : +iunit*C0 );
                        default: return 0.;
                    }
                }
                else
                {
                    switch(v0)
                    {
                        case Decomposition::A1:
                        case Decomposition::A2:
                            return 2.;
                        case Decomposition::E1:
                            return ( IsHeads ? -1 : -double(s0)*iunit );
                        case Decomposition::E2:
                            return ( IsHeads ? -1 : +double(s0)*iunit );
                        default: return 0.;
                    }
                }
            default: return 0.;
        }
    }
}










