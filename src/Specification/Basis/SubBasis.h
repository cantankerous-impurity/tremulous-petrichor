namespace Basis
{
    template< size_t N >
    using IndexArray = std::array<Indices,N>;
    template< size_t N >
    using BasisSet_t = std::array<State,N>;

    template< size_t N >
    class SubBasis
    {
        Params_t cP;
        BasisSet_t<N> bS;
    public:
        template< size_t... iE >
        constexpr BasisSet_t<N> BuildBasis(
            IndexArray<N> iX, indices_pack<iE...>
        ) { return {{ State(iX[iE],&cP)... }}; }
        constexpr BasisSet_t<N> BuildBasis(IndexArray<N> iX)
        {
            return BuildBasis(iX,idx_range<N>{});
        }

        SubBasis() = delete;
        SubBasis(Params_t cPi, IndexArray<N> iX)
        :
            cP{ cPi }, bS{ BuildBasis(iX) }
        {}
        SubBasis(const SubBasis& x) : cP{ x.cP }, bS{ x.bS } {
            for(auto&& b: bS) { b.SetPointer(&cP); }
        }

        constexpr size_t NumStates() const { return N; }

        State& operator[](size_t m) { return bS[m]; }
        Params_t& Params() { return cP; }

    };

    void PrintWfType(std::ostream &s)
    {
        s << "WavefunctionType: " << WfType;
        s << std::endl;
    }
}