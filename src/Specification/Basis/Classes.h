namespace Basis{

    constexpr bool WfIsGauss { WfType == Radial::Type::Gauss };
    constexpr bool WfIsSlater { WfType == Radial::Type::Slater };

    using enum Flip::Enum;
    using enum Td::Decomposition::Enum;

    using ui_t = uint_fast8_t;

    using Params_t = std::tuple<Td::Decomposition,double,double>;
    using Indices = std::tuple<ui_t,ui_t,Flip,Flip>;
    using KineticScalings = std::array<double,3> ;
    class State
    {
    public:
        static constexpr double D_Numerator(const int l, const int m)
        {
            return ( l*l - m*m ) * ( (l+1)*(l+1) - m*m );
        }
        static constexpr double D_Denominator(const int l)
        {
            const double x{ 2.*l + 1. };
            return (x-2)*x*x*(x+2);
        }
        static constexpr double D_Frac(const int l, const int m)
        {
            return D_Numerator(l,m)/D_Denominator(l);
        }
        static constexpr double N_Leg(const int L, const int m)
        {
            if ( m>L ) { return 0.; }
            return sqrt( (L+0.5)*boost::math::tgamma_ratio(L-m+1,L+m+1) );
        }

        const Indices dI;
        const KineticScalings dS;
        const KineticScalings nS;

    private:
        Params_t* cP;
        KineticScalings kS;
        bool NeedsReset{ false };

    public:
        State() = delete;
        constexpr State(Indices dIi, Params_t* cPi)
        : 
            dI{ dIi }, dS{ DS_c() }, nS{ NS_c() },
            cP{ cPi }, kS{ KS_c() }
        {}

        constexpr const Indices& DI() const { return dI; }
        constexpr const Params_t& CI() const { return *cP; }
        constexpr const KineticScalings& KS() const { return kS; }
        constexpr double KS(int s) const { return KS()[s+1]; }
        constexpr const KineticScalings& NS() const { return nS; }
        constexpr double NS(int s) const { return NS()[s+1]; }
        constexpr ui_t m() const { return std::get<0>(DI()); }
        constexpr ui_t lki() const { return std::get<1>(DI()); }
        constexpr ui_t l() const { return Ordered_lks[lki()][0]; }
        constexpr ui_t k() const { return Ordered_lks[lki()][1]; }
        constexpr Flip Azimuth() const { return std::get<2>(DI()); }
        constexpr Flip Omega() const { return std::get<3>(DI()); }
        constexpr Td::Decomposition Decomp() const { return std::get<0>(CI()); }
        constexpr double radius() const { return std::get<1>(CI()); }
        constexpr double aniso() const { return std::get<2>(CI()); }

        void SetPointer(Params_t* cPi) { cP = cPi; }
        void Reset() { kS = KS_c(); }


        constexpr KineticScalings NS_c() const
        {
            const ui_t mi{ m() };
            const ui_t li{ l() };
            
            return {
                N_Leg(li-2,mi),
                N_Leg(li,mi),
                N_Leg(li+2,mi),
            };
        }
        constexpr KineticScalings DS_c() const
        {
            const ui_t mi{ m() };
            const ui_t li{ l() };
            const ui_t ki{ k() };
            const double g{ 1./(ki+2.) };
            const double X{ 2.*li*li - 2.*mi*mi + 2.*li - 1. };
            const double Y{ (2.*li-1.)*(2.*li+3.) };
            return {
                sqrt( D_Frac(li-1,mi) )*g,
                ( X/Y ),
                sqrt( D_Frac(li+1,mi) ),
            };
        }
        constexpr KineticScalings KS_c() const
        {
            const ui_t ki{ k() };
            const double f{ 1./(ki+1) };
            const double a{ aniso() };
            const double A{ Operator::mass_aniso*a*a };
            return {
                -f*(1-A)*dS[0],
                f*(1 - (1-A)*dS[1]),
                -f*(1-A)*dS[2],
            };
        }
    };

    using ParamPairPtr_t = std::array<Params_t*,2>;
    class ParameterPair
    {
        Params_t *PL, *PR;
        double *rL, *rR, *aL, *aR;
        double rLR, sLR, qL, qR;
        bool S_R, S_An, S_U;

    public:

        ParameterPair() = delete;
        ParameterPair(Params_t* PLi, Params_t* PRi) :
            PL{ PLi }, PR{ PRi },
            rL{ &std::get<1>(*PL) }, rR{ &std::get<1>(*PR) },
            aL{ &std::get<2>(*PL) }, aR{ &std::get<2>(*PR) },
            rLR{ RadAvg(*rL,*rR) }, sLR{ RadMismatch(*rL,*rR) },
            qL{ rLR/(*rL) }, qR{ rLR/(*rR) },
            S_R{ *rL == *rR }, S_An{ *aL == *aR }, S_U{ *aL == 1 && *aR == 1 }
        {}
        ParameterPair(ParamPairPtr_t P) : ParameterPair(P[0],P[1]) {}

        static constexpr double RadAvg(double r0, double r1)
        {
            if constexpr(WfIsSlater) { return (2*r0*r1) / (r0+r1); }
            if constexpr(WfIsGauss)
            {
                return sqrt( (2*r0*r0*r1*r1) / (r0*r0 + r1*r1) );
            }
        }

        static constexpr double RadMismatch(double r0, double r1)
        {
            if constexpr(WfIsSlater) { return (r1-r0) / (r0+r1); }
            if constexpr(WfIsGauss)
            {
                return (r1*r1 - r0*r0) / (r0*r0 + r1*r1);
            }
        }

        void SetL(const double r=1., const double a=1.)
        {
            *aL = a;
            *rL = r;
            Reset();
        }

        void SetR(const double r=1., const double a=1.)
        {
            *aR = a;
            *rR = r;
            Reset();
        }

        void Reset()
        {
            rLR = RadAvg(*rL,*rR);
            sLR = RadMismatch(*rL,*rR);
            qL = rLR/(*rL);
            qR = rLR/(*rR);
            S_R = ( *rL == *rR );
            S_An = ( *aL == *aR );
            S_U = ( *aL == 1 ) && ( *aR == 1 );
        }

        constexpr double RadiusLR() const { return rLR; }
        constexpr double MismatchLR() const { return sLR; }
        constexpr double RadScaleL() const { return qL; }
        constexpr double RadScaleR() const { return qR; }
        constexpr bool Same_Radius() const { return S_R; }
        constexpr bool Same_Aniso() const { return S_An; }
        constexpr bool Unity_Aniso() const { return S_U; }
    };

    using StatePairPtr_t = std::array<State*,2>;
    class StatePair
    {
        State* const Ls;
        State* const Rs;
        ParameterPair* const Pp;

    public:

        const size_t LI;
        const bool RF;

        const bool S_m;
        const bool S_A;
        const bool S_Am;

        const Flip L_P;
        const Flip A_P;

        StatePair() = delete;
        StatePair(State* Li, State* Ri, ParameterPair* Pi) :
            Ls{ Li }, Rs{ Ri }, Pp{ Pi },
            LI{ PackIdxs(Ls->lki(),Rs->lki()) },
            RF{ Rs->lki() < Ls->lki() },
            S_m{ mL() == mR() },
            S_A{ AzimuthR() == AzimuthL() },
            S_Am { S_m && S_A },
            L_P{ Flip( int_fast8_t(1 - 2*( (lL()+mL()+lR()+mR())%2 )) ) },
            A_P{ AzimuthL()*AzimuthR() }
        {}
        StatePair(StatePairPtr_t S, ParameterPair* Pi) : StatePair(S[0],S[1],Pi) {}

        constexpr ui_t mL() const { return Ls->m(); }
        constexpr ui_t lL() const { return Ls->l(); }
        constexpr ui_t kL() const { return Ls->k(); }
        constexpr Flip AzimuthL() const { return Ls->Azimuth(); }
        constexpr Flip OmegaL() const { return Ls->Omega(); }
        constexpr Td::Decomposition DecL() const { return Ls->Decomp(); }
        constexpr double AnisoL() const { return Ls->aniso(); }
        constexpr double RadiusL() const { return Ls->radius(); }
        constexpr KineticScalings KSL() const { return Ls->KS(); }
        constexpr double KSL(int s) const { return Ls->KS(s); }
        constexpr KineticScalings NSL() const { return Ls->NS(); }
        constexpr double NSL(int s) const { return Ls->NS(s); }

        constexpr ui_t mR() const { return Rs->m(); }
        constexpr ui_t lR() const { return Rs->l(); }
        constexpr ui_t kR() const { return Rs->k(); }
        constexpr Flip AzimuthR() const { return Rs->Azimuth(); }
        constexpr Flip OmegaR() const { return Rs->Omega(); }
        constexpr Td::Decomposition DecR() const { return Rs->Decomp(); }
        constexpr double AnisoR() const { return Rs->aniso(); }
        constexpr double RadiusR() const { return Rs->radius(); }
        constexpr KineticScalings KSR() const { return Rs->KS(); }
        constexpr double KSR(int s) const { return Rs->KS(s); }
        constexpr KineticScalings NSR() const { return Rs->NS(); }
        constexpr double NSR(int s) const { return Rs->NS(s); }


        constexpr ui_t m0() const { return (RF ? mR() : mL()); }
        constexpr ui_t l0() const { return (RF ? lR() : lL()); }
        constexpr ui_t k0() const { return (RF ? kR() : kL()); }
        constexpr Flip Azimuth0() const
        { return (RF ? AzimuthR() : AzimuthL()); }
        constexpr Flip Omega0() const { return (RF ? OmegaR() : OmegaL()); }
        constexpr Td::Decomposition Dec0() const
        { return (RF ? DecR() : DecL()); }
        constexpr double Aniso0() const { return (RF ? AnisoR() : AnisoL()); }
        constexpr double Radius0() const
        { return (RF ? RadiusR() : RadiusL()); }
        constexpr KineticScalings KS0() const { return (RF ? KSR() : KSL()); }
        constexpr double KS0(int s) const { return (RF ? KSR(s) : KSL(s)); }
        constexpr double NS0(int s) const { return (RF ? NSR(s) : NSL(s)); }

        constexpr ui_t m1() const { return (RF ? mL() : mR()); }
        constexpr ui_t l1() const { return (RF ? lL() : lR()); }
        constexpr ui_t k1() const { return (RF ? kL() : kR()); }
        constexpr Flip Azimuth1() const
        { return (RF ? AzimuthL() : AzimuthR()); }
        constexpr Flip Omega1() const
        { return (RF ? OmegaL() : OmegaR()); }
        constexpr Td::Decomposition Dec1() const
        { return (RF ? DecL() : DecR()); }
        constexpr double Aniso1() const { return (RF ? AnisoL() : AnisoR()); }
        constexpr double Radius1() const
        { return (RF ? RadiusL() : RadiusR()); }
        constexpr KineticScalings KS1() const { return (RF ? KSL() : KSR()); }
        constexpr double KS1(int s) const { return (RF ? KSL(s) : KSR(s)); }
        constexpr double NS1(int s) const { return (RF ? NSL(s) : NSR(s)); }


        constexpr double RadiusLR() const { return (*Pp).RadiusLR(); }
        constexpr double MismatchLR() const { return (*Pp).MismatchLR(); }
        constexpr double RadScaleL() const { return (*Pp).RadScaleL(); }
        constexpr double RadScaleR() const { return (*Pp).RadScaleR(); }
        constexpr bool Same_Radius() const { return (*Pp).Same_Radius(); }
        constexpr bool Same_Aniso() const { return (*Pp).Same_Aniso(); }
        constexpr bool Unity_Aniso() const { return (*Pp).Unity_Aniso(); }

        constexpr double Radius01() const { return RadiusLR(); }
        constexpr double Mismatch01() const
            { return (RF ? -1. : 1.)*MismatchLR(); }
        constexpr double RadScale0() const
            { return RF ? RadScaleR() : RadScaleL(); }
        constexpr double RadScale1() const
            { return RF ? RadScaleL() : RadScaleR();  }

        constexpr size_t LaguerreIndex() const { return LI; }

        constexpr bool ReverseFlag() const { return RF; }
        constexpr bool Same_m() const { return S_m; }
        constexpr bool Same_Azimuth() const { return S_A; }
        constexpr bool Same_AzimuthFunc() const { return S_Am; }


        constexpr std::complex<double> PhaseFactor() const
        {
            cmplx ph;
            if(Same_AzimuthFunc())
            {
                switch(AzimuthL())
                {
                    case Flip::Heads : ph=+1; break;
                    case Flip::Tails : ph=-1; break;
                    default : throw;
                }
            }
            else
            {
                switch(AzimuthL())
                {
                    case Flip::Heads : ph=+iunit; break;
                    case Flip::Tails : ph=-iunit; break;
                    default : throw;
                }
            }
            ph *= ( Omega0()*Omega1() ? 1. : iunit );
            return ph;
        }

        constexpr Flip LongitudinalParity() const { return L_P; }
        constexpr Flip AzimuthalParity() const { return A_P; }

        void Reset()
        {
            (*Ls).Reset();
            (*Rs).Reset();
        }
    };
}