namespace Basis
{
    class SuperBasis
    {
    public:
        using SuperBasisSet_t = std::array<State*,NumStates>;
        using ParamPairSet_t = std::array<ParameterPair,NumParamPairs>;
        using StatePairSet_t = std::array<StatePair,NumElems>;

    private:
        BasesArray baR;
        SuperBasisSet_t rF;
        ParamPairSet_t pS;
        StatePairSet_t spS;

    public:

        SuperBasis() = delete;
        SuperBasis(BasesArray baRi)
        :
            baR{ baRi },
            rF{ CollectBasis<NumStates>() },
            pS{ CollectParamPairs<NumParamPairs>() },
            spS{ CollectStatePairs<NumElems>() }
        {}

        template< size_t... iE >
        constexpr SuperBasisSet_t CollectBasis(indices_pack<iE...>)
        {
            return {{ GetState<iE>()... }};
        }
        template< size_t N >
        constexpr SuperBasisSet_t CollectBasis()
        {
            return CollectBasis(idx_range<N>{});
        }

        template< size_t... iE >
        constexpr ParamPairSet_t CollectParamPairs(indices_pack<iE...>)
        {
            return {{ ParameterPair(GetParamPair<iE>())... }};
        }
        template< size_t N >
        constexpr ParamPairSet_t CollectParamPairs()
        {
            return CollectParamPairs(idx_range<N>{});
        }

        template< size_t... iE >
        constexpr StatePairSet_t CollectStatePairs(indices_pack<iE...>)
        {
            return {{ __StatePair<iE>()... }};
        }
        template< size_t N >
        constexpr StatePairSet_t CollectStatePairs()
        {
            return CollectStatePairs(idx_range<N>{});
        }

        template< size_t N >
        StatePair __StatePair()
        {
            constexpr auto I{ UnpackIdx(N) };
            constexpr auto bn{
                PackIdxs(WhichBasis<I[0]>(),WhichBasis<I[1]>()) 
            };
            return StatePair(GetStatePair<N>(),&pS[bn]);
        }


        template< size_t n >
        auto& GetSubBasis() { return std::get<n>(baR); }
        template< size_t N >
        State* GetState()
        {
            constexpr size_t n{ WhichBasis<N>() };
            return &(GetSubBasis<n>()[N - BasisDelimeters[n]]);
        }
        template< size_t n >
        Params_t* GetParams() { return &(GetSubBasis<n>().Params());}


        template< size_t N >
        constexpr static size_t WhichBasis()
        {
            constexpr auto F{ 
                [](const size_t v) { return N >= v; }
            };

            constexpr auto it{
                std::find_if(
                    std::rbegin(BasisDelimeters),std::rend(BasisDelimeters),
                    F
                )
            };
            return (std::distance(std::begin(BasisDelimeters),it.base()) - 1);
        }

        template< size_t i0, size_t i1 >
        ParamPairPtr_t GetParamPair()
        { return { GetParams<i0>(),GetParams<i1>() }; }
        template< size_t I >
        ParamPairPtr_t GetParamPair()
        {
            constexpr auto ixs{ UnpackIdx(I) };
            return GetParamPair<ixs[0],ixs[1]>();
        }

        template< size_t i0, size_t i1 >
        StatePairPtr_t GetStatePair()
        { return {GetState<i0>(),GetState<i1>()}; }
        template< size_t I >
        StatePairPtr_t GetStatePair()
        {
            constexpr auto ixs{ UnpackIdx(I) };
            return GetStatePair<ixs[0],ixs[1]>();
        }

        StatePair& operator[](size_t N) { return spS[N]; }

        template< size_t n >
        void Set(double r=1., double a=1.)
        {
            for(size_t M = 0; M < NumParamPairs; M++ )
            {
                auto ixs{ UnpackIdx(M) };
                if( ixs[0] == n ) { pS[M].SetL(r,a); }
                else if( ixs[1] == n ) { pS[M].SetR(r,a); }
            }
            const auto M0{ BasisDelimeters[n] };
            const auto M1{ (n+1 < NumBases) ? BasisDelimeters[n+1] : NumElems };
            for(size_t M = M0; M < M1; M++ ) { spS[M].Reset(); }
        }


    };
}