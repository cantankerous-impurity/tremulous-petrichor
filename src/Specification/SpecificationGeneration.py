from os import environ
from pathlib import Path
from os.path import join as pathjoin
from os.path import isdir
from sys import exit

if not isdir(environ.get("dataDir")):
    exit("Data directory is not found.")

Path(environ.get("precDataDir")).mkdir(parents=True,exist_ok=True)
Path(environ.get("blochDataDir")).mkdir(parents=True,exist_ok=True)
Path(environ.get("resultDataDir")).mkdir(parents=True,exist_ok=True)


from SpecificationSupport import nl,tab
from SpecificationSupport import Parse_Ordered_lks
from SpecificationSupport import ParseLabels
from SpecificationSupport import ParseSymmetryReprs,ParseRadialExponents
from SpecificationSupport import ParseFuncArray,ParseDecaySpecArray
from SpecificationSupport import ParsePotsForExponential,ParsePotsForIndices
from SpecificationSupport import ParseSwitch
from SpecificationSupport import StorageDef,InitLoop

from BasisSpecification import rT
from BasisSpecification import TotalBasis
from BasisSpecification import Loc as BasisLoc
from BasisSpecification import NumBases, NumParamPairs, NumStates, NumElems
from BasisSpecification import NumLKs, NumLKPairs

from OperatorSpecification import mass_aniso, NaiveBloch, SingleValleyModel
from OperatorSpecification import SystemDielectric

from OperatorSpecification import Exps, Reprs
from OperatorSpecification import Kins, KinsX, Pots
from OperatorSpecification import Loc as OpLoc
from OperatorSpecification import PotLabels,OpLabels
from OperatorSpecification import PotDict
from OperatorSpecification import TermMask,SpecFuncs,FuncGroup
from OperatorSpecification import NPotTypes,NPotTerms,NOps
from OperatorSpecification import HamLabels,HamHamLabels
from OperatorSpecification import NHamTerms,NHHTerms

from SpecificationClasses import RadialType
rT:RadialType


# Generate the C++ header file Basis/SubBasisInfo.h ============================

Line = ""
# Line += "constexpr double mass_aniso{ " + str( mass_aniso ) +" };"+nl

# Line += 2*nl

Line += "constexpr Radial::Type WfType{ Radial::Type::"+rT.name+" };"+nl
Line += "constexpr const size_t NumBases{ "+str(NumBases)+" };"+nl
Line += "constexpr const size_t NumParamPairs{ "+str(NumParamPairs)+" };"+nl
Line += "constexpr const size_t NumStates{ "+str(NumStates)+" };"+nl
Line += "constexpr const size_t NumElems{ "+str(NumElems)+" };"+nl
Line += "constexpr size_t NumLKs{ "+str(NumLKs)+" };"+nl
Line += "constexpr size_t NumLKPairs{ "+str(NumLKPairs)+" };"+nl

Line += 2*nl

Line += "constexpr std::array<std::array<int,2>,NumLKs> Ordered_lks{{"+nl
Line += Parse_Ordered_lks(TotalBasis.O,tab)+nl
Line +="}};"


with open(pathjoin(BasisLoc,"SubBasisInfo.h"),"w") as file:
    file.write(Line)


# Generate the C++ header file Basis/SuperBasisInfo.h ==========================

Line = ""
Line += "constexpr std::array<size_t,NumBases> BasisSize{{"+nl
Line += tab+','.join(str(n) for n in TotalBasis.BasisSize)+nl
Line += "}};"+nl
Line += nl
Line += "constexpr std::array<size_t,NumBases> BasisDelimeters{{"+nl
Line += tab
N = 0
for n in TotalBasis.BasisSize:
    Line += str(N)+','
    N += n 
Line += nl
Line +="}};"+nl
Line += nl
Line += "using BasesArray = std::tuple<"+nl
Line += tab+','.join(
    "Basis::SubBasis<{}>".format(str(n)) for n in TotalBasis.BasisSize
)+nl
Line += ">;"


with open(pathjoin(BasisLoc,"SuperBasisInfo.h"),"w") as file:
    file.write(Line)


# Generate the C++ header file Basis/Spec.h ====================================

Line = ""
Line += "namespace Basis{"+nl
Line += tab+"SuperBasis CurrBases{"+nl
for l in TotalBasis.Read():
    Line += 2*tab+l+nl
Line += tab+"};"+nl
Line += "}"

with open(pathjoin(BasisLoc,"Spec.h"),"w") as file:
    file.write(Line)


# Generate the C++ header file Operator/Spec.h =================================

Line = ""
Line += "constexpr int NumPotTypes{ "  + str( NPotTypes ) +" };"+nl
Line += "constexpr int NumOperators{ " + str( NOps )      +" };"+nl
Line += "constexpr int NumHamTerms{ "  + str( NHamTerms ) +" };"+nl
Line += "constexpr int NumHHTerms{ "   + str( NHHTerms )  +" };"+nl
Line += "constexpr int NumPotTerms{ "  + str( NPotTerms ) +" };"+nl

Line += 2*nl

Line += "constexpr bool NaiveBloch{ "
Line += "true" if NaiveBloch else "false"
Line += " };"+nl
Line += "constexpr bool SingleValleyModel{ "
Line += "true" if SingleValleyModel else "false"
Line += " };"+nl

Line += 2*nl

Line += "constexpr DielectricType SystemDielectric"
Line += " { DielectricType::"+SystemDielectric.name+" };"+nl

Line += 2*nl

Line += "namespace Potential"+nl
Line += "{"+nl
Line += tab+"enum Enum"+nl
Line += tab+"{"+nl
Line += ParseLabels(PotLabels,2*tab)+nl
Line += tab+"};"+nl
Line += nl
Line += tab+"auto Id = "+"\""+"+".join(PotLabels[1:NHamTerms])+"\""+";"+nl
Line += nl
Line += ParseSymmetryReprs(Reprs,tab)+nl
Line += nl
Line += ParseRadialExponents(Exps,tab)+nl
Line += nl
Line += ParsePotsForIndices(PotDict,2*tab)+nl
Line += nl
Line += ParseFuncArray(FuncGroup,2*tab)+nl
Line += nl
Line += ParseDecaySpecArray(SpecFuncs,2*tab)+nl
Line += nl
Line += ParsePotsForExponential(TermMask,2*tab)+nl
Line += "};"+nl

Line += 2*nl

Line += "namespace Operator"+nl
Line += "{"+nl
Line += tab+"constexpr double mass_aniso{ " + str( mass_aniso ) +" };"+nl
Line += nl
Line += tab+"enum Enum"+nl
Line += tab+"{"+nl
Line += ParseLabels(OpLabels,2*tab)+nl
Line += tab+"};"+nl
Line += tab+"constexpr std::array<Enum,NumOperators> Order"+nl
Line += tab+"{{"+nl
Line += ParseLabels(OpLabels,2*tab)+nl
Line += tab+"}};"+nl
Line += tab+"constexpr std::array<Enum,NumHamTerms> HamOrder"+nl
Line += tab+"{"+nl
Line += ParseLabels(HamLabels,2*tab)+nl
Line += tab+"};"+nl
Line += tab+"constexpr std::array<Enum,NumHHTerms> HHOrder"+nl
Line += tab+"{"+nl
Line += ParseLabels(HamHamLabels,2*tab)+nl
Line += tab+"};"+nl
Line += nl
Line += tab+"static constexpr auto K(Enum in)"+nl
Line += tab+"{"+nl
Line += 2*tab+"switch (in)"+nl
Line += 2*tab+"{"+nl
Line += ParseSwitch(OpLabels,Kins,3*tab)+nl
Line += 2*tab+"}"+nl
Line += tab+"}"+nl
Line += nl
Line += tab+"static constexpr auto Kx(Enum in)"+nl
Line += tab+"{"+nl
Line += 2*tab+"switch (in)"+nl
Line += 2*tab+"{"+nl
Line += ParseSwitch(OpLabels,KinsX,3*tab)+nl
Line += 2*tab+"}"+nl
Line += tab+"}"+nl
Line += nl
Line += tab+"static constexpr auto U(Enum in)"+nl
Line += tab+"{"+nl
Line += 2*tab+"switch (in)"+nl
Line += 2*tab+"{"+nl
temp = { oL: Pots[oL] for oL in OpLabels }
Line += ParseSwitch(OpLabels,temp,3*tab)+nl
Line += 2*tab+"}"+nl
Line += tab+"}"+nl
Line += "}"

with open(pathjoin(OpLoc,"Spec.h"),"w") as file:
    file.write(Line)


# Generate the C++ header file Coeff/Loc.h =====================================

Line = ""
Line += "namespace Coeff"+nl
Line += "{"+nl
Line += tab+"const std::string DBPath{"+nl
Line += 2*tab+"\""+environ.get("precDataDir")+"/Coeffs.db\""+nl
Line += tab+"};"+nl
Line += "}"+nl

with open(pathjoin(environ.get("coeffPath"),"Loc.h"),"w") as file:
    file.write(Line)


# Generate the C++ header file Coeff/Elem.h ====================================

Line = ""
Line += "namespace Coeff"+nl
Line += "{"+nl
Line += tab+"class Element"+nl
Line += tab+"{"+nl
Line += StorageDef(2*tab)+nl
Line += tab+"public:"+nl
Line += 2*tab+"Element() = default;"+nl
Line += nl
temp = "template < Kinetic::Spec K, size_t ex, Decay::Type D >"
Line += 2*tab+temp+nl
Line += 2*tab+"Pair<K,ex,D>& Get()"+nl
Line += 2*tab+"{"+nl
Line += 3*tab+"return std::get<Pair<K,ex,D>>(_data);"+nl
Line += 2*tab+"}"+nl
Line += nl
Line += 2*tab+temp+nl
Line += 2*tab+"const Pair<K,ex,D>& Get() const"+nl
Line += 2*tab+"{"+nl
Line += 3*tab+"return std::get<Pair<K,ex,D>>(_data);"+nl
Line += 2*tab+"}"+nl
Line += nl
temp = "template < Kinetic::Spec K, size_t ex, Decay::Type D, bool dZ >"
Line += 2*tab+temp+nl
Line += 2*tab+"Interface<K,ex,D,dZ>& Get()"+nl
Line += 2*tab+"{"+nl
Line += 3*tab+"return Get<K,ex,D>().template Get<dZ>();"+nl
Line += 2*tab+"}"+nl
Line += nl
Line += 2*tab+temp+nl
Line += 2*tab+"const Interface<K,ex,D,dZ>& Get() const"+nl
Line += 2*tab+"{"+nl
Line += 3*tab+"return Get<K,ex,D>().template Get<dZ>();"+nl
Line += 2*tab+"}"+nl
Line += nl
Line += 2*tab+temp+nl
Line += 2*tab+"double value(size_t n) const"+nl
Line += 2*tab+"{"+nl
Line += 3*tab+"return Get<K,ex,D>().value<dZ>(n);"+nl
Line += 2*tab+"}"+nl
Line += 2*tab+temp+nl
Line += 2*tab+"int index(size_t n, int j) const"+nl
Line += 2*tab+"{"+nl
Line += 3*tab+"return Get<K,ex,D>().index<dZ>(n,j);"+nl
Line += 2*tab+"}"+nl
Line += nl
Line += 2*tab+"template< bool dZ >"+nl
Line += 2*tab+"void Initialize(size_t lkN)"+nl
Line += 2*tab+"{"+nl
Line += 3*tab+"IndexPair iP { UnpackIdx(lkN) };"+nl
Line += 3*tab+"Initialize<dZ>(iP[0],iP[1]);"+nl
Line += 2*tab+"}"+nl
Line += 2*tab+"template< bool dZ >"+nl
Line += 2*tab+"void Initialize(size_t lki1,size_t lki2)"+nl
Line += 2*tab+"{"+nl
Line += InitLoop(3*tab)+nl
Line += 2*tab+"}"+nl
Line += tab+"};"+nl
Line += "}"+nl


with open(pathjoin(environ.get("coeffPath"),"Elem.h"),"w") as file:
    file.write(Line)


# Generate the C++ header file Bloch/Loc.h =====================================

Line = ""
Line += "namespace Bloch"+nl
Line += "{"+nl
Line += tab+"const std::string DBPath{"+nl
Line += 2*tab+"\""+environ.get("blochDataDir")+"/Bloch.db\""+nl
Line += tab+"};"+nl
Line += "}"+nl

with open(pathjoin(environ.get("blochPath"),"Loc.h"),"w") as file:
    file.write(Line)




# Generate the C++ header file Results/Loc.h =============================

Line = ""
Line += "const std::string DataPath{"+nl
Line += tab+"\""+environ.get("resultDataDir")+"\""+nl
Line += "};"+nl

Path(environ.get("resultPath")).mkdir(parents=True,exist_ok=True)
with open(pathjoin(environ.get("resultPath"),"Loc.h"),"w") as file:
    file.write(Line)
