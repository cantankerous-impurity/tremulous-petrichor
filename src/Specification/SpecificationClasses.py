from collections import defaultdict
from enum import Enum,auto
from itertools import product
from math import pi, sqrt
from numbers import Number

from DatabaseInterface import DBInterface, ReferenceTable


PlanckSI = 6.62607015E-34      # J / Hz
ChargeSI = 1.602176634E-19     # C
Lightspeed = 299792458         # m / s

ElectronVolt = ChargeSI        # J
Planck = PlanckSI/ElectronVolt # 4.135667696E-15 eV / Hz

HBar = Planck/(2*pi)

ElectronMassSI = 9.1093837015E-31 # kg

ElectronMassEnergySI = ElectronMassSI*Lightspeed**2    # 8.1871057769E-14 J
ElectronMassEnergy = ElectronMassEnergySI/ElectronVolt # 0.51099895000E+6 eV

ElectronMass = ElectronMassEnergy/Lightspeed**2 # eV s^2 / m^2


def CPPCmplx_Str(z:Number):
    z = complex(z)
    if z.imag != 0:
        zR = z.real
        zI = z.imag
        zStr = f"cmplx({zR},{zI})"
    else:
        zStr = str(z.real)
    return zStr


class Flip(Enum):
    Heads = +1
    Tails = -1
    undef = 0

    def __repr__(self):
        return "Flip."+self.name

    @property
    def cpp(self):
        return "Flip::"+self.name

    def __str__(self):
        if (self == Flip.Heads): return "+"
        if (self == Flip.Tails): return "-"
        if (self == Flip.undef): return "0"

    def __mul__(self, other:'Flip'):
        return Flip( self.value * other.value )

class AnisoDirection(Enum):
    X = auto()
    Y = auto()
    Z = auto()

    def __repr__(self):
        return "AnisoDirection."+self.name





class RadialType(Enum):
    Slater = 1
    Gauss = 2

    def __repr__(self):
        return "RadialType."+self.name

    @property
    def namespace(self):
        return "Radial"

    @property
    def cpp_id(self):
        return "Type::"+self.name

    @property
    def cpp(self):
        return self.namespace+"::"+self.cpp_id

class Td_Representation(Enum):
    A1 = (Flip.Heads, Flip.Heads)
    A2 = (Flip.Tails, Flip.Heads)
    E  = (Flip.undef, Flip.Heads)
    T1 = (Flip.Tails, Flip.Tails)
    T2 = (Flip.Heads, Flip.Tails)

    def __repr__(self):
        return "Td_Representation."+self.name

    @property
    def namespace(self):
        return "Td"

    @property
    def cpp_id(self):
        return "Representation::"+self.name

    @property
    def cpp(self):
        return self.namespace+"::"+self.cpp_id

    @property
    def Acyclic(self)->Flip:
        return self.value[0]

    @property
    def Inversion(self)->Flip:
        return self.value[1]

class Td_Decomposition(Enum):
    A1   = (Td_Representation.A1, 1)
    A2   = (Td_Representation.A2, 1)
    E1   = (Td_Representation.E,  1)
    E2   = (Td_Representation.E,  2)
    T1_x = (Td_Representation.T1, 1)
    T1_y = (Td_Representation.T1, 2)
    T1_z = (Td_Representation.T1, 3)
    T2_x = (Td_Representation.T2, 1)
    T2_y = (Td_Representation.T2, 2)
    T2_z = (Td_Representation.T2, 3)

    def __repr__(self): return "Td_Decomposition."+self.name
    def __hash__(self): return hash(repr(self))
    def __str__(self): return self.cpp

    def __eq__(self,other):
        if type(other) is type(self):
            return self.__dict__ == other.__dict__
        else:
            return False

    @property
    def namespace(self):
        return "Td"

    @property
    def cpp_id(self):
        return "Decomposition::"+self.name

    @property
    def cpp(self):
        return self.namespace+"::"+self.cpp_id

    @property
    def Acyclic(self):
        self.value:tuple[Td_Representation,int]
        return self.value[0].Acyclic

    @property
    def Inversion(self):
        self.value:tuple[Td_Representation,int]
        return self.value[0].Inversion


eta = 1/sqrt(3)
tau = 0.5*complex(-1,sqrt(3))
tauS = 0.5*complex(-1,-sqrt(3))

A_comb = (eta,eta,eta,)
Tx_comb = (1,0,0,)
Ty_comb = (0,1,0,)
Tz_comb = (0,0,1,)
E1_Even_comb = (eta*tau,eta*tauS,eta)
E1_Odd_comb = tuple(1j*x for x in E1_Even_comb)
E2_Even_comb = (eta*tauS,eta*tau,eta)
E2_Odd_comb = tuple(-1j*x for x in E2_Even_comb)

class Valley_Combination(Enum):
    A  = A_comb
    E1_Even = E1_Even_comb
    E1_Odd  = E1_Odd_comb
    E2_Even = E2_Even_comb
    E2_Odd  = E2_Odd_comb
    Tx = Tx_comb
    Ty = Ty_comb
    Tz = Tz_comb


    def __repr__(self):
        return "Valley_Combination."+self.name

    @property
    def namespace(self):
        return "Td"

    @property
    def cpp_id(self):
        return "ValleyComb::"+self.name

    @property
    def cpp(self):
        return self.namespace+"::"+self.cpp_id

    @property
    def Acyclic(self):
        self.value:tuple[Td_Decomposition,tuple[Number,Number,Number]]
        s = self.name
        if ( s == "E1_Even" or s == "E2_Even" ):
            return Flip.Heads
        if ( s == "E1_Odd" or s == "E2_Odd" ):
            return Flip.Tails
        else:
            return self.value[0].Acyclic

    @property
    def Inversion(self):
        self.value:tuple[Td_Decomposition,tuple[Number,Number,Number]]
        return self.value[0].Inversion




from functools import lru_cache

@lru_cache
def D_Numerator(l,m):
    return ( (l)**2 - m**2 ) * ( (l+1)**2 - m**2 )
@lru_cache
def D_Denominator(l):
    return (2*l-1)*(2*l+3)*(2*l+1)**2
@lru_cache
def D_Frac(l,m):
    return D_Numerator(l,m)/D_Denominator(l)
@lru_cache
def D_sC(m,l,k,s):
    if s == +1:
        res = sqrt( D_Frac(l+1,m) )
        res /= k+1
        return res
    if s == 0:
        res = 2*l**2 - 2*m**2 + 2*l - 1
        res /= (k+1)*(2*l-1)*(2*l+3)
        return res
    if s == -1:
        res = sqrt( D_Frac(l-1,m) )
        res /= (k+1)*(k+2)
        return res
@lru_cache
def K_sC(A,m,l,k):
    res = [ -(1-A)*D_sC(m,l,k,s) for s in (-1,0,1) ]
    res[1] += 1/(k+1)
    return tuple(res)
@lru_cache
def Kinetic_Scaling(m,l,k,ma,sa):
    return K_sC(ma*(sa**2),m,l,k)




basisSpec_t = tuple[int,int,int]
basisSpec_tuple = tuple[basisSpec_t,...]
keyPart_t = tuple[int,int]
keyPart_tuple = tuple[keyPart_t,...]

@lru_cache
def Distinct_lks(B:basisSpec_tuple)->keyPart_tuple:
    return tuple(sorted({ (l,k) for (_,l,k,) in B }))

class Ordered_lks:

    def __init__(self,B:basisSpec_tuple,DB:DBInterface):
        L = Distinct_lks(B)
        T = ReferenceTable(DB,'lk','l,k')

        T.Insert(L) # hopefully this doesn't add too much
        D0 = { 
            rowid:(l,k)
            for rowid,l,k in T.GetReferenceRows(L)
        }
        D1 = { v:k for k,v in D0.items() }
        D = { **D0, **D1 }
        I = tuple(sorted(D[lk] for lk in L))

        self.lks = tuple( D[i] for i in I )
        self.DB = DB
        self.NumLKs = len(self.lks)

    def __call__(self)->keyPart_tuple: return self.lks
    def __len__(self): return self.NumLKs
    def __index__(self,X): return self.lks.index(X)
    def __getitem__(self,i): return self.lks[i]
    
    def index(self,X): return self.__index__(X)
    def DB(self): return self.DB






mT = 0.1905 * ElectronMass # eV s^2 / m^2

class DielectricType(Enum):
    Static = auto()
    PantelidesSah = auto()
    NaraMorita = auto()
    WalterCohen = auto()
    RichardsonVinsome = auto()

    def __repr__(self):
        return "DielectricType."+self.name

    def StaticDielectric(self):
        if self == DielectricType.Static:
            return 11.4
        if self == DielectricType.PantelidesSah:
            return 11.3
        if self == DielectricType.NaraMorita:
            return 10.8
        if self == DielectricType.WalterCohen:
            return 11.3
        if self == DielectricType.RichardsonVinsome:
            return 10.8

    def A(self):
        if self == DielectricType.Static:
            return 0.
        if self == DielectricType.PantelidesSah:
            return self.StaticDielectric()*(1.1750)
        if self == DielectricType.NaraMorita:
            return self.StaticDielectric()*(1.1750)
        if self == DielectricType.WalterCohen:
            return self.StaticDielectric()
        if self == DielectricType.RichardsonVinsome:
            return self.StaticDielectric()*(0.8918)

    def B(self):
        if self == DielectricType.Static:
            return 0.
        if self == DielectricType.PantelidesSah:
            return self.StaticDielectric()*(-.1750)
        if self == DielectricType.NaraMorita:
            return self.StaticDielectric()*(-.1750)
        if self == DielectricType.WalterCohen:
            return 0.
        if self == DielectricType.RichardsonVinsome:
            return self.StaticDielectric()*(0.1082)

    def C(self):
        if self == DielectricType.Static:
            return 0.
        if self == DielectricType.PantelidesSah:
            return -1.000
        if self == DielectricType.NaraMorita:
            return -1.000
        if self == DielectricType.WalterCohen:
            return -1.000
        if self == DielectricType.RichardsonVinsome:
            return -1.000

    def lamA(self):
        if self == DielectricType.Static:
            return 0.
        if self == DielectricType.PantelidesSah:
            return .7572
        if self == DielectricType.NaraMorita:
            return .7572
        if self == DielectricType.WalterCohen:
            return .9500
        if self == DielectricType.RichardsonVinsome:
            return .9743

    def lamB(self):
        if self == DielectricType.Static:
            return 0.
        if self == DielectricType.PantelidesSah:
            return .3223
        if self == DielectricType.NaraMorita:
            return .3223
        if self == DielectricType.WalterCohen:
            return 0.
        if self == DielectricType.RichardsonVinsome:
            return .1586

    def lamC(self):
        if self == DielectricType.Static:
            return 0.
        if self == DielectricType.PantelidesSah:
            return 2.044
        if self == DielectricType.NaraMorita:
            return 2.044
        if self == DielectricType.WalterCohen:
            return 2.044
        if self == DielectricType.RichardsonVinsome:
            return .1586

class DecayType(Enum):
    Zero = auto()
    Yukawa = auto()
    Gauss = auto()
    YukGau = auto()

    def __repr__(self):
        return "DecayType."+self.name

    @property
    def namespace(self):
        return "Decay"

    @property
    def cpp_id(self):
        return "Type::"+self.name

    @staticmethod
    def AddBloch(x:"DecayType"):
        if(x == DecayType.Zero) : return (DecayType.Zero,DecayType.Yukawa,)
        if(x == DecayType.Gauss) : return (DecayType.Gauss,DecayType.YukGau,)
        if(x == DecayType.Yukawa) : return (DecayType.Yukawa,)
        if(x == DecayType.YukGau) : return (DecayType.YukGau,)

    @property
    def BlochTuple(self:"DecayType")->tuple["DecayType","DecayType"]:
        return DecayType.AddBloch(self)

    @property
    def cpp(self):
        return self.namespace+"::"+self.cpp_id

    def __add__(self,other):

        if self == DecayType.Zero and isinstance(other,DecayType):
            return DecayType(other.value)
        if other == DecayType.Zero:
            return DecayType(self.value)

        if (self == DecayType.Yukawa) and (other == DecayType.Yukawa):
            return DecayType.Yukawa

        if (self == DecayType.Gauss) and (other == DecayType.Gauss):
            return DecayType.Gauss

        if (
            ((self == DecayType.Gauss) and (other == DecayType.Yukawa))
            or
            ((self == DecayType.Yukawa) and (other == DecayType.Gauss))
        ):
            return DecayType.YukGau

        raise ValueError("These two objects cannot be added.")

    def __radd__(self,other):
        return self.__add__(other)

class DecaySpec:

    def __repr__(self):
        if self.dec_type == DecayType.Zero:
            return 'Spec()'
        if self.dec_type == DecayType.Yukawa:
            return 'Spec(Decay::Type::Yukawa,{})'.format(self.a_SAfunc.cpp_id)
        if self.dec_type == DecayType.Gauss:
            return 'Spec(Decay::Type::Gauss,{})'.format(self.b_SAfunc.cpp_id)
        if self.dec_type == DecayType.YukGau:
            temp = ( self.a_SAfunc.cpp_id, self.b_SAfunc.cpp_id, )
            return 'Spec({})'.format(','.join(temp))

    @property
    def namespace(self):
        return "Decay"

    @property
    def cpp_id(self):
        return self.__repr__()

    @property
    def cpp(self):
        return self.namespace+"::"+self.cpp_id
    
    @property
    def func(self):
        if self.dec_type == DecayType.Zero:
            return SolidAngleFunc()
        if self.dec_type == DecayType.Yukawa:
            return self.a_SAfunc
        if self.dec_type == DecayType.Gauss:
            return self.b_SAfunc
        if self.dec_type == DecayType.YukGau:
            return ( self.a_SAfunc, self.b_SAfunc, )

    def __hash__(self):
        if self.dec_type == DecayType.Zero:
            return hash(0)
        if self.dec_type == DecayType.Yukawa:
            return hash((DecayType.Yukawa,hash(self.a_SAfunc),))
        if self.dec_type == DecayType.Gauss:
            return hash((DecayType.Gauss,hash(self.b_SAfunc),))
        if self.dec_type == DecayType.YukGau:
            temp = (
                DecayType.YukGau, hash(self.a_SAfunc), hash(self.b_SAfunc), 
            )
            return hash(temp)

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if hash(self) == hash(other):
            return True
        else:
            return False

    def __mul__(self,other):
        if isinstance(other,DecaySpec):
            new_a_SAfunc = self.a_SAfunc + other.a_SAfunc
            new_b_SAfunc = self.b_SAfunc + other.b_SAfunc
            return DecaySpec(new_a_SAfunc,new_b_SAfunc)

        raise ValueError("These two objects cannot be multiplied.")

    def __rmul__(self,other):
        return self.__mul__(other)

    def __init__(self,*x):
        self.dec_type:DecayType

        self.a_SAfunc = SolidAngleFunc(0) # Yukawa decay function
        self.b_SAfunc = SolidAngleFunc(0) # Gauss decay constant
        self.dec_type = DecayType.Zero

        # default
        if len(x) == 0:
            return

        if len(x) == 1:
            # DecaySpec(DecayType.Zero) equivalent to default
            if not isinstance(x[0],DecayType):
                raise TypeError("Unable to determine decay type.")
            if x != DecayType.Zero:
                raise ValueError(
                    "Nonzero decay type, "
                    +"but no decay constant provided."
                )
            return

        if len(x) == 2:
            # DecaySpec(dt:DecayType,c:Number)
            if isinstance(x[0],DecayType) and isinstance(x[1],Number):
                try:
                    self.dec_type = x[0]
                    if(x[0] == DecayType.Yukawa):
                        self.a_SAfunc = SolidAngleFunc(complex(x[1]))
                        return
                    if(x[0] == DecayType.Gaussian):
                        self.b_SAfunc = SolidAngleFunc(float(x[1].real))
                        return
                    raise ValueError(
                        "Decay type inconsistent with provided constants."
                    )
                except TypeError:
                    print(
                        "The provided decay constant "
                        +"is not the correct type."
                    )
                    exit()
            # DecaySpec(a:complex,b:float)
            if isinstance(x[0],Number) and isinstance(x[1],Number):
                if x[0] == 0. and x[1] == 0.:
                    # DecaySpec(0.,0.) equivalent to default
                    return
                if x[0] != 0. and x[1] != 0.:
                    # both decay constants specified, no ang. dep specified.
                    self.dec_type = DecayType.YukGau
                    try:
                        self.a_SAfunc = SolidAngleFunc(complex(x[1]))
                        self.b_SAfunc = SolidAngleFunc(float(x[1].real))
                        return
                    except TypeError:
                        print(
                            "The provided decay constants "
                            +"are not the correct type."
                        )
                        exit()
                if x[0] != 0.:
                    # DecaySpec(a:complex,0.)
                    # Yukawa decay constant specified, no ang. dep specified.
                    self.dec_type = DecayType.Yukawa
                    try:
                        self.a_SAfunc = SolidAngleFunc(complex(x[1]))
                        return
                    except TypeError:
                        print(
                            "The provided decay constant "
                            +"is not the correct type."
                        )
                        exit()
                if x[1] != 0.:
                    # DecaySpec(0.,b:float)
                    # Gauss decay constant specified, no ang. dep specified.
                    self.dec_type = DecayType.Gaussian
                    try:
                        self.b_SAfunc = SolidAngleFunc(float(x[1].real))
                        return
                    except TypeError:
                        print(
                            "The provided decay constant "
                            +"is not the correct type."
                        )
                        exit()
            # DecaySpec(dt:DecayType,SA:func)
            if isinstance(x[0],DecayType) and isinstance(x[1],SolidAngleFunc):
                self.dec_type = x[0]
                if(x[0] == DecayType.Yukawa):
                    self.a_SAfunc = x[1]
                if(x[0] == DecayType.Gaussian):
                    self.b_SAfunc = x[1]
                if(x[0] == DecayType.YukGau):
                    raise ValueError(
                        "Decay type inconsistent with provided function."
                    )
                if(x[0] == DecayType.Zero and x[1] != SolidAngleFunc(0) ):
                    raise ValueError(
                        "Decay type inconsistent with provided function."
                    )
            # DecaySpec(a_SA:func,b_SA:func)
            if (
                isinstance(x[0],SolidAngleFunc)
                and
                isinstance(x[1],SolidAngleFunc)
            ):
                self.a_SAfunc = x[0]
                self.b_SAfunc = x[1]
                if not x[0].empty and not x[1].empty:
                    self.dec_type = DecayType.YukGau
                    return
                if not x[0].empty and not x[1].empty:
                    self.dec_type = DecayType.Zero
                    return
                if not x[0].empty and x[1].empty:
                    self.dec_type = DecayType.Yukawa
                    return
                if x[0].empty and not x[1].empty:
                    self.dec_type = DecayType.Gauss
                    return
            # return


class SolidAngleDep(Enum):
    Unity = auto()
    FieldX = auto()
    FieldY = auto()
    FieldZ = auto()
    FieldXX = auto()
    FieldXY = auto()
    FieldXZ = auto()
    FieldYY = auto()
    FieldYZ = auto()
    FieldZZ = auto()

    @property
    @classmethod
    def NumDeps():
        return len(SolidAngleDep.__members__)

    def __repr__(self):
        return "SolidAngleDep."+self.name

    @property
    def namespace(self):
        return "SolidAngle"

    @property
    def cpp_id(self):
        return "Dependence::"+self.name

    @property
    def cpp(self):
        return self.namespace+"::"+self.cpp_id

    def __mul__(self,other):

        if self == SolidAngleDep.Unity and isinstance(other,SolidAngleDep):
            return SolidAngleDep(other.value)
        if other == SolidAngleDep.Unity:
            return SolidAngleDep(self.value)

        if (self == SolidAngleDep.FieldX) and (other == SolidAngleDep.FieldX):
            return SolidAngleDep.FieldXX
        if (self == SolidAngleDep.FieldX) and (other == SolidAngleDep.FieldY):
            return SolidAngleDep.FieldXY
        if (self == SolidAngleDep.FieldX) and (other == SolidAngleDep.FieldZ):
            return SolidAngleDep.FieldXZ

        if (self == SolidAngleDep.FieldY) and (other == SolidAngleDep.FieldX):
            return SolidAngleDep.FieldXY
        if (self == SolidAngleDep.FieldY) and (other == SolidAngleDep.FieldY):
            return SolidAngleDep.FieldYY
        if (self == SolidAngleDep.FieldY) and (other == SolidAngleDep.FieldZ):
            return SolidAngleDep.FieldYZ

        if (self == SolidAngleDep.FieldZ) and (other == SolidAngleDep.FieldX):
            return SolidAngleDep.FieldXZ
        if (self == SolidAngleDep.FieldZ) and (other == SolidAngleDep.FieldY):
            return SolidAngleDep.FieldYZ
        if (self == SolidAngleDep.FieldZ) and (other == SolidAngleDep.FieldZ):
            return SolidAngleDep.FieldZZ

        raise ValueError("These two objects cannot be multiplied.")

    def __rmul__(self,other):
        return self.__mul__(other)


class SolidAngleFunc:
    def __init__(self,d=None):
        self.data:defaultdict[SolidAngleDep,Number]
        if isinstance(d,SolidAngleFunc):
            self.data = d.data
        elif isinstance(d,dict):
            d:dict[SolidAngleDep,Number]
            x = { k for k,v in d.items() if v == 0 }
            for k in x:
                d.pop(k)
            self.data = defaultdict(int,d)
        elif isinstance(d,list) or isinstance(d,tuple):
            self.data = defaultdict(int)
            for coeff,dep in d:
                if coeff != 0:
                    self.data[dep] += coeff
        elif isinstance(d,Number):
                self.data = defaultdict(int)
                self.data[SolidAngleDep.Unity] = d
        elif d is None:
            self.data = defaultdict(int)
            self.data[SolidAngleDep.Unity] = 0

    @property
    def len(self):
        return len(self.data)

    @property
    def constant(self):
        return all(  
            v == 0 for k,v in self.data.items() if k != SolidAngleDep.Unity
        )

    @property
    def raw_array(self):
        return tuple( self.data[k] for k in SolidAngleDep )

    @property
    def empty(self):
        return ( self.constant and self.data[SolidAngleDep.Unity] == 0 )

    @property
    def namespace(self):
        return "SolidAngle"

    @property
    def cpp_id(self):

        if self.constant:
            s = CPPCmplx_Str(self.data[SolidAngleDep.Unity])
            return "Func({})".format(s)
        else:
            x = defaultdict(lambda:"0",
                { k:CPPCmplx_Str(v) for k,v in self.data.items() }
            )
            s = ','.join( x[k] for k in SolidAngleDep  if k in self.data)
            return "Func({{{}}})".format(s)

    @property
    def cpp(self):
        return self.namespace+"::"+self.cpp_id

    def __repr__(self):
        return "SolidAngleDep."+self.cpp_id

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if self.raw_array == other.raw_array:
            return True
        else:
            return False

    def __hash__(self):
        return hash(self.raw_array)

    def __add__(self,other):
        s = self.data

        if isinstance(other,SolidAngleFunc):
            o = other.data
            if not s: return SolidAngleFunc(o)
            if not o: return SolidAngleFunc(s)

            x = defaultdict(int)
            for ds,vs in s.items():
                x[ds] += vs
            for do,vo in o.items():
                x[do] += vo
                
            return SolidAngleFunc(x)

        raise ValueError("These two objects cannot be multiplied.")

    def __radd__(self,other):
        return self.__add__(other)

    def __mul__(self,other):
        s = self.data
        
        if isinstance(other,Number):
            x = dict(self.data)
            for key in x:
                x[key] *= other
            return SolidAngleFunc(x)

        elif isinstance(other,SolidAngleFunc):
            o = other.data
            if not s: return SolidAngleFunc(o)
            if not o: return SolidAngleFunc(s)

            x = defaultdict(lambda : 0)
            for ds,do in product(s,o):
                x[ds*do] += s[ds]*o[do]
                
            return SolidAngleFunc(x)

        raise ValueError("These two objects cannot be multiplied.")

    def __rmul__(self,other):
        return self.__mul__(other)