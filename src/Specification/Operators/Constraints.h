namespace Operator
{
    template< Spec oS >
    concept II = Kinetic::II<oS.K>;
    template< Spec oS >
    concept IK = Kinetic::IK<oS.K>;
    template< Spec oS >
    concept KI = Kinetic::KI<oS.K>;
    template< Spec oS >
    concept Kin = IK<oS> || KI<oS>;
    template< Spec oS >
    concept KK = Kinetic::KK<oS.K>;

    template< Spec oS >
    concept NoPot = requires
    {
        requires ( oS == Overlap || oS == Kinetic || oS == KinKin );
    };
    template< Spec oS >
    concept Pot = !NoPot<oS>;
    template< Spec oS >
    concept KinPot = Kin<oS> && Pot<oS>;
    

    // template< Spec oS, Bloch::Type bT >
    // concept NoPotAndIsTrunc = ( NoPot<oS> && Trunc<bT> );
    // template< Spec oS, Bloch::Type bT >
    // concept PotOrIsNaive = ( Pot<oS> || Naive<bT> );

    namespace Symmetry
    {
        template< Spec oS >
        concept Azi = requires { requires IsAzimuthallyInvariant<oS>(); };

        template< Spec oS >
        concept Sphere = requires { requires IsSphericallyInvariant<oS>(); };

        template< Spec oS >
        concept Flat = requires { requires IsFlat<oS>(); };

        template< Spec oS >
        concept Isotropic = requires { requires IsIsotropic<oS>(); };

        template< Spec oS >
        concept FlatOrIso = Flat<oS> || Isotropic<oS>;
    }

    // namespace Decay
    // {

    //     template< Spec oS, size_t i >
    //     concept Zero = ::Decay::Zero<oS.Decay(i)>;

    //     template< Spec oS, size_t i >
    //     concept NonZero = ::Decay::NonZero<oS.Decay(i)>;

    //     template< Spec oS, size_t i >
    //     concept Combination = ::Decay::Combination<oS.Decay(i)>;

    //     template< Spec oS, size_t i >
    //     concept Yukawa = ::Decay::Yukawa<oS.Decay(i)>;

    //     template< Spec oS, size_t i >
    //     concept Gauss = ::Decay::Gauss<oS.Decay(i)>;

    //     template< Spec oS, size_t i >
    //     concept Wf_Match = ::Decay::Wf_Match<oS.Decay(i)>;

    //     template< Spec oS, size_t i >
    //     concept Wf_Mismatch = ::Decay::Wf_Mismatch<oS.Decay(i)>;
    // }

    // template< Spec oS, Term::Type tT, Bloch::Type bT, size_t ui >
    // concept ZeroDecay = ( Decay::Zero<oS,ui> && NaivePara<tT,bT> );
    // template< Spec oS, Term::Type tT, Bloch::Type bT, size_t ui >
    // concept NonZeroDecay = !ZeroDecay<oS,tT,bT,ui>;
    // template< Spec oS, Term::Type tT, Bloch::Type bT, size_t ui >
    // concept _CD = ( Decay::Gauss<oS,ui> && !NaivePara<tT,bT> );
    // template< Spec oS, Term::Type tT, Bloch::Type bT, size_t ui >
    // concept Combination = ( Decay::Combination<oS,ui> || _CD<oS,tT,bT,ui> );

    // template< Spec oS, Term::Type tT, Bloch::Type bT, size_t ui >
    // concept ZeroDecay = ( ::Decay::Zero<oS.Decay(ui)> && NaivePara<tT,bT> );
    // template< Spec oS, Term::Type tT, Bloch::Type bT, size_t ui >
    // concept NonZeroDecay = !ZeroDecay<oS,tT,bT,ui>;
    // template< Spec oS, Term::Type tT, Bloch::Type bT, size_t ui >
    // concept _CD = ( ::Decay::Gauss<oS.Decay(ui)> && !NaivePara<tT,bT> );
    // template< Spec oS, Term::Type tT, Bloch::Type bT, size_t ui >
    // concept Combination = (
    //     ::Decay::Combination<oS.Decay(ui)> || _CD<oS,tT,bT,ui>
    // );


    template< Term::Type tT, Bloch::Type bT >
    concept _Mismatch = ( !Basis::WfIsSlater && !NaivePara<tT,bT> );

    // template< Spec oS, size_t i, Term::Type tT, Bloch::Type bT >
    // concept Wf_Mismatch = (
    //     ::Decay::Wf_Mismatch<oS.Decay(i)> || _Mismatch<tT,bT>
    // );


    template< Term::Type tT, Bloch::Type bT >
    concept _Match = ( Basis::WfIsSlater && !NaivePara<tT,bT> );


    // template< Spec oS, size_t i, Term::Type tT, Bloch::Type bT >
    // concept Wf_Match = ( ::Decay::Wf_Match<oS.Decay(i)> || _Match<tT,bT> );
}