namespace Decay
{
    template< Type D >
    concept _YY = ( Yukawa<D> && Basis::WfIsSlater );
    template< Type D >
    concept _GG = ( Gauss<D> && Basis::WfIsGauss );
    template< Type D >
    concept _YG = ( Yukawa<D> && Basis::WfIsGauss );
    template< Type D >
    concept _GY = ( Gauss<D> && Basis::WfIsSlater );

    template< Type D >
    concept Wf_Match = ( _YY<D> || _GG<D> );
    template< Type D >
    concept Wf_Mismatch = ( _YG<D> || _GY<D> );
    
    using Z_t = std::array<cmplx,2>;

    template< Type dT, Term::Type tT, Bloch::Type bT >
    constexpr Type EffectiveType()
    { 
        if constexpr( NaivePara<tT,bT> ) { return dT; }
        else
        {
            if constexpr( dT == Type::Zero || dT == Type::Yukawa )
            {
                return Type::Yukawa;
            }
            if constexpr( dT == Type::Gauss || dT == Type::YukGau )
            {
                return Type::YukGau;
            }
        }
    };

    cmplx Z_Alpha(const double eta, const cmplx &alpha)
    {
        if constexpr( Basis::WfIsSlater ) { return 1./(1.+0.5*alpha*eta); }
        if constexpr( Basis::WfIsGauss ) { return 0.5*alpha*eta; }
    }

    double Z_Beta(const double eta, const double beta)
    {
        if constexpr( Basis::WfIsSlater ) { return 1./sqrt(beta*eta*eta); }
        if constexpr( Basis::WfIsGauss ) { return 1./(1.+beta*eta*eta); }
    }

    Z_t Z_AlphaBeta(
        const double eta, const cmplx &alpha, const double beta
    ) {
        cmplx A{ 0.5*alpha*eta };
        double B{ beta*eta*eta };
        if constexpr( Basis::WfIsSlater )
        { return { 1./(1.+A), (1.+A)/sqrt(B) }; }
        if constexpr( Basis::WfIsGauss )
        { return { 1./(1.+B), A/sqrt(1.+B) }; }
    }

    // template< Spec dS, Term::Type tT, Bloch::Type bT, typename... Types >
    // Z_t Z(
    //     const SolidAngle::Func &f, const double eta,
    //     [[maybe_unused]]Types... args
    // ) {
    //     const auto &&_da{ dS._da+f };
    //     const auto &&_dS{ Spec(_da,dS._db) };
    //     constexpr Type dT{ EffectiveType<dS,tT,bT>() };
    //     if constexpr( dT.UsesZAlpha() )
    //     {
    //         const cmplx &&A{ _dS.YukawaDecay()(args...) };
    //         return { Z_Alpha(eta,A), 0. };
    //     }
    //     else if constexpr( dT.UsesZBeta() )
    //     {
    //         const double B{ (_dS.GaussianDecay()(args...)).real() };
    //         return { 0., Z_Beta(eta,B) };
    //     }
    //     else if constexpr( dT.UsesZAlphaBeta() )
    //     {
    //         const cmplx &&A{ _dS.YukawaDecay()(args...) };
    //         const double B{ (_dS.GaussianDecay()(args...)).real() };
    //         return Z_AlphaBeta(eta,A,B);
    //     }
    //     return {0.,0.};
    // }

    template<
        const SolidAngle::Func &f, Spec dS, 
        Term::Type tT, Bloch::Type bT,
        typename... Types 
    >
    Z_t Z(const double eta, [[maybe_unused]]Types... args)
    {
        const auto &&_da{ dS._da+f };
        const auto &&_dS{ Spec(_da,dS._db) };
        constexpr Type dT{ EffectiveType<dS,tT,bT>() };
        if constexpr( dT.UsesZAlpha() )
        {
            const cmplx &&A{ _dS.YukawaDecay()(args...) };
            return { Z_Alpha(eta,A), 0. };
        }
        else if constexpr( dT.UsesZBeta() )
        {
            const double B{ (_dS.GaussianDecay()(args...)).real() };
            return { 0., Z_Beta(eta,B) };
        }
        else if constexpr( dT.UsesZAlphaBeta() )
        {
            const cmplx &&A{ _dS.YukawaDecay()(args...) };
            const double B{ (_dS.GaussianDecay()(args...)).real() };
            return Z_AlphaBeta(eta,A,B);
        }
        return {0.,0.};
    }
}