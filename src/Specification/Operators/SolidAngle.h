namespace SolidAngle
{

    using ui_t = uint_fast8_t;

    struct Func; // Forward declaration
    struct Dependence
    {

        enum Enum
        {
            Unity=0,
            FieldZ=1,FieldZZ=2,
            FieldX=3,FieldY=4,
            FieldXX=5,FieldYY=6,
            FieldXY=7,FieldXZ=8,FieldYZ=9
        };
        static constexpr ui_t NumDeps = 10;

        static constexpr std::array<Enum,NumDeps> Order 
        {
            Unity,
            FieldZ,FieldX,FieldY,
            FieldXX,FieldYY,FieldZZ,
            FieldXY,FieldXZ,FieldYZ
        };

        Dependence() = delete;
        constexpr Dependence(Enum in) : _value(in) {}
        const Enum _value;


        // Allow switch and comparisons.
        constexpr operator Enum() const { return _value; }
        constexpr operator size_t() const
        {
            return static_cast<size_t>(_value); 
        }

        // Prevent usage: if(OperatorSpec)
        explicit operator bool() = delete;

        constexpr double operator()() const {
            switch (_value)
            {
                case Unity :
                    return 1.;
                default :
                    throw;
            }
        }

        constexpr double operator()(double chi) const {
            switch (_value)
            {
                case FieldZ :
                    return chi;
                case FieldZZ :
                    return chi*chi;
                default :
                    throw;
            }
        }

        constexpr double operator()(double chi,double phi) const {
            switch (_value)
            {
                case FieldX :
                    return sqrt(1-chi*chi)*cos(phi);
                case FieldY :
                    return sqrt(1-chi*chi)*sin(phi);
                case FieldXX :
                {
                    double c{ cos(phi) };
                    return (1-chi*chi)*c*c;
                }
                case FieldYY :
                {
                    double s{ sin(phi) };
                    return (1-chi*chi)*s*s;
                }
                case FieldXY :
                {
                    double c{ cos(phi) };
                    double s{ sin(phi) };
                    return (1-chi*chi)*c*s;
                }
                case FieldXZ :
                    return sqrt(1-chi*chi)*cos(phi)*chi;
                case FieldYZ :
                    return sqrt(1-chi*chi)*sin(phi)*chi;
                default :
                    throw;
            }
        }

        // static constexpr Func Func() const;
        constexpr operator Func() const;
        // constexpr operator Func() const
        // {
        //     return Func(1.,_value);
        // };

        using Repr = Td::Representation;
        using Decomp = Td::Decomposition;
        constexpr Func Representation(Repr r) const;
        constexpr Func Decomposition(Decomp d) const;
        constexpr Func Decomposition(Decomp d0, Decomp d1) const;


        static constexpr Flip AzimuthalParity(Enum in)
        {
            switch (in)
            {
                case Unity :
                case FieldZ :
                case FieldZZ :
                case FieldX :
                case FieldXX :
                case FieldYY :
                case FieldXZ :
                    return Flip::Heads;
                case FieldY :
                case FieldXY :
                case FieldYZ :
                    return Flip::Tails;
                default :
                    return Flip::undef;
            }
        }

        static constexpr Flip LongitudinalParity(Enum in)
        {
            switch (in)
            {
                case Unity :
                case FieldZZ :
                case FieldX :
                case FieldXX :
                case FieldY :
                case FieldYY :
                case FieldXY :
                    return Flip::Heads;
                case FieldZ :
                case FieldXZ :
                case FieldYZ :
                    return Flip::Tails;
                default :
                    return Flip::undef;
            }
        }



        static constexpr bool IsSphericallyInvariant(Enum in)
        {
            switch (in)
            {
                case Unity :
                    return true;
                default :
                    return false;
            }
        }

        static constexpr bool IsAzimuthallyInvariant(Enum in)
        {
            switch (in)
            {
                case Unity :
                case FieldZ :
                case FieldZZ :
                    return true;
                default :
                    return false;
            }
        }



        constexpr Flip AzimuthalParity() const
        {
            return AzimuthalParity(_value);
        }

        constexpr Flip LongitudinalParity() const
        {
            return LongitudinalParity(_value);
        }

        constexpr bool IsAzimuthallyInvariant() const
        {
            return IsAzimuthallyInvariant(_value);
        }

        constexpr bool IsSphericallyInvariant() const
        {
            return IsSphericallyInvariant(_value);
        }
    };




    class Func
    {
    public:
        // using data_t = std::array<cmplx,Dependence::NumDeps>;
        using data_t = std::array<std::array<double,2>,Dependence::NumDeps>;

        using data0_t = std::array<cmplx,Dependence::NumDeps>;

        // template< size_t N >
        // using spec_t = std::array<std::tuple<cmplx,Dependence>,N>;

        using spec_t = std::tuple<cmplx,Dependence>;

        static constexpr Dependence E { Dependence::Unity };
        static constexpr Dependence X { Dependence::FieldX };
        static constexpr Dependence Y { Dependence::FieldY };
        static constexpr Dependence Z { Dependence::FieldZ };
        static constexpr Dependence XX { Dependence::FieldXX };
        static constexpr Dependence YY { Dependence::FieldYY };
        static constexpr Dependence ZZ { Dependence::FieldZZ };
        static constexpr Dependence XY { Dependence::FieldXY };
        static constexpr Dependence XZ { Dependence::FieldXZ };
        static constexpr Dependence YZ { Dependence::FieldYZ };

        static constexpr cmplx tau { Td::ValleyComb::tau };
        static constexpr cmplx tauS { Td::ValleyComb::tauS };
        static constexpr std::array<cmplx,3> v { tau/3.,tauS/3.,1/3. };

    private:
        data_t _data;
        bool UnityFlag{ false };
        bool ZFieldFlag{ false };
        bool XYFieldFlag{ false };

    public:
        constexpr Func() : _data{ {0,0} } {}
        constexpr Func(const data_t in) :
            _data{ in },
            UnityFlag{ Cd(E) != czero },
            ZFieldFlag{ Cd(Z) != czero || Cd(ZZ) != czero },
            XYFieldFlag{
                 Cd(X) != czero ||  Cd(Y) != czero ||
                Cd(XX) != czero || Cd(YY) != czero ||
                Cd(XY) != czero || Cd(XZ) != czero || Cd(YZ) != czero
            }
        {}
        constexpr Func(data0_t in) : Func(Data(in)) {}
        constexpr Func(const cmplx z, const Dependence d=E) : Func(Data(z,d)) {}
        constexpr Func(const spec_t in) : Func(Data(in)) {}
        constexpr Func(std::initializer_list<spec_t> in)  : Func(Data(in)) {}

        // constexpr Func(const Dependence d) : Func(Data(1.0,d)) {}
        // template< size_t N >
        // constexpr Func(spec_t<N> in) : Func(Data(in)) {}


        static constexpr data_t Data(data0_t in)
        {
            // data_t r{ {0,0} };
            data_t r;
            r.fill({0,0});
            for ( size_t d=0; d<Dependence::NumDeps; d++ ) {
                r[d][0] += in[d].real();
                r[d][1] += in[d].imag();
            }
            return r;
        }
        static constexpr data_t Data(const cmplx z, const Dependence d)
        {
            data_t r;
            r.fill({0,0});
            r[d][0] += z.real();
            r[d][1] += z.imag();
            return r;
        }

        static constexpr data_t Data(const spec_t in)
        {
            data_t r;
            r.fill({0,0});
            auto z{ std::get<cmplx>(in) };
            auto d{ std::get<1>(in) };
            return Data(z,d);
        }

        static constexpr data_t Data(std::initializer_list<spec_t> in)
        {
            data_t r;
            r.fill({0,0});
            for ( auto xi : in )
            {
                auto z{ std::get<0>(xi) };
                auto d{ std::get<1>(xi) };
                r[d][0] += z.real();
                r[d][1] += z.imag();
            }
            return r;
        }

        constexpr Func operator-() const
        {
            data_t x { _data };
            for ( auto & xi : x ) { xi[0] = -xi[0]; xi[1] = -xi[1]; }
            return Func(x);
        }

        constexpr Func operator+(const Func &other) const
        {
            const auto &x{ _data };
            const auto &y{ other._data };
            data_t z{ {0,0} };
            // for ( size_t d=0; d<Dependence::NumDeps; d++ )
            // {
            //     z[d][0] += x[d][0]+y[d][0];
            //     z[d][1] += x[d][1]+y[d][1];
            // }
            constexpr_for<(ui_t)0,Dependence::NumDeps,(ui_t)1>
            ([&](auto d){
                z[d][0] += x[d][0]+y[d][0];
                z[d][1] += x[d][1]+y[d][1];
            });
            return Func(z);
        }

        constexpr cmplx Cd(Dependence d) const {
            return cmplx(_data[d][0],_data[d][1]); 
        }
        constexpr cmplx Coefficient(Dependence d) const { return Cd(d); }


        constexpr cmplx UnityPart() const { return Cd(E); }        
        constexpr cmplx ZField_Part(double chi) const
        {
            return Cd(Z)*Z(chi) + Cd(ZZ)*ZZ(chi);
        }
        constexpr cmplx XYField_Part(double chi, double phi) const
        {
            return (
                Cd(X)*X(chi,phi) + Cd(Y)*Y(chi,phi)
                +
                Cd(XX)*XX(chi,phi) + Cd(YY)*YY(chi,phi)
                +
                Cd(XY)*XY(chi,phi) + Cd(XZ)*XZ(chi,phi) + Cd(YZ)*YZ(chi,phi)
            );
        }

        constexpr cmplx operator()() const
        {
            if ( IsEmpty() ) { return 0.; }
            else if ( IsConstant() ) { return UnityPart(); }
            throw;
        }

        constexpr cmplx operator()(double chi) const
        {
            if ( IsEmpty() ) { return 0.; }
            else if ( IsConstant() ) { return UnityPart(); }
            if (ZFieldFlag && !XYFieldFlag)
            {
                return UnityPart() + ZField_Part(chi);
            }
            throw;
        }

        constexpr cmplx operator()(double chi, double phi) const
        {
            if ( IsEmpty() ) { return 0.; }
            else if ( IsConstant() ) { return UnityPart(); }
            if (ZFieldFlag && !XYFieldFlag)
            {
                return UnityPart() + ZField_Part(chi);
            }
            if (XYFieldFlag)
            {
                return UnityPart() + ZField_Part(chi) + XYField_Part(chi,phi);
            }
            throw;
        }


        constexpr bool IsEmpty() const 
        {
            return !UnityFlag && !ZFieldFlag && !XYFieldFlag;
        }
        constexpr bool IsConstant() const 
        {
            return UnityFlag && !ZFieldFlag && !XYFieldFlag;
        }

        constexpr bool IsAzimuthallyInvariant(Dependence d) const
        {
            return ( Cd(d) != czero ? d.IsAzimuthallyInvariant() : true );
        }
        constexpr bool IsSphericallyInvariant(Dependence d) const
        {
            return ( Cd(d) != czero ? d.IsSphericallyInvariant() : true );
        }

        constexpr Flip AzimuthalParity(Dependence d) const
        {
            if ( Cd(d) != czero ) { return d.AzimuthalParity(); }
            else { return Flip::undef; }
        }
        constexpr Flip LongitudinalParity(Dependence d) const
        {
            if ( Cd(d) != czero ) { return d.LongitudinalParity(); }
            else { return Flip::undef; }
        }


        constexpr bool IsAzimuthallyInvariant() const { return !XYFieldFlag; }
        constexpr bool IsSphericallyInvariant() const
        {
            return ( !XYFieldFlag && !ZFieldFlag );
        }

        constexpr Flip AzimuthalParity() const
        {
            bool NoFirstEval { true };
            Flip x { Flip::undef };
            for ( Dependence d : Dependence::Order )
            {
                if ( Cd(d) != czero )
                {
                    if ( NoFirstEval )
                    {
                        x = d.AzimuthalParity();
                        NoFirstEval = false;
                    }
                    else
                    {
                        x = x + d.AzimuthalParity();
                    }
                }
            }
            return x;
        }

        constexpr Flip LongitudinalParity() const
        {
            bool NoFirstEval { true };
            Flip x { Flip::undef };
            for ( Dependence d : Dependence::Order )
            {
                if ( Cd(d) != czero )
                {
                    if ( NoFirstEval )
                    {
                        x = d.LongitudinalParity();
                        NoFirstEval = false;
                    }
                    else
                    {
                        x = x + d.LongitudinalParity();
                    }
                }
            }
            return x;
        }

        using Decomp = Td::Decomposition;
        using enum SolidAngle::Dependence::Enum;
        constexpr Func Decomposition(Decomp d) const;
        constexpr Func Decomposition(Decomp d0, Decomp d1) const;
    };




    constexpr Func Dependence::Representation(Repr r) const
    {
        using data0_t = Func::data0_t;
        switch (_value)
        {
            case Unity : if(r == Repr::A1) { return Func(1,Unity); }
                break;
            case FieldX : if(r == Repr::T2) { return Func(1,FieldX); }
                break;
            case FieldY : if(r == Repr::T2) { return Func(1,FieldY); }
                break;
            case FieldZ : if(r == Repr::T2) { return Func(1,FieldZ); }
                break;
            case FieldZZ :
                switch(r)
                {
                    case Repr::A1: return Func(1,Unity);
                    case Repr::E:
                        return Func(data0_t{0,0,0,0,-1/3.,-1/3.,2/3.,0,0,0});
                    default:
                        break;
                }
                break;
            case FieldXX :
                switch(r)
                {
                    case Repr::A1: return Func(1,Unity);
                    case Repr::E:
                        return Func(data0_t{0,0,0,0,2/3.,-1/3.,-1/3.,0,0,0});
                    default:
                        break;
                }
                break;
            case FieldYY :
                switch(r)
                {
                    case Repr::A1: return Func(1,Unity);
                    case Repr::E:
                        return Func(data0_t{0,0,0,0,-1/3.,2/3.,-1/3.,0,0,0});
                    default:
                        break;
                }
                break;
            case FieldXY : if(r == Repr::T2) { return Func(1,FieldXY); }
                break;
            case FieldXZ : if(r == Repr::T2) { return Func(1,FieldXZ); }
                break;
            case FieldYZ : if(r == Repr::T2) { return Func(1,FieldYZ); }
                break;
            return Func();
        }
    }

    constexpr Func Dependence::Decomposition(Decomp d) const
    {
        auto & v { Func::v };
        switch (_value)
        {
            case Unity : if(d == Decomp::A1) { return Func(1,Unity); }
                break;
            case FieldX : if (d == Decomp::T2_x) { return Func(1,FieldX); }
                break;
            case FieldY : if (d == Decomp::T2_y) { return Func(1,FieldY); }
                break;
            case FieldZ : if (d == Decomp::T2_z) { return Func(1,FieldZ); }
                break;
            case FieldZZ :
            {
                switch(d)
                {
                    case Decomp::A1: return Func(1,Unity);
                    case Decomp::E1:
                        return Func({0,0,0,0,v[0],v[1],v[2],0,0,0});
                    case Decomp::E2:
                        return Func({0,0,0,0,v[1],v[0],v[2],0,0,0});
                    default:
                        break;
                }
                break;
            }
            case FieldXX :
            {
                switch(d)
                {
                    case Decomp::A1: return Func(1,Unity);
                    case Decomp::E1:
                        return Func({0,0,0,0,v[2],v[0],v[1],0,0,0});
                    case Decomp::E2:
                        return Func({0,0,0,0,v[2],v[1],v[0],0,0,0});
                    default:
                        break;
                }
                break;
            }
            case FieldYY :
            {
                switch(d)
                {
                    case Decomp::A1: return Func(1,Unity);
                    case Decomp::E1: 
                        return Func({0,0,0,0,v[1],v[2],v[0],0,0,0});
                    case Decomp::E2:
                        return Func({0,0,0,0,v[0],v[2],v[1],0,0,0});
                    default:
                        break;
                }
                break;
            }
            case FieldXY : if(d == Decomp::T2_z) { return Func(1,FieldXY); }
                break;
            case FieldXZ : if(d == Decomp::T2_y) { return Func(1,FieldXZ); }
                break;
            case FieldYZ : if(d == Decomp::T2_x) { return Func(1,FieldYZ); }
                break;
            return Func();
        }
    }

    constexpr Func Dependence::Decomposition(Decomp d0,Decomp d1) const
    {
        auto & v { Func::v };
        if ( d0 == d1 ) { return Dependence::Decomposition(d0); }
        switch (_value)
        {
            case FieldX :
                if (d1 == Decomp::T2_x)
                {
                    switch(d0)
                    {
                        case Decomp::T2_y: return Func(1,FieldY);
                        case Decomp::T2_z: return Func(1,FieldZ);
                        default: break;
                    }
                }
                break;
            case FieldY :
                if (d1 == Decomp::T2_y)
                {
                    switch(d0)
                    {
                        case Decomp::T2_x: return Func(1,FieldX);
                        case Decomp::T2_z: return Func(1,FieldZ);
                        default: break;
                    }
                }
                break;
            case FieldZ :
                if (d1 == Decomp::T2_z)
                {
                    switch(d0)
                    {
                        case Decomp::T2_x: return Func(1,FieldX);
                        case Decomp::T2_y: return Func(1,FieldY);
                        default: break;
                    }
                }
                break;
            case FieldZZ :
            {
                bool b_21 { d0 == Decomp::E2 && d1 == Decomp::E1 };
                bool b_12 { d0 == Decomp::E1 && d1 == Decomp::E2 };
                if(b_21) { return Func({0,0,0,0,v[1],v[0],v[2],0,0,0}); }
                if(b_12) { return Func({0,0,0,0,v[0],v[1],v[2],0,0,0}); }
                break;
            }
            case FieldXX :
            {
                bool b_21 { d0 == Decomp::E2 && d1 == Decomp::E1 };
                bool b_12 { d0 == Decomp::E1 && d1 == Decomp::E2 };
                if(b_21) { return Func({0,0,0,0,v[0],v[2],v[1],0,0,0}); }
                if(b_12) { return Func({0,0,0,0,v[1],v[2],v[0],0,0,0}); }
                break;
            }
            case FieldYY :
            {
                bool b_21 { d0 == Decomp::E2 && d1 == Decomp::E1 };
                bool b_12 { d0 == Decomp::E1 && d1 == Decomp::E2 };
                if(b_21) { return Func({0,0,0,0,v[1],v[0],v[2],0,0,0}); }
                if(b_12) { return Func({0,0,0,0,v[0],v[1],v[2],0,0,0}); }
                break;
            }
            case FieldXY :
            {
                bool b_xz { d0 == Decomp::T2_x && d1 == Decomp::T2_z };
                bool b_yz { d0 == Decomp::T2_y && d1 == Decomp::T2_z };
                if(b_xz) {  return Func(1,FieldYZ); }
                if(b_yz) { return Func(1,FieldXZ); }
                break;
            }
            case FieldXZ :
            {
                bool b_xy { d0 == Decomp::T2_x && d1 == Decomp::T2_y };
                bool b_zy { d0 == Decomp::T2_z && d1 == Decomp::T2_y };
                if(b_xy) { return Func(1,FieldYZ); }
                if(b_zy) { return Func(1,FieldXY); }
                break;
            }
            case FieldYZ :
            {
                bool b_yx { d0 == Decomp::T2_y && d1 == Decomp::T2_x };
                bool b_zx { d0 == Decomp::T2_z && d1 == Decomp::T2_x };
                if(b_yx) { return Func(1,FieldXZ); }
                if(b_zx) { return Func(1,FieldXY); }
                break;
            }
            default: break;
            return Func();
        }
    }

    constexpr Dependence::operator Func() const
    {
        switch(_value)
        {
            case Unity : return Func(1,Unity);
            case FieldZ : return Func(1,FieldZ);
            case FieldX : return Func(1,FieldX);
            case FieldY : return Func(1,FieldY);
            case FieldXX : return Func(1,FieldXX);
            case FieldYY : return Func(1,FieldYY);
            case FieldZZ : return Func(1,FieldZZ);
            case FieldXY : return Func(1,FieldXY);
            case FieldXZ : return Func(1,FieldXZ);
            case FieldYZ : return Func(1,FieldYZ);
        }
    }

    constexpr Func Func::Decomposition(Decomp d) const
    {
        switch (d)
        {
            case Decomp::A1 :
                return Func(Cd(E)+(Cd(XX)+Cd(YY)+Cd(XX))/3.,Unity); 
            case Decomp::A2 :
                return Func();
            case Decomp::E1 :
            {
                const cmplx cx{ v[2]*Cd(XX)+v[1]*Cd(YY)+v[0]*Cd(ZZ) };
                const cmplx cy{ v[0]*Cd(XX)+v[2]*Cd(YY)+v[1]*Cd(ZZ) };
                const cmplx cz{ v[1]*Cd(XX)+v[0]*Cd(YY)+v[2]*Cd(ZZ) };
                return Func( {{cx,XX},{cy,YY},{cz,ZZ}} );
            }
            case Decomp::E2 :
            {
                const cmplx cx{ v[2]*Cd(XX)+v[0]*Cd(YY)+v[1]*Cd(ZZ) };
                const cmplx cy{ v[1]*Cd(XX)+v[2]*Cd(YY)+v[0]*Cd(ZZ) };
                const cmplx cz{ v[0]*Cd(XX)+v[1]*Cd(YY)+v[2]*Cd(ZZ) };
                return Func( {{cx,XX},{cy,YY},{cz,ZZ}} );
            }
            case Decomp::T1_x :
            case Decomp::T1_y :
            case Decomp::T1_z :
                return Func();
            case Decomp::T2_x : 
                return Func( {{Cd(X),X}, {Cd(YZ),YZ}} );
            case Decomp::T2_y :
                return Func( {{Cd(Y),Y},{Cd(XZ),XZ}} );
            case Decomp::T2_z :
                return Func( {{Cd(Z),Z},{Cd(XY),XY}} );
            return Func();
        }
    }

    constexpr Func
    Func::Decomposition(Decomp d0,Decomp d1) const
    {
        if ( d0 == d1 ) { return Func::Decomposition(d0); }
        switch (d1)
        {
            case Decomp::E1 :
                if (d0 == Decomp::E2)
                {
                    const cmplx cx{ v[2]*Cd(XX)+v[1]*Cd(YY)+v[0]*Cd(ZZ) };
                    const cmplx cy{ v[0]*Cd(XX)+v[2]*Cd(YY)+v[1]*Cd(ZZ) };
                    const cmplx cz{ v[1]*Cd(XX)+v[0]*Cd(YY)+v[2]*Cd(ZZ) };
                    return Func( {{cx,YY},{cy,XX},{cz,ZZ}} );
                }
                break;
            case Decomp::E2 :
                if (d0 == Decomp::E1)
                {
                    const cmplx cx{ v[2]*Cd(XX)+v[0]*Cd(YY)+v[1]*Cd(ZZ) };
                    const cmplx cy{ v[1]*Cd(XX)+v[2]*Cd(YY)+v[0]*Cd(ZZ) };
                    const cmplx cz{ v[0]*Cd(XX)+v[1]*Cd(YY)+v[2]*Cd(ZZ) };
                    return Func( {{cx,YY},{cy,XX},{cz,ZZ}} );
                }
                break;
            case Decomp::T2_x :
                switch (d0)
                {
                    case Decomp::T2_y :
                        return Func( {{Cd(X),Y},{Cd(YZ),XZ}} );
                    case Decomp::T2_z :
                        return Func( {{Cd(X),Z},{Cd(YZ),XY}} );
                    default: break;
                }
                break;
            case Decomp::T2_y :
                switch (d0)
                {
                    case Decomp::T2_x :
                        return Func( {{Cd(Y),X},{Cd(XZ),YZ}} );
                    case Decomp::T2_z :
                        return Func( {{Cd(Y),Z},{Cd(XZ),XY}} );
                    default: break;
                }
                break;
            case Decomp::T2_z :
                switch (d0)
                {
                    case Decomp::T2_x :
                        return Func( {{Cd(Z),X},{Cd(XY),YZ}} );
                    case Decomp::T2_y :
                        return Func( {{Cd(Z),Y},{Cd(XY),XZ}} );
                    default: break;
                }
                break;
            default: break;
            return Func();
        }
    }
}