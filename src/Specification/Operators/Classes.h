namespace Potential
{
    
    struct Type
    {
        const Enum value;

        Type() = delete;
        constexpr Type(Enum in) : value(in) {}
        

        // Allow switch and comparisons.
        constexpr operator Enum() const { return value; }

        // Prevent usage: if(OperatorSpec)
        explicit operator bool() = delete;

        constexpr int PotIndex() const { return static_cast<int>(value); }

        constexpr size_t r_idx() const { return _idx_terms[value][0]; }
        constexpr Td::Representation Repr() const { return _reprs[r_idx()]; }

        constexpr size_t e_idx() const { return _idx_terms[value][1]; }
        constexpr double exp() const { return std::get<0>(_exps[e_idx()]); }
        constexpr auto id() const { return std::get<1>(_exps[e_idx()]); }


        static constexpr auto id(size_t ex)
        {
            return std::get<1>(_exps[_idx_terms[ex][1]]);
        }


        constexpr size_t i0() const { return _idx_terms[value][2]; }
        constexpr size_t i1() const { return _idx_terms[value][3]; }


        constexpr size_t nterms() const { return i1()-i0()+1; }
        
        // constexpr size_t ex() const { return value; }


        constexpr size_t idx(size_t i=0) const 
        {
            const size_t j0{ i0() };
            return i+j0;
        }

        constexpr const auto& et(size_t i=0) const
        {
            return _exp_terms[idx(i)];
        }
        constexpr const cmplx& U0(size_t i=0) const { return et(i).U0(); }
        constexpr const Decay::Spec& Decay(size_t i=0) const
        {
            return et(i).Decay();
        }
        constexpr const Func& SolidAngle(size_t i=0) const
        { 
            return et(i).SolidAngle();
        }

        constexpr Flip AzimuthalParity(size_t i=0) const
        {
            return (
                Decay(i).AzimuthalParity()*SolidAngle(i).AzimuthalParity()
            );
        }
        constexpr Flip LongitudinalParity(size_t i=0) const
        {
            return (
                Decay(i).LongitudinalParity()
                *SolidAngle(i).LongitudinalParity()
            );
        }

        constexpr bool IsAzimuthallyInvariant(size_t i=0) const
        {
            return (
                Decay(i).IsAzimuthallyInvariant()
                &&
                SolidAngle(i).IsAzimuthallyInvariant()
            );
        }

        constexpr bool IsSphericallyInvariant(size_t i=0) const
        {
            return (
                Decay(i).IsSphericallyInvariant()
                &&
                SolidAngle(i).IsSphericallyInvariant()
            );
        }

        constexpr bool HasNoDecay(size_t i=0) const
        {
            return Decay(i).HasNoDecay();
        }
        constexpr bool HasYukawaDecay(size_t i=0) const
        {
            return Decay(i).HasYukawaDecay();
        }
        constexpr bool HasGaussianDecay(size_t i=0) const
        {
            return Decay(i).HasGaussianDecay();
        }
    };

}

namespace Operator
{

    struct Spec
    {
        const Enum value;
        const Kinetic::Spec K;
        const Kinetic::Spec Kx;
        const Potential::Type U;

        constexpr Spec() = delete;
        constexpr Spec(Enum in) : 
            value{ in },
            K{ Operator::K(value) }, Kx{ Operator::Kx(value) },
            U{ Operator::U(value) }
        {}
        constexpr Spec(int in) : Spec(static_cast<Enum>(in)) {}

        constexpr size_t nterms() const { return U.nterms(); }

        // Allow switch and comparisons.
        constexpr operator Enum() const { return value; }

        // Prevent usage: if(OperatorSpec)
        explicit operator bool() = delete;

        constexpr int TermIndex() const { return static_cast<int>(value); }

        template< class F >
        static constexpr void for_each(F&& f)
        {
            constexpr_for<0,NumOperators,1>(
                [&](auto i) { f(std::integral_constant<Spec,Spec(i)>()); }
            );
        }

        constexpr bool KL() const { return K.KL(); }
        constexpr bool KR() const { return K.KR(); }
        constexpr Td::Representation Repr() const { return U.Repr(); }
        constexpr double exp() const { return U.exp(); }
        constexpr auto id() const { return U.id(); }
        constexpr size_t i0() const { return U.i0(); }
        constexpr size_t i1() const { return U.i1(); }
        constexpr size_t e_idx() const { return U.e_idx(); }
        constexpr cmplx U0(size_t i) const { return U.U0(i); }
        constexpr const auto& Decay(size_t i) const { return U.Decay(i); }
        constexpr auto SolidAngle(size_t i) const { return U.SolidAngle(i); }
        
        constexpr Flip AzimuthalParity(size_t i) const
        {
            return U.AzimuthalParity(i);
        }
        constexpr Flip LongitudinalParity(size_t i) const
        {
            return U.LongitudinalParity(i);
        }

        constexpr bool IsAzimuthallyInvariant(size_t i) const
        {
            return U.IsAzimuthallyInvariant(i);
        }
        constexpr bool IsSphericallyInvariant(size_t i) const
        {
            return U.IsSphericallyInvariant(i);
        }
        
        constexpr bool HasNoDecay(size_t i) const
        {
            return U.HasNoDecay(i);
        }

        constexpr bool HasYukawaDecay(size_t i) const
        {
            return U.HasYukawaDecay(i);
        }

        constexpr bool HasGaussianDecay(size_t i) const
        {
            return U.HasGaussianDecay(i);
        }

        constexpr bool IsFlat(size_t i) const
        {
            return (
                IsSphericallyInvariant(i)
                &&
                U.exp()==0
                &&
                U.HasNoDecay(i)
            );
        }

        constexpr bool IsIsotropic(size_t i) const
        {
            return ( IsSphericallyInvariant(i) && U.exp()!=0 );
        }
    };

    template< Spec oS >
    constexpr Flip AzimuthalParity(Flip x)
    {
        auto ap { [&](auto i){ return oS.AzimuthalParity(i); } };
        return AllFlipProduct<oS.nterms()>(ap,x);

    }
    template< Spec oS >
    constexpr Flip LongitudinalParity(Flip x)
    {
        auto lp { [&](size_t i)->Flip{ return oS.LongitudinalParity(i); } };
        return AllFlipProduct<oS.nterms()>(lp,x);
    }

    template< Spec oS >
    constexpr bool IsAzimuthallyInvariant()
    {
        auto ai_q { [&](auto i){ return oS.IsAzimuthallyInvariant(i); } };
        return AllTrue<oS.nterms()>(ai_q);
    }
    template< Spec oS >
    constexpr bool IsSphericallyInvariant()
    {
        auto si_q { [&](auto i){ return oS.IsSphericallyInvariant(i); } };
        return AllTrue<oS.nterms()>(si_q);
    }

    template< Spec oS >
    constexpr bool HasNoDecay()
    {
        auto q { [&](auto i){ return oS.HasNoDecay(i); } };
        return AllTrue<oS.nterms()>(q);
    }
    template< Spec oS >
    constexpr bool HasYukawaDecay()
    {
        auto q { [&](auto i){ return oS.HasYukawaDecay(i); } };
        return AllTrue<oS.nterms()>(q);
    }
    template< Spec oS >
    constexpr bool HasGaussianDecay()
    {
        auto q { [&](auto i){ return oS.HasGaussianDecay(i); } };
        return AllTrue<oS.nterms()>(q);
    }

    template< Spec oS >
    constexpr bool IsFlat()
    {
        auto flat_q { [&](auto i){ return oS.IsFlat(i); } };
        return AllTrue<oS.nterms()>(flat_q);
    }
    template< Spec oS >
    constexpr bool IsIsotropic()
    {
        auto iso_q { [&](auto i){ return oS.IsIsotropic(i); } };
        return AllTrue<oS.nterms()>(iso_q);
    }

    template< Spec oS >
    constexpr bool IsAzimuthallyInvariant(
        const Bloch::Type bT, const Term::Type tT
    ) {
        using enum Bloch::Type::Enum;
        using enum Term::Type::Enum;
        switch(bT)
        {
            case Naive :
                switch(tT)
                {
                    case Parallel :
                    case Antiparallel : return IsAzimuthallyInvariant<oS>();
                    case Perpendicular : return false;
                    default : return false;
                }
            case Trunc : return false;
            default : return false;
        }
    }

    template< Spec oS >
    constexpr bool IsFlat(const Bloch::Type bT, const Term::Type tT)
    {
        using enum Bloch::Type::Enum;
        using enum Term::Type::Enum;
        switch(bT)
        {
            case Naive :
                switch(tT)
                {
                    case Parallel : return IsFlat<oS>();
                    default : return false;
                }
            case Trunc : return false;
        }
    }
    template< Spec oS >
    constexpr bool IsIsotropic(const Bloch::Type bT, const Term::Type tT)
    {
        using enum Bloch::Type::Enum;
        using enum Term::Type::Enum;
        switch(bT)
        {
            case Naive :
                switch(tT)
                {
                    case Parallel : return IsIsotropic<oS>();
                    default : return false;
                }
            case Trunc : return false;
        }
    }

}