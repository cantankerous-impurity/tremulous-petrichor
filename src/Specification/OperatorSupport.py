from SpecificationClasses import DecaySpec,SolidAngleFunc
from numbers import Number

from itertools import combinations, product


grp_type = tuple[tuple[DecaySpec,SolidAngleFunc,Number,Number],...]

def PotSquare(p):
    (_,abbr,exp,repr,grp) = p
    grp:grp_type
    ngrp = (
        tuple( (de*de,sa*sa,U0*U0,2*ph) for (de,sa,U0,ph) in grp )
        +
        tuple(
            (d0*d1,s0*s1,2*U0*U1,ph0+ph1)
            for ((d0,s0,U0,ph0),(d1,s1,U1,ph1))
            in combinations(grp,2)
        )
    )
    return (2*abbr,(2*exp,repr,ngrp))
def PotProduct(pa,pb):
    grpx:grp_type
    grpy:grp_type
    (_,abx,dx,repr,grpx) = pa
    (_,aby,dy,repr,grpy) = pb
    # Currently, the code can only handle both repr being A1.
    ngrp = tuple( 
        (dex*dey,sax*say,2*Ux*Uy,phx+phy)
        for ((dex,sax,Ux,phx),(dey,say,Uy,phy))
        in product(grpx,grpy)
    )
    return (abx+aby,(dx+dy,repr,ngrp))





def get_OpId(exp):
    from DatabaseInterface import DBInterface
    from CoefficientSupport import Loc
    DB = DBInterface("Operators.db",Loc)

    Query = "SELECT uuid FROM OperatorIds WHERE exponent = ?"
    
    res = DB.Execute(Query,(exp,))
    return "\""+next(res)[0]+"\""




KinSpecs = tuple(''.join(x) for x in product('IK',repeat=2))

















# def parse_PotExp(v:grp_type):
#     return ','.join(str(x) for x in v)
    


# def parse_PotTerm(v:grp_type):
#     return tuple(
#         ','.join(str(x) for x in (de.cpp_id,sa.name,U1,ph))
#         for de,sa,U1,ph in (
#             (de,sa,U0*Us,ph) 
#             for de,sf,U0,ph in v for sa,Us in sf.data.items()
#         )
        
#     )

# def parse_PotDecay(v:grp_type)->tuple[DecayType,...]:
#     return tuple(
#         dt.cpp
#         for ds in set(ds for ds,_,_,_ in v)
#         for dt in ds.dec_type.BlochTuple
#     )







def parsepot_Pot(x):
    name,abbr,_,_,_ = x
    return (
        name,
        abbr+"Kin",
        "Kin"+abbr,
    )
def parsekin_Pot(x):
    y = parsepot_Pot(x)
    return (
        (y[0],"Kinetic::Spec::II"),
        (y[1],"Kinetic::Spec::IK"),
        (y[2],"Kinetic::Spec::KI"),
    )


def parsepot_PP(x):
    _,abbr,_,_,_ = x
    return ( 2*abbr, )
def parsekin_PP(x):
    y = parsepot_PP(x)
    return ( (y[0],"Kinetic::Spec::II"), )



def parsepot_PaPb(x,y):
    (_,abx,_,_,_) = x
    (_,aby,_,_,_) = y
    return ( abx+aby, )
def parsekin_PaPb(x,y):
    name = parsepot_PaPb(x,y)
    return ( (name[0],"Kinetic::Spec::II"), )