namespace Bloch
{
    using iN_0 = NaiveInterface<Term::Type::Parallel>;
    using iN_A = NaiveInterface<Term::Type::Antiparallel>;
    using iN_P = NaiveInterface<Term::Type::Perpendicular>;

    constexpr iN_0 N0;
    constexpr iN_A NA;
    constexpr iN_P NP;

    // static constexpr std::array<size_t,3> NumBlochTerms { 15,10,8 };
    static constexpr std::array<size_t,3> NumBlochTerms { 2,2,2 };
    // static constexpr std::array<size_t,3> NumBlochTerms { 0,0,0 };

    template< Term::Type tT >
    using inT = TruncInterface<tT,NumBlochTerms[tT]>;
    using iT_0 = inT<Term::Type::Parallel>;
    using iT_A = inT<Term::Type::Antiparallel>; 
    using iT_P = inT<Term::Type::Perpendicular>; 

    const iT_0 T0;
    const iT_A TA;
    const iT_P TP;

    template < Term::Type tT, Bloch::Type bT >
    constexpr auto& GetInterface()
    {
        if constexpr( bT == Bloch::Type::Naive )
        {
            if constexpr( tT == Term::Type::Parallel ) { return N0; }
            if constexpr( tT == Term::Type::Antiparallel) { return NA; }
            if constexpr( tT == Term::Type::Perpendicular) { return NP; }
        }
        if constexpr( bT == Bloch::Type::Trunc )
        {
            if constexpr( tT == Term::Type::Parallel ) { return T0; }
            if constexpr( tT == Term::Type::Antiparallel) { return TA; }
            if constexpr( tT == Term::Type::Perpendicular) { return TP; }
        }
    }

}