namespace Bloch
{
    using ui_t = uint_fast8_t;
    using Func = SolidAngle::Func;
    using Dep = SolidAngle::Dependence;
    using enum Flip::Enum;
    using enum Dep::Enum;
    using enum Term::Type::Enum;
    using enum Bloch::Type::Enum;
    using vec_t = Spec::vec_t;
    using Z_t = Decay::Z_t;
    using Z_arr = std::array<Z_t,8>;


    template< Term::Type tT >
    struct NaiveInterface
    {
        static constexpr auto _f{
            []{
                if constexpr( tT == Perpendicular )
                {
                    return []()->std::array<Func,2>{
                        constexpr cmplx z0 { -iunit*SystemUnits::k_val };
                        return {
                            Func( {{z0,FieldX}, {+z0,FieldY}} ),
                            Func( {{z0,FieldX}, {-z0,FieldY}} ) 
                        };
                    }();
                }
                else
                {
                    return []()->std::array<Func,1>{
                        if constexpr(tT == Parallel) { return { Func() }; }
                        if constexpr(tT == Antiparallel)
                        {
                            constexpr cmplx z0 { -2.*iunit*SystemUnits::k_val };
                            return { Func(z0,FieldZ), };
                        }
                    }();
                }
            }()
        };
        static constexpr const Func& KeyFunc(ui_t k = 0){ return _f[k]; }

        template< Operator::Spec oS >
        static constexpr size_t NumValues() { return 1; }
        template< Operator::Spec oS >
        static constexpr size_t NumKeys(size_t g = 0)
        {
            if( g==0 )
            {
                if constexpr( tT == Perpendicular ) { return 2; }
                else { return 1; }
            }
            throw std::invalid_argument( "Value index out of range." ); 
        }
        static constexpr double cAP{ -0.24221590214356248 };
        static constexpr double cPerp{ 0.37545876238673850 };
        template< Operator::Spec oS >
        static constexpr double Value(size_t g = 0)
        {
            if constexpr( Operator::NoPot<oS> ) { return 1.; }
            if constexpr( Operator::Pot<oS> )
            {
                if( g==0 )
                {
                    if constexpr(tT == Parallel) { return 1.; }
                    if constexpr(tT == Antiparallel){ return cAP; }
                    if constexpr(tT == Perpendicular){ return cPerp; }
                }
                throw std::invalid_argument("Value index out of range."); 
            }
        }
        template< Operator::Spec oS >
        static constexpr double Phase(size_t g = 0, ui_t k = 0)
        {
            if ( g==0 )
            {
                if( k==0 ) { return 0.; }
                if constexpr( tT == Perpendicular ){ if( k<=1 ) { return 0.; } }
                throw std::invalid_argument("Key index out of range.");
            }
            throw std::invalid_argument("Value index out of range.");
        }

        template < Operator::Spec oS, Decay::Spec dS, typename... Types >
        static constexpr void CalcZ(Z_arr& res,double r, Types... args)
        {
            constexpr_for<(ui_t)0.,NumKeys<oS>(),(ui_t)1.>
            (
                [&](auto k){
                    constexpr auto& f{ KeyFunc(k) };
                    res[k] = Decay::Z<f,dS,tT,Bloch::Type::Naive>(r, args...);
                    // res[k] = Decay::Z<dS,tT,Bloch::Type::Naive>(f,r, args...);
                }   
            );
        }

        template< Operator::Spec oS >
        static constexpr bool IsAnti(size_t g = 0, ui_t k = 0)
        {
            if( g==0 )
            {
                if constexpr(tT == Perpendicular)
                {
                    if (k==0) { return true; }
                    if (k==1) { return false; }
                    throw std::invalid_argument("Key index out of range.");
                }
                else
                {
                    if( k==0 )
                    {
                        if constexpr(tT == Parallel) { return false; }
                        if constexpr(tT == Antiparallel) { return true; }
                    }
                    throw std::invalid_argument("Key index out of range.");
                }
            }
            throw std::invalid_argument("Value index out of range.");
        }
        template< Operator::Spec oS >
        static constexpr Flip anti(size_t g = 0, ui_t k = 0)
        {
            return IsAnti<oS>(g,k);
        }
    };



    template< Term::Type tT, size_t Nt >
    class TruncInterface : public SQL::Interface<>
    {
    public:
        using Z_a = std::array<Z_arr,Nt>;

        // Queries to get Bloch coefficient values.
        const std::string QC{
            "SELECT Dabs,Darg FROM Bloch_Coeffs_"+tT.DirSpec()+"_Gamble"
            +" WHERE g = ?1;"
        };

        // Queries to get Bloch vectors corresponding to coefficient values.
        const std::string QK{
            "SELECT gx,gy,gz,p,n FROM Bloch_Keys_"+tT.DirSpec()+"_Gamble"
            +" WHERE g = ?1;"
        };

        using stars_a = std::array<Container,Nt>;
        using funcs_v = std::array<Func,8>;
        using funcs_a = std::array<funcs_v,Nt>;

    private:
        stars_a _stars;
        funcs_a _funcs;

        Z_a _Zs;
        Container& star(size_t g) { return _stars[g]; }

    public:
        TruncInterface() { Import(); SetFunc(); }

        Func MakeFunc(const vec_t& v)
        {
            constexpr cmplx z1{ -0.5*iunit*SystemUnits::k_Si };
            return Func(
                {{z1*v[0],FieldX}, {z1*v[1],FieldY}, {z1*v[2],FieldZ}}
            );
        }

        void SetFunc()
        {
            for( size_t g=0; g<NumStars(); g++ )
            {
                for( ui_t k=0; k<NumKeys_Pot(g); k++ )
                {
                    const vec_t& v{ wave_vector(g,k) };
                    const Func&& f{ MakeFunc(std::forward<const vec_t>(v)) };
                    _funcs[g][k] = f;
                }
            }
        }

        constexpr Func KeyFunc(size_t g, ui_t k) const { return _funcs[g][k]; }

        const Container& star(size_t g) const { return _stars[g]; }
        const Spec& key(size_t g, ui_t k) const { return star(g).key(k); }

        Flip anti(size_t g, ui_t k) const { return key(g,k).anti(); }
        Flip conjg(size_t g, ui_t k) const { return key(g,k).conjg(); }
        bool IsAnti(size_t g, ui_t k) const { return key(g,k).IsAnti(); }
        bool IsConj(size_t g, ui_t k) const { return key(g,k).IsConj(); }
        const vec_t& wave_vector(size_t g, ui_t k) const
        { return key(g,k).wave_vector();  }
        double wave_vector(size_t g, ui_t k, AnisoDirection d) const
        {
            return key(g,k).wave_vector(d);
        }
        static constexpr size_t NumStars() { return Nt; };
        

        static constexpr size_t NumKeys_NoPot(size_t g = 0)
        {
            if( g==0 ) { return 0; }
            throw std::invalid_argument("Value index out of range.");
        }
        static constexpr double Value_NoPot(size_t g = 0)
        {
            if( g==0 ) { return 0; }
            throw std::invalid_argument("Value index out of range.");
        }
        static constexpr double Phase_NoPot(size_t g = 0, ui_t k = 0)
        {
            if ( g==0 )
            {
                if ( k== 0) { return 0.; }
                throw std::invalid_argument("Key index out of range.");
            }
            throw std::invalid_argument("Value index out of range.");
        }

        size_t NumKeys_Pot(size_t g = 0) const { return star(g).size(); }
        double Value_Pot(size_t g) const { return star(g).value(); }
        double _Ph(size_t g) const { return star(g).phase(); }
        double Phase_Pot(size_t g, ui_t k) const { return _Ph(g)*conjg(g,k); }


        template< Operator::Spec oS >
        static constexpr size_t NumValues()
        {
            if constexpr( Operator::NoPot<oS> ) { return 0; }
            if constexpr( Operator::Pot<oS> ) { return Nt; }
        }
        template< Operator::Spec oS >
        auto NumKeys(size_t g = 0)
        {
            if constexpr( Operator::NoPot<oS> ) { return NumKeys_NoPot(g); }
            if constexpr( Operator::Pot<oS> ) { return NumKeys_Pot(g); }
        }
        template< Operator::Spec oS >
        auto Value(size_t g = 0)
        {
            if constexpr( Operator::NoPot<oS> ) { return Value_NoPot(g); }
            if constexpr( Operator::Pot<oS> ) { return Value_Pot(g); }
        }
        template< Operator::Spec oS >
        auto Phase(size_t g = 0, ui_t k = 0)
        {
            if constexpr( Operator::NoPot<oS> ) { return Phase_NoPot(g,k); }
            if constexpr( Operator::Pot<oS> ) { return Phase_Pot(g,k); }
        }

        template < Operator::Spec oS, Decay::Spec dS, typename... Types >
        void CalcZ(double r, Types... args)
        {
            constexpr Bloch::Type bT{ Bloch::Type::Trunc };
            constexpr_for<size_t(0),NumValues<oS>(),size_t(1)>
            (
                [&](auto g)
                {
                    constexpr_for<ui_t(0),NumKeys<oS>(g),ui_t(1)>
                    (
                        [&](auto k)
                        {
                            constexpr const Func& f{ KeyFunc(g,k) };
                            _Zs[g][k] = Z<f,dS,tT,bT>(r, args...);
                        }
                    );
                }
            );

            // for( size_t g=0; g<; g++ )
            // {
            //     for( ui_t  k=0; k<NumKeys<oS>(g); k++ )
            //     {
            //         constexpr const Func& f{ KeyFunc(g,k) };
                    
            //     }
            // }
        }
        
        auto& GetZ(size_t g, ui_t k) { return _Zs[g][k]; }

        static constexpr bool IsConjugated_NoPot() { return false; }
        template< Operator::Spec oS >
        auto IsConjugated(size_t g = 0, ui_t k = 0)
        {
            if constexpr( Operator::NoPot<oS> )
            {
                if( g==0 )
                {
                    if ( k==0 ) { return IsConjugated_NoPot(); }
                    throw std::invalid_argument("Key index out of range.");
                }
                throw std::invalid_argument("Value index out of range.");
            }
            if constexpr( Operator::Pot<oS> ) { return IsAnti(g,k); }
        }

        void Import()
        {
            rc = OpenDatabase(DBPath.c_str());

            // Prepare a statement to extract the coefficients from database.
            sqlite3_stmt* StmtC{ nullptr };
            rc = PrepareStatement(&StmtC, QC);
            if( rc != SQLITE_OK ) { EndProgram(); }

            sqlite3_stmt* StmtK{ nullptr };
            rc = PrepareStatement(&StmtK, QK);
            if( rc != SQLITE_OK ) { EndProgram(); }

            for( size_t g=0; g<Nt; g++ )
            {
                // Bind specific coeff index to the sqlite queries.
                try
                {
                    rc = sqlite3_reset(StmtC);
                    if( rc != SQLITE_OK ) { EndProgram(); }
                    rc = sqlite3_clear_bindings(StmtC);
                    if( rc != SQLITE_OK ) { EndProgram(); }
                    rc = sqlite3_bind_int(StmtC,1,g);
                    if( rc != SQLITE_OK ) { EndProgram(); }
                    
                }
                catch (const char* e)
                {
                    std::cerr << "Binding of SQLite3 C parameter had errors!";
                    std::cerr << std::endl;
                    std::cerr << "QC: " << QC << std::endl;
                    std::cerr << "g: " << g << std::endl;
                    EndProgram();
                }
                try
                {
                    rc = sqlite3_reset(StmtK);
                    if( rc != SQLITE_OK ) { EndProgram(); }
                    rc = sqlite3_clear_bindings(StmtK);
                    if( rc != SQLITE_OK ) { EndProgram(); }
                    rc = sqlite3_bind_int(StmtK,1,g);
                    if( rc != SQLITE_OK ) { EndProgram(); }
                }
                catch (const char* e)
                {
                    std::cerr << "Binding of SQLite3 K parameter had errors!";
                    std::cerr << std::endl;
                    try{ if( rc != SQLITE_OK ) { CloseDatabase(); } }
                    catch (const char* e)
                    {
                        std::cerr << "Ending program.";
                        std::cerr << std::endl;
                        throw e;
                    }
                    throw e;
                }

                // Fill coefficients array
                rc = sqlite3_step(StmtC);
                while (rc == SQLITE_ROW) {
                    double c{ sqlite3_column_double(StmtC, 0) };
                    double ph{ sqlite3_column_double(StmtC, 1) };
                    star(g).set({c,ph});
                    rc = sqlite3_step(StmtC);
                }
                // Fill key array
                rc = sqlite3_step(StmtK);
                while (rc == SQLITE_ROW) {
                    double gx{ sqlite3_column_double(StmtK, 0) };
                    double gy{ sqlite3_column_double(StmtK, 1) };
                    double gz{ sqlite3_column_double(StmtK, 2) };
                    Flip conj{ (bool)((sqlite3_column_int(StmtK, 3)+1)/2) };
                    Flip anti{ (bool)sqlite3_column_int(StmtK, 4) };
                    star(g).push_back(g,gx,gy,gz,anti,conj);
                    rc = sqlite3_step(StmtK);
                }
            }

            rc = FinalizeStatement(StmtC);
            rc = FinalizeStatement(StmtK);

            rc = CloseDatabase();          
        }
    };
}