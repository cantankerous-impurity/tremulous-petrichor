// constexpr bool WillDoLoop { true };
constexpr bool WillDoLoop { false };

#include"TremulousPetrichor.h"
#define mstr(s) str(s)
#define str(s) #s


constexpr size_t NR10{ 257 };
// constexpr size_t NR10{ 129 };
// constexpr size_t NR10{ 65 };
// constexpr size_t NR10{ 33 };
// constexpr size_t NR10{ 17 };
// constexpr size_t NR10{ 11 };
// constexpr size_t NR10{ 9 };
// constexpr size_t NR10{ 5 };
// constexpr size_t NR10{ 3 };

constexpr double r0{ 0.05 };constexpr double r1{ 3.00};
// constexpr double rC{ 0.505 };constexpr double rDel{ 2*(0.205) };
// constexpr double r0{ rC - rDel/2 };constexpr double r1{ rC + rDel/2 };
constexpr double DR10{ (NR10>1) ? abs(r1-r0)/(NR10-1) : 0. };

constexpr size_t NA10{ 257 };
// constexpr size_t NA10{ 129 };
// constexpr size_t NA10{ 65 };
// constexpr size_t NA10{ 33 };
// constexpr size_t NA10{ 17 };
// constexpr size_t NA10{ 11 };
// constexpr size_t NA10{ 9 };
// constexpr size_t NA10{ 5 };
// constexpr size_t NA10{ 3 };

constexpr double an0{ 0.05 };constexpr double an1{ 3.00 };
// constexpr double anC{ 0.9 };constexpr double anDel{ 2*(.6) };
// constexpr double an0{ anC - anDel/2 };constexpr double an1{ anC + anDel/2 };
constexpr double DA10{ (NA10>1) ? abs(an1-an0)/(NA10-1) : 0. };


int main(int argc,char **args)
{
    #include<limits>
    std::cout.precision(std::numeric_limits<double>::max_digits10);

    fs::path subdir{ SingleValleyModel ? "SV_" : "MV_" };
    subdir += ( NaiveBloch ? "NB_" : "TB_" );
    subdir += Potential::Id;

    fs::path dir{ DataPath / subdir };
    fs::create_directories(dir);

    fs::path file{
        std::to_string( duration_cast<ch::milliseconds>(
            ch::system_clock::now().time_since_epoch()
        ).count() )
        +".dat"
    };

    fs::path full_path = dir / file;
    std::ofstream ofs;
    ofs.open( full_path, std::ofstream::out | std::ofstream::app );


    // (void)argc;
    // (void)args;
    // PetscCall(PetscOptionsSetValue(nullptr,"-info",nullptr));
    PetscCall(SlepcInitialize(&argc,&args,nullptr,nullptr));
    
    PetscMPIInt nprocs,rproc;
    PetscCallMPI(MPI_Comm_size(PETSC_COMM_WORLD,&nprocs));
    PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD,&rproc));

    Operator::PrintMassAniso(ofs);
    Operator::PrintRelativeDielectric(ofs);
    ofs << std::endl;
    SystemUnits::PrintEnergy(ofs);
    SystemUnits::PrintLength(ofs);
    
    ofs << std::endl;
    ofs << std::endl;

    Potential::PrintDescription(ofs);

    ofs << std::endl;
    ofs << std::endl;

    Basis::Print(ofs);

    ofs << std::endl;
    ofs << std::endl;

    Matrix::Manager mat0;

    constexpr_for<0,NumBases,1>(
        [&](auto i){
            ofs << "Radius" << i << "(AU),";
            ofs << "Aniso" << i << ",";
        }
    );
    for(size_t i = 0; i < NumStates; i++)
    {
        ofs << "Energy" << i << "(AU),";
        ofs << "Variance" << i << ",";
    }
    ofs << "Del";
    ofs << std::endl;

    if constexpr ( WillDoLoop )
    {
        DoLoop<2>(
            std::min(r1,r0),std::min(an1,an0),
            NR10,DR10,NA10,DA10,ofs,mat0
        );
    }
    else
    {
        GetResults(ofs,mat0);
    }

    mat0.Finalize();
    PetscCall(SlepcFinalize());

    return 0;
}