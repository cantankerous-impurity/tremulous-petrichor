FROM ubuntu:latest

ENV PROJECT_NAME=cantankerous-impurity
ENV COMPUTATION_SUBPROJECT=Computations
ENV PRECALC_SUBPROJECT=Precalculation

WORKDIR /

RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get install bash
RUN /bin/bash -c 'apt-get update'
RUN /bin/bash -c 'apt-get upgrade -y'
RUN /bin/bash -c 'apt-get install git -y'
RUN /bin/bash -c 'apt-get install make -y'
RUN /bin/bash -c 'apt-get install wget -y'
RUN /bin/bash -c 'apt-get install python3 -y'
RUN /bin/bash -c 'apt-get install python3-pip -y'
RUN /bin/bash -c 'python3 -m pip install sympy'
RUN /bin/bash -c 'apt-get install build-essential -y'
RUN /bin/bash -c 'apt-get install gfortran -y'
RUN /bin/bash -c 'apt-get install libboost-dev -y'
RUN /bin/bash -c 'apt-get install libsqlite3-dev -y'
RUN /bin/bash -c 'wget http://www.feynarts.de/cuba/Cuba-4.2.2.tar.gz'

ENV CUBA_DIR /cuba
RUN /bin/bash -c 'tar -xzvf Cuba-4.2.2.tar.gz'
RUN /bin/bash -c 'mv Cuba-4.2.2 ${CUBA_DIR}'
WORKDIR ${CUBA_DIR}
RUN /bin/bash -c './configure'
RUN /bin/bash -c 'make lib'

ENV PETSC_DIR=/petsc
ENV PETSC_GIT=https://gitlab.com/petsc/petsc.git
RUN /bin/bash -c 'git clone -b release ${PETSC_GIT} ${PETSC_DIR}'
WORKDIR ${PETSC_DIR}
ENV PETSC_ARCH=arch-${PROJECT_NAME}-docker
ENV PETSC_ARCH_DIR=${PETSC_DIR}/${PETSC_ARCH}
RUN /bin/bash -c './configure \
--download-mpich --download-fblaslapack --with-scalar-type=complex'
RUN /bin/bash -c 'make all'

WORKDIR /

ENV SLEPC_DIR=/slepc
ENV SLEPC_GIT=https://gitlab.com/slepc/slepc.git
RUN /bin/bash -c 'git clone -b release ${SLEPC_GIT} ${SLEPC_DIR}'
WORKDIR ${SLEPC_DIR}
ENV SLEPC_ARCH_DIR=${SLEPC_DIR}/${PETSC_ARCH}
RUN /bin/bash -c './configure'
RUN /bin/bash -c 'make all'


WORKDIR /


ENV PROJECT_DIR=/${PROJECT_NAME}
RUN /bin/bash -c 'mkdir ${PROJECT_DIR}'


ENV PROJECT_DATA_DIR=${PROJECT_DIR}/Data
RUN /bin/bash -c 'mkdir ${PROJECT_DATA_DIR}'

ENV PRECALC_DIR=${PROJECT_DIR}/${PRECALC_SUBPROJECT}
ENV PRECALC_GIT=https://gitlab.com/${PROJECT_NAME}/${PRECALC_SUBPROJECT}.git
RUN /bin/bash -c 'git clone ${PRECALC_GIT} ${PRECALC_DIR}'


ENV COMPUTE_DIR=${PROJECT_DIR}/${COMPUTATION_SUBPROJECT}
ADD . ${COMPUTE_DIR}
WORKDIR ${COMPUTE_DIR}