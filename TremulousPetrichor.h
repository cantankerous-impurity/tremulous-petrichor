#include<filesystem>
namespace fs = std::filesystem;
#include<chrono>
namespace ch = std::chrono;
#include <iostream>
#include <fstream>
#include <cstring>
#include <complex>
#include <array>
#include <vector>
#include <valarray>
#include <tuple>
#include <iterator>
#include <variant>


#include "src/Parameters/General.h"
#include "src/Parameters/Physical.h"


#include "src/Parameters/Symmetry.h"
#include <boost/math/special_functions/gamma.hpp>
#include "src/Parameters/MathematicsSupport.h"
#include "src/Parameters/StorageClasses.h"

static Mesh::One<double> _OneD;
static Mesh::One<cmplx> _One0;
static Mesh::One<cmplx> _One1;
static Mesh::Two<cmplx> _Two;

auto& _GetOneD() { return _OneD; }
auto& _GetOne0() { return _One0; }
auto& _GetOne1() { return _One1; }
auto& _GetTwo() { return _Two; }

using OneArr_t = std::array<Mesh::One<double>,3>;
using TwoArr_t = std::array<Mesh::Two<double>,3>;
using OneOneArr_t = std::array<Mesh::OneOne<double>,3>;
using TwoOneArr_t = std::array<Mesh::TwoOne<double>,3>;

static OneArr_t _OneArr;
static TwoArr_t _TwoArr;
static OneOneArr_t _OneOneArr;
static TwoOneArr_t _TwoOneArr;

auto& _GetOneArr() { return _OneArr; }
auto& _GetTwoArr() { return _TwoArr; }
auto& _GetOneOneArr() { return _OneOneArr; }
auto& _GetTwoOneArr() { return _TwoOneArr; }

static std::array<double,3> _KDoubleArr;
static std::array<double,9> _KKDoubleArr;

auto& _GetSAKin() { return _KDoubleArr; }
auto& _GetSAKinKin() { return _KKDoubleArr; }


#include "src/Specification/Results/Loc.h"
#include "src/Specification/Operators/SolidAngle.h"
#include "src/Specification/Operators/Decay.h"
#include "src/Specification/Operators/Support.h"
#include "src/Specification/Operators/Spec.h"
#include "src/Specification/Operators/SpecSupport.h"
#include "src/Specification/Basis/SubBasisInfo.h"
#include "src/Specification/Basis/Classes.h"
#include "src/Specification/Operators/DecaySupport.h"
#include "src/Specification/Basis/SubBasis.h"
#include "src/Specification/Basis/SuperBasisInfo.h"
#include "src/Specification/Basis/SuperBasis.h"
#include "src/Specification/Basis/Spec.h"
#include "src/Specification/Basis/SpecSupport.h"
#include "src/Specification/Operators/Classes.h"
#include "src/Specification/Operators/Constraints.h"


#include <sqlite3.h>
#include "src/SQLite/Container.h"
#include "src/SQLite/Interface.h"


#include "src/Coeffs/Loc.h"
#include "src/Coeffs/Spec.h"
#include "src/Coeffs/Container.h"
#include "src/Coeffs/Interface.h"
#include "src/Coeffs/Pair.h"
#include "src/Coeffs/Elem.h"
#include "src/Coeffs/Manager.h"


#include "src/Bloch/Loc.h"
#include "src/Bloch/Container.h"
#include "src/Bloch/Interface.h"
#include "src/Bloch/Storage.h"


#include "src/Visualization/Interface.h"


#include <boost/math/special_functions/legendre.hpp>
#include "src/Integrand/Support.h"
#include "src/Integrand/SolidAngle/Support.h"
#include "src/Integrand/Radial/Support.h"
#include "src/Integrand/Calcs.h"
#include "src/Integrand/Radial/Core_Td.h"
#include "src/Integrand/Radial/Summation.h"
#include "src/Integrand/Summation.h"


#include "src/Puncture/Integrands.h"
#include "src/Puncture/Manager.h"


#include <boost/array.hpp>
// #include <boost/numeric/odeint/integrate/integrate.hpp>
#include <boost/math/quadrature/gauss_kronrod.hpp>


#include "src/Quadrature/Integrands.h"
#include "src/Quadrature/Manager.h"


#include "cuba.h"
#include "src/Cubature/Integrands.h"
#include "src/Cubature/Manager.h"


#include <slepceps.h>
#include "src/Matrix/Elem.h"
#include "src/Matrix/Manager.h"

void GetResults(std::ostream &s,Matrix::Manager& M)
{
    constexpr_for<0,NumBases,1>(
        [&](auto i){
            auto ra { Basis::CurrBases.GetParams<i>() };
            s << std::get<1>(*ra) << ",";
            s << std::get<2>(*ra) << ",";
        }
    );
    for(size_t i = 0; i < NumStates; i++)
    {
        s << M.Energy(i) << ",";
        s << M.Variance(i) << ",";
    }
    s << M.Del();
    s << std::endl;

};
template< size_t b >
void DoLoop(
    double r0, double a0,
    size_t NR, double DR,
    size_t NA, double DA,
    std::ostream &s,Matrix::Manager& M
){

    for(size_t ir = 0; ir < NR; ir++)
    {
        const double r{ r0 + ir*DR };
        for(size_t ia = 0; ia < NA; ia++)
        {
            const double a{ a0 + ia*DA };
            M.Set<b>(r,a);
            GetResults(s,M);
        }
    }

};