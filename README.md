# tremulous-petrichor
Tremulous Petrichor is a subproject under the umbrella of Cantankerous Impurity. It is the subproject for constructing and maintaining the Fortran computation engine used by Cantankerous Impurity.

The Tasks Tremulous Petrichor is responsible to complete are:

1. Providing a function to compute matrix elements for specified Hamiltonians. Such functions will be provided a pair of discrete and continuous basis indices, and will be expected to return the complex-valued matrix element of the specified Hamiltonian.
2. Provide a function to construct specified matrix Hamiltonians. Such functions will be provided a set of discrete and continuous basis indices, and will be expected to return a complex-valued array corresponding to the LAPACK packed storage form of the Hermitian Hamiltonian. 
3. Provide a function to return the spectrum of a specified matrix Hamiltonian. Such functions will be provided with a set of discrete and continuous basis indices, which can be used to construct the corresponding matrix Hamiltonian. The function will be expected to return an 1-D real-valued array of energy eigenvalues as well as a 2-D complex-valued array of energy eigenvectors.
4. Provide a function to return a set of spectra of a specified matrix Hamiltonian. Such functions will be provided with a set of bases. Each basis will be a set of discrete and continuous basis indices sufficient to construct a specified matrix Hamiltonian via Task 2. The function will be expected to return a 2-D real-valued array of the sets of energy eigenvectors, as well as a 4-D complex-valued array of energy eigenvectors. This will streamline basis benchmarking, both in the form of variational optimization as well as basis filling.
5. Provide versions of the above with output restricted to a single state or subset of eigenstates. This is to save memory space and computation time as well as focus the subject of inquiry.
6. Provide wrappers to these Fortran functions so that they may be used within python.

# cantankerous-impurity
Cantankerous Impurity is an attempt to completely and adequately describe the spectrum and wavefunction of silicon donors / point-like impurities by extending and completing the effective mass model.

Silicon crystals can be host to impurities that attract excess electrons. If the impurity potential is weak enough, we expect that the electron's state will be localized near conduction band minima. Through a few simple and somewhat persuasive approximations, we can smear out the silicon background. As such, we model the electron's interactions with the surrounding media and the impurity using an effective Hamiltonian. We expect this Hamiltonian to contain a kinetic energy-like term with a modified effective mass as well as a potential term intended to effectively capture all deviations from the pure Silicon crystal potential.

It is unclear to what extent an isolated donor in a silicon crystal can be said to defy the effective mass approximation (EMA). As such, it is unclear what is lost in qualitative and descriptive power when employing the EMA. This is complicated by the fact that existing effective mass models often contain unquantified amounts of truncation and approximation error. It is not clear how much error comes from physical approximations (such as assuming that the electron wavefunction is strongly localized within k-space at conduction band minima) and how much error comes from mathematical approximations (such as those introduced by using "approximate" wavefunctions such as variational wavefunctions or truncated bases)

In Cantankerous Impurity we seek to accomplish two primary goals:
1) Quantify to what degree a particular wavefunction can be said to "solve" the implicit problem posed by an effective mass model.
2) Explore substantial additions and corrections to the effective mass model. In particular, we wish to consider how to model the exchange and correlation effects of the core valence electrons of silicon atoms close to the donor.
